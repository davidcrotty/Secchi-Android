package uk.ac.plymouth.matmutt.secchi.unit;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import io.realm.RealmList;
import uk.ac.plymouth.matmutt.secchi.BuildConfig;
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState;
import uk.ac.plymouth.matmutt.secchi.domain.DocumentationPage;
import uk.ac.plymouth.matmutt.secchi.domain.SecchiReading;
import uk.ac.plymouth.matmutt.secchi.service.LegacyXmlParserService;

/**
 * Created by David Crotty on 11/07/2017.
 * <p>
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
@RunWith(AndroidJUnit4.class)
public class LegacyXmlParserServiceTest {

    @Test
    public void test_when_retrieving_prior_experiments() throws XmlPullParserException, IOException {
        //Given
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        LegacyXmlParserService service = new LegacyXmlParserService(parser);

        //When
        InputStream firstStream = InstrumentationRegistry.getContext().getAssets().open("valid_experiments.xml");
        List<SecchiReading> firstReadingList = service.retrieveDataModelsWith(firstStream);

        //Should
        Assert.assertTrue(firstReadingList.size() == 3);
        firstStream.close();

        //When
        InputStream secondStream = InstrumentationRegistry.getContext().getAssets().open("valid_experiment_minimum.xml");
        List<SecchiReading> secondReadingList = service.retrieveDataModelsWith(secondStream);

        //Should
        Assert.assertTrue(secondReadingList.size() == 1);
        Assert.assertTrue(secondReadingList.get(0).getVesselName().equals("third ship"));
        secondStream.close();
    }

    @Test
    public void test_when_retrieving_prior_experiments_invalid() throws XmlPullParserException, IOException {
        //Given
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        LegacyXmlParserService service = new LegacyXmlParserService(parser);
        InputStream stream = InstrumentationRegistry.getContext().getAssets().open("invalid_experiments.xml");

        //When
        List<SecchiReading> readingList = service.retrieveDataModelsWith(stream);

        //Should
        Assert.assertTrue(readingList.size() == 0);
    }

    @Test
    public void test_when_retrieving_current_state() throws XmlPullParserException, IOException {
        //Given
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        LegacyXmlParserService service = new LegacyXmlParserService(parser);
        InputStream stream = InstrumentationRegistry.getContext().getAssets().open("valid_state.xml");
        ApplicationState state = new ApplicationState();
        state.setDocumentationStateList(setupDocumentation());

        //When
        ApplicationState mutatedState = service.retrieveCurrentApplicationStateWith(stream, state);

        //Should
        Assert.assertTrue(mutatedState.getDocumentationStateList().get(4).getRead());
        Assert.assertTrue(mutatedState.getDocumentationStateList().get(8).getRead());
        Assert.assertTrue(mutatedState.getDocumentationStateList().get(10).getRead());
        Assert.assertFalse(mutatedState.getDocumentationStateList().get(17).getRead());
        Assert.assertFalse(mutatedState.getDocumentationStateList().get(22).getRead());
        Assert.assertEquals(1.0f , mutatedState.getInProgressReading().getLattiutde());
        Assert.assertEquals(1.0f , mutatedState.getInProgressReading().getLongitude());
        Assert.assertEquals(0L , mutatedState.getInProgressReading().getTimeOfLocation().longValue());
        Assert.assertEquals(0.0f , mutatedState.getInProgressReading().getTemperature());
        Assert.assertEquals(0L , mutatedState.getInProgressReading().getTimeOfLocation().longValue());
        Assert.assertEquals(0.0f , mutatedState.getInProgressReading().getDepth());
        Assert.assertEquals(0L , mutatedState.getInProgressReading().getTimeOfDepth().longValue());
        Assert.assertEquals("valid ship", mutatedState.getInProgressReading().getVesselName());
    }

    @Test
    public void test_when_retrieving_current_state_invalid() throws XmlPullParserException, IOException {
        //Given
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        LegacyXmlParserService service = new LegacyXmlParserService(parser);
        InputStream stream = InstrumentationRegistry.getContext().getAssets().open("invalid_state.xml");
        ApplicationState state = new ApplicationState();
        state.setDocumentationStateList(setupDocumentation());

        //When
        ApplicationState mutatedState = service.retrieveCurrentApplicationStateWith(stream, state);

        //Should
        Assert.assertTrue(mutatedState.getDocumentationStateList().get(4).getRead());
        Assert.assertTrue(mutatedState.getDocumentationStateList().get(8).getRead());
        Assert.assertFalse(mutatedState.getDocumentationStateList().get(10).getRead());
        Assert.assertFalse(mutatedState.getDocumentationStateList().get(17).getRead());
        Assert.assertFalse(mutatedState.getDocumentationStateList().get(22).getRead());
        Assert.assertNull(mutatedState.getInProgressReading().getLattiutde());
        Assert.assertNull(mutatedState.getInProgressReading().getDepth());
        Assert.assertEquals(1.0f , mutatedState.getInProgressReading().getLongitude());
        Assert.assertEquals(0L , mutatedState.getInProgressReading().getTimeOfLocation().longValue());
        Assert.assertNull(mutatedState.getInProgressReading().getTemperature());
        Assert.assertEquals(0L , mutatedState.getInProgressReading().getTimeOfLocation().longValue());
        Assert.assertNull(mutatedState.getInProgressReading().getDepth());
        Assert.assertEquals(0L , mutatedState.getInProgressReading().getTimeOfDepth().longValue());
        Assert.assertEquals("bad ship", mutatedState.getInProgressReading().getVesselName());
    }

    private RealmList<DocumentationPage> setupDocumentation() {
        RealmList<DocumentationPage> documentList = new RealmList();

        for(int i = 0; i < BuildConfig.NUMBER_OF_DOC_PAGES; i++) {
            DocumentationPage document = new DocumentationPage();
            document.setId(i);
            document.setRead(false);
            documentList.add(document);
        }

        return documentList;
    }
}
