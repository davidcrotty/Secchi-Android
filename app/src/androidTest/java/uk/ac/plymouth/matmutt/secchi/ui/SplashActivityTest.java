package uk.ac.plymouth.matmutt.secchi.ui;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.Intents;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;

import io.realm.Realm;
import uk.ac.plymouth.matmutt.secchi.activities.MainMenuActivity;
import uk.ac.plymouth.matmutt.secchi.activities.SplashActivity;
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState;
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;

/**
 * Created by David Crotty on 02/07/2017.
 * <p>
 * Copyright © 2017 David Crotty - All Rights Reserved
 */

@RunWith(AndroidJUnit4.class)
public class SplashActivityTest {

    @Rule
    public ActivityTestRule<SplashActivity> splashRule =
            new ActivityTestRule<>(SplashActivity.class);

    @Before
    public void setup() {
        File dir = getInstrumentation().getTargetContext().getCacheDir();
        if (dir.isDirectory())
        {
            String[] children = dir.list();
            for(int i = 0; i < children.length; i++)
            {
                new File(dir, children[i]).delete();
            }
        }
    }

    @Test
    public void when_no_legacy_xml_to_migrate() {
        //steps
        //clear realm
        //pass data
        //next_white activity should start
        //UI should look like X
    }

    @After
    public void tearDown() {
    }
}
