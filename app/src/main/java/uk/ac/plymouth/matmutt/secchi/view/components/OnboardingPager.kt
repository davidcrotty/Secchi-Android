package uk.ac.plymouth.matmutt.secchi.view.components

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.view.OnBoardingFragment

/**
 * Created by David Crotty on 15/11/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class OnboardingPager(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    companion object {
        val INTRO = 0
        val GPS = 1
        val USER_INTERFACE = 2
        val UPLOAD = 3
        val PUSH_NOTIFICATIONS = 4
    }

    override fun getItem(position: Int): Fragment {
        return OnBoardingFragment.newInstance(position)
    }

    override fun getCount(): Int = BuildConfig.NUMBER_OF_ONBOARDING_PAGES

}