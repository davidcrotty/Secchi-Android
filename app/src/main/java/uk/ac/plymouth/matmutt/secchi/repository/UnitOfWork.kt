package uk.ac.plymouth.matmutt.secchi.repository

import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext

/**
 * Created by David Crotty on 14/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class UnitOfWork(private val context: RealmContext) : IUnitOfWork {

    /**
     * Cannot follow full repository pattern. This is due to the context needing
     * to know a reified type. This alone destroys the abstract layer as it cannot be
     * passed down via an abstract contract interface due to byte codes lack of
     * support for reified classes.
     */
    fun getRealmContext(): RealmContext {
        return context
    }

    override fun complete() {
        context.complete()
    }
}