package uk.ac.plymouth.matmutt.secchi.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import uk.ac.plymouth.matmutt.secchi.view.SecchiFragmentConfig.Companion.EXPERIMENT
import uk.ac.plymouth.matmutt.secchi.view.model.Experiment

/**
 * Created by David Crotty on 25/10/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class RoamingWarningDialog : AppCompatDialogFragment() {

    private var listener: DialogProvider.RoamingWarningDialogListener? = null

    companion object : SecchiFragmentConfig() {
        val TAG = "RoamingWarningDialog"

        override fun newInstance(title: String, description: String, positiveText: String, negativeText: String, experiment: Experiment, fragment: AppCompatDialogFragment): AppCompatDialogFragment {
            return super.newInstance(title, description, positiveText, negativeText, experiment, fragment)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is DialogProvider.RoamingWarningDialogListener) {
            listener = context
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setTitle(arguments.getString(SecchiFragmentConfig.TITLE))
                .setMessage(arguments.getString(SecchiFragmentConfig.DESCRIPTION))
                .setPositiveButton(arguments.getString(SecchiFragmentConfig.POSITIVE_TEXT), { dialog, which ->
                    listener?.continueUpload(arguments.getParcelable(EXPERIMENT))
                    dismiss()
                })
                .setNegativeButton(arguments.getString(SecchiFragmentConfig.NEGATIVE_TEXT), { dialog, which ->
                    listener?.dontUpload()
                    dismiss()
                })
                .create()
    }
}