package uk.ac.plymouth.matmutt.secchi.service

import android.annotation.TargetApi
import android.content.Context
import io.realm.RealmList
import org.xmlpull.v1.XmlPullParser
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState
import uk.ac.plymouth.matmutt.secchi.domain.DocumentationPage
import uk.ac.plymouth.matmutt.secchi.domain.SecchiReading
import uk.ac.plymouth.matmutt.secchi.domain.User
import java.io.IOException
import java.io.InputStream
import java.io.InvalidClassException
import java.io.InvalidObjectException
import java.nio.charset.StandardCharsets
import java.util.*

/**
 * Created by David Crotty on 08/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class LegacyXmlParserService(val parser: XmlPullParser) {

    private val SECCHI_READING = "secchireading"
    private val VESSEL_NAME = "vesselname"
    private val LATITUDE = "latitude"
    private val LONGITUDE = "longitude"
    private val DEPTH = "depth"
    private val TEMPERATURE = "temperature"
    private val NOTES = "notes"
    private val PHOTO_LOCATION = "photolocation"
    private val TIME_OF_LOCATION = "timeoflocation"
    private val TIME_OF_DEPTH = "timeofdepth"
    private val TIME_OF_TEMPERATURE = "timeoftemperature"
    private val EXTRA = "extra"
    private val PACKAGE_VARIABLES: String = "sceehipackagevariables"
    private val PACKAGE_TIME_LOCATION: String = "timeoflocation"
    private val PACKAGE_TIME_DEPTH: String = "timeofdepth"
    private val PACKAGE_TIME_TEMPERATURE: String = "timeoftemperature"
    private val PACKAGE_LONGITUDE = "lon"
    private val PACKAGE_LATITUDE = "lat"
    private val USER_READ_SECCHI_INFO: String = "userhasreadsecchiinfo"
    private val USER_READ_WHAT_IS_SECCHI_DISK: String = "userhasreadwhatisasecchidisk"
    private val USER_READ_MAKING_SECCHI_DISK: String = "userhasreadmakingasecchidisk"
    private val USER_READ_USING_SECCHI_DISK: String = "userhasreadusingasecchidisk"
    private val USER_READ_OPTIONAL_OBSERVATIONS: String = "userhasreadoptionalobservations"
    /* Legacy XML does not have consistent element names with pending + submitted.xml, unfortunatley
    results in duplicate code */
    private val PACKAGE_DEPTH: String = "secchidepth"
    private val PACKAGE_SEA_TEMPERATURE = "seaTemperature"
    private val PACKAGE_NOTES = "notes"
    private val PACKAGE_EXTRA = "extra"
    private val PACKAGE_BOAT_NAME = "vesselparticulars"

    fun retrieveDataModelsWith(stream: InputStream) : List<SecchiReading> {
        parser.setInput(stream, null)

        val readingList = ArrayList<SecchiReading>()
        var reading: SecchiReading? = null

        var eventType = parser.eventType

        while(eventType != XmlPullParser.END_DOCUMENT) {
            try {
                when (eventType) {
                    XmlPullParser.START_TAG -> {
                        val name = parser.name
                        if (name.equalsIgnoreCase(SECCHI_READING)) {
                            reading = SecchiReading()
                        }
                        parseModelUsing(reading, parser)
                    }
                    XmlPullParser.END_TAG -> {
                        val name = parser.name
                        if (name.equalsIgnoreCase(SECCHI_READING)) {
                            reading?.let { reading ->
                                if(reading.isValidModel()) {
                                    readingList.add(reading)
                                }
                            }
                        }
                    }
                }
            } catch (ex : Exception) {
                if(ex is NullPointerException || ex is NumberFormatException) {
                    if(readingList.isEmpty() == false) {
                        readingList.removeAt(readingList.size - 1)
                    }
                } else {
                    throw ex
                }
            } finally {
                eventType = parser.next()
            }
        }

        return readingList
    }

    @TargetApi(19)
    fun logFileOutputWith(context: Context, path: String) : String? {
        val inputStream = try {
            context.openFileInput(path)
        } catch (ex: IOException) {
            return null
        }

        val scanner = Scanner(inputStream, StandardCharsets.UTF_8.name())
        val builder = StringBuilder()
        while(scanner.hasNext()) {
            builder.append(scanner.next())
        }

        return builder.toString()
    }

    fun retrieveCurrentApplicationStateWith(stream: InputStream,
                                            applicationState: ApplicationState) : ApplicationState {
        parser.setInput(stream, null)
        var inProgressReading: SecchiReading? = null

        var eventType = parser.eventType
        while(eventType != XmlPullParser.END_DOCUMENT) {
            when(eventType) {
                XmlPullParser.START_TAG -> {
                    val name = parser.name
                    if(name.equalsIgnoreCase(PACKAGE_VARIABLES)) {
                        inProgressReading = SecchiReading()
                    } else if(name.equalsIgnoreCase(PACKAGE_LATITUDE)) {
                        //don't take the last reading as prior update stores without time, likely user has not updated at sea due to cellar charges.
                    } else if(name.equalsIgnoreCase(PACKAGE_LONGITUDE)) {
                        //don't take the last reading as prior update stores without time, likely user has not updated at sea due to cellar charges.
                    } else if(name.equalsIgnoreCase(PACKAGE_DEPTH)) {
                        val depth = getValueAllowNull<Float>(parser.nextText())
                        inProgressReading?.takeUnless { depth == null || depth == -100.0f }?.also {
                            it.depth = depth
                        }
                    } else if(name.equalsIgnoreCase(PACKAGE_SEA_TEMPERATURE)) {
                        val temp = getValueAllowNull<Float>(parser.nextText())
                        inProgressReading?.takeUnless { temp == null || temp == -100.0f }?.also {
                            it.temperature = temp
                        }
                    } else if(name.equalsIgnoreCase(PACKAGE_NOTES)) {
                        val readInfo = parser.nextText()
                        readInfo?.let {
                            inProgressReading?.notes = it
                        }
                    } else if(name.equalsIgnoreCase(PACKAGE_EXTRA)) {
                        val readInfo = parser.nextText()
                        readInfo?.let {
                            inProgressReading?.extra = it
                        }
                    } else if(name.equalsIgnoreCase(PACKAGE_TIME_LOCATION)) {
                        inProgressReading?.timeOfLocation = getValueAllowNull<Long>(parser.nextText())
                    } else if(name.equalsIgnoreCase(PACKAGE_TIME_DEPTH)) {
                        inProgressReading?.timeOfDepth = getValueAllowNull<Long>(parser.nextText())
                    } else if(name.equalsIgnoreCase(PACKAGE_TIME_TEMPERATURE)) {
                        var temperature = getValueAllowNull<Long>(parser.nextText())
                        if(temperature != null) {
                            if(temperature == -100L) {
                                temperature = null
                            }
                        }
                        inProgressReading?.timeOfTemperature = temperature
                    } else if(name.equalsIgnoreCase(PACKAGE_BOAT_NAME)) {
                        val readInfo = parser.getAttributeValue("",
                                "Name")
                        val userID = parser.getAttributeValue(null, "SSR")
                        userID?.let {
                            applicationState.user?.APIID = it
                        }
                        readInfo?.let {
                            inProgressReading?.vesselName = it
                        }
                    } else if(name.equalsIgnoreCase(USER_READ_SECCHI_INFO)) {
                        val readInfo = parser.nextText()
                        readInfo?.let {
                            if(it.toBoolean()) {
                                val documentation = applicationState.documentationStateList ?: return@let
                                applicationState.documentationStateList = markAsReadFor(0..4, documentation, it.toBoolean())
                            }
                        }
                    } else if(name.equalsIgnoreCase(USER_READ_WHAT_IS_SECCHI_DISK)) {
                        val whatIsDisk = parser.nextText()
                        whatIsDisk?.let {
                            val documentation = applicationState.documentationStateList ?: return@let
                            applicationState.documentationStateList = markAsReadFor(5..8, documentation, it.toBoolean())
                        }
                    } else if(name.equalsIgnoreCase(USER_READ_MAKING_SECCHI_DISK)) {
                        val makingDisk = parser.nextText()
                        makingDisk?.let {
                            val documentation = applicationState.documentationStateList ?: return@let
                            applicationState.documentationStateList = markAsReadFor(9..10, documentation, it.toBoolean())
                        }
                    } else if(name.equalsIgnoreCase(USER_READ_USING_SECCHI_DISK)) {
                        val usingDisk = parser.nextText()
                        usingDisk?.let {
                            val documentation = applicationState.documentationStateList ?: return@let
                            applicationState.documentationStateList = markAsReadFor(11..17, documentation, it.toBoolean())
                        }
                    } else if(name.equalsIgnoreCase(USER_READ_OPTIONAL_OBSERVATIONS)) {
                        val optionalObservations = parser.nextText()
                        optionalObservations?.let {
                            val documentation = applicationState.documentationStateList ?: return@let
                            applicationState.documentationStateList = markAsReadFor(18..22, documentation, it.toBoolean())
                        }
                    }
                }
                XmlPullParser.END_TAG -> {
                    applicationState.inProgressReading = inProgressReading
                }
            }

            eventType = parser.next()
        }

        return applicationState
    }

    private fun markAsReadFor(range : IntRange, documentation: RealmList<DocumentationPage>, value: Boolean) : RealmList<DocumentationPage> {
        for(i in range) {
            documentation[i]?.read = value
        }

        return documentation
    }

    inline private fun <reified T>getValueAllowNull(xmlValue: String?) : T? {
        if(xmlValue == null) return null
        try {
            if (Float::class.javaObjectType.isAssignableFrom(T::class.java)) {
                return xmlValue.toFloat() as T
            } else if (Long::class.javaObjectType.isAssignableFrom(T::class.java)) {
                return xmlValue.toLong() as T
            } else if(Boolean::class.javaObjectType.isAssignableFrom(T::class.java)) {
                try {
                    return xmlValue.toBoolean() as T
                } catch (ex: Exception) {
                    return false as T
                }
            }
        } catch (ex: NumberFormatException) {
            return null
        }

        return null
    }

    inline private fun <reified T>getValue(xmlValue: String?) : T {
        if(xmlValue == null) throw NullPointerException("Value is null")

        if(Float::class.javaObjectType.isAssignableFrom(T::class.java)) {
          return xmlValue!!.toFloat() as T
        } else if (Long::class.javaObjectType.isAssignableFrom(T::class.java)) {
          return xmlValue!!.toLong() as T
        }

        return null as T
    }

    private fun parseModelUsing(reading: SecchiReading?, parser: XmlPullParser) {
        val name = parser.name

        when(name.toLowerCase()) {
            VESSEL_NAME -> {
                reading?.vesselName = parser.nextText()
            }
            LATITUDE -> {
                reading?.lattiutde = getValue<Float>(parser.nextText())
            }
            LONGITUDE -> {
                reading?.longitude = getValue<Float>(parser.nextText())
            }
            DEPTH -> {
                reading?.depth = getValue<Float>(parser.nextText())
            }
            TEMPERATURE -> {
                reading?.temperature = getValue<Float>(parser.nextText())
            }
            NOTES -> {
                reading?.notes = parser.nextText()
            }
            PHOTO_LOCATION -> {
                reading?.photoPath = parser.nextText()
            }
            TIME_OF_LOCATION -> {
                reading?.timeOfLocation = getValue<Long>(parser.nextText())
            }
            TIME_OF_DEPTH -> {
                reading?.timeOfDepth = getValue<Long>(parser.nextText())
            }
            TIME_OF_TEMPERATURE -> {
                reading?.timeOfTemperature = getValue<Long>(parser.nextText())
            }
            EXTRA -> {
                reading?.deviceInfo = parser.nextText()
            }
        }
    }

    private fun String.equalsIgnoreCase(to: String) : Boolean {
        if(this == null) {
            return false
        }
        if(to == null) {
            return false
        }
        return this.toLowerCase().equals(to.toLowerCase())
    }
}