//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Custom viewI for satellite status.										//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;


import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
//We can see from the line below that this Activity listens for events.		//
//**************************************************************************//

public class SatellitesStatusActivity extends Activity 
                                          
{

	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName           = "";
	private SatellitesView         satellitesView         = null;
	private SecchiPackageVariables secchiPackageVariables = null;
	private LocationManager        locationManager        = null;
	private String			       bestProvider	       	  = null;

	
	
	


	
	//**********************************************************************//
	//As the name suggests; called when the activity is first created. 		//
	//**********************************************************************//
	@Override
    public void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        activityName   = getClass().getSimpleName();
  	    satellitesView = new SatellitesView(this);
        setContentView(satellitesView);
	}

	
	
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
	// If we are to avoid flattening the battery, we must remove the status	 //
	// listeners.  It is possible that we might want to keep logging position//
	// at a reduced rate, though.											 //
	//***********************************************************************//
	@Override
	public void onPause()
   {
	   super.onPause();
	   locationManager.removeUpdates(satellitesView);
	   locationManager.removeGpsStatusListener(satellitesView);
	   secchiPackageVariables.release();
	   satellitesView.satellites = null;		//Delete old data.


	   if (D.debug) Log.i(activityName, "Activity paused");
   }

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
	
  	    //Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
		
		initLocationService();
	    int gpsUpdateTimeSecs   = 1;

		String provider = null;
		if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			provider = LocationManager.GPS_PROVIDER;
		} else if(locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			provider = LocationManager.NETWORK_PROVIDER;
		}

		locationManager.requestLocationUpdates(provider,
                                               gpsUpdateTimeSecs*1000, 
                                               0, satellitesView);
		locationManager.addGpsStatusListener(satellitesView);

		if (D.debug) Log.i(activityName, "Activity resumed");   		
	}
	


	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	// If we are to avoid flattening the battery, we must remove the status	 //
	// listeners.  It is possible that we might want to keep logging position//
	// at a reduced rate, though.											 //
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}


	
	

	//***********************************************************************//
	// Initialise the location service.										 //
	//***********************************************************************//
	
	private void initLocationService()
	{
        //******************************************************************//
        // Get the location service.  This method, getSystemService() is how//
        // you get many of the system services.								//
        //******************************************************************//        
        if (locationManager == null)
        	locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        bestProvider = secchiPackageVariables.getBestLocationProvider();
	}

	
	
	
	

   	//**********************************************************************//
   	//**********************************************************************//
	//**********************************************************************//
	// An inner class to represent the satellites viewI.						//
	//**********************************************************************//
	public class SatellitesView extends View implements LocationListener,
														GpsStatus.Listener
	{
		//**********************************************************************//
		// Constructor.  It must be parameterised and we must invoke the		//
		// superclass constructor.												//
		//**********************************************************************//
		public SatellitesView(Context context) 
		{
			super(context);
			this.context = context;
		}

		
		
		//**********************************************************************//
		// Instance variables.													//
		//**********************************************************************//
		Paint                   painter    = new Paint();
		Iterable <GpsSatellite> satellites = null;
		Context                 context    = null;
		
		
		
		//**********************************************************************//
		// As the name suggests, this is called when the Canvas needs to be 	//
		// re-drawn.															//
		//**********************************************************************//
		@Override
		public void onDraw(Canvas canvas)
		{
			int width  = canvas.getWidth();
			int height = canvas.getHeight();
		    int numSatelites = 0;

		    setBackgroundResource(R.drawable.gradient_fill_no_padding_no_line);
		    
		    //******************************************************************//
		    // If no satellites just display "waiting" and bomb out.			//
		    //******************************************************************//
		    if (satellites == null)
		    {
		    	painter.setColor(Color.WHITE);
				painter.setTextSize(50);

		    	int y = height/2;
		    	canvas.drawText(context.getString(R.string.waiting), 
		    			        20, y, painter);
		    	
		    	return;
		    }
		    
		    
		    for (GpsSatellite sat : satellites)
		    {
		    	numSatelites++;			//This is silly!
		    }


			
		    float  yScale        =  (float) (height * 0.6) / numSatelites;
		    int    yOfsTxt       =  (int) (height / 20);
		    int    xOfsTxt       =  (int) (width / 20);
		    int    xOfsBar       =  (int) (width / 8);
		    float  barWidthScale =  (width -xOfsBar); 
		    int    barHeight     =  (height / 50);
		    int    barWidth      = 0;
		    int    satNum        = 0;
		    int    y             = 0;
		    float  snr			 = 0;
		    float  snrFullScale  = 40;   
		    String strSatNum     = null;
		    int    satsUsedInFix = 0;
		    

		    for (GpsSatellite sat : satellites)
		    {
		    	y         = (int) (satNum * yScale + yOfsTxt);
		    	strSatNum = Integer.toString(sat.getPrn());
		    	satNum++;
   	
		    	
				painter.setColor(Color.WHITE);			
				painter.setTextSize(20);

		    	canvas.drawText(strSatNum, xOfsTxt, y, painter);
		    	
				if (sat.usedInFix())
				{
					satsUsedInFix++;
					painter.setColor(Color.GREEN);  //Green if sat used
				}									//to get fix.
				else								
					painter.setColor(Color.YELLOW);

		    	snr = sat.getSnr() / snrFullScale; 
		    	barWidth = (int) (snr * barWidthScale);
		    	canvas.drawRect(xOfsBar, y - barHeight, barWidth, y + barHeight, painter);
		    	
		    }
		    
			painter.setColor(Color.WHITE);			
			painter.setTextSize(20);
			
			canvas.drawText(context.getString(R.string.satellitesUsedInFix)
					        + " " 
					        + Integer.toString(satsUsedInFix)
					        + " "
					        + context.getString(R.string.of)
					        + " "
					        + Integer.toString(numSatelites), 
					          10, height - (height / 5), painter);
		}
		
		


		
		//***********************************************************************//
		// Call-backs for location service.										 //
		//***********************************************************************//
		// Location changed call-back.										 	 //
		//																		 //
		// Broadcast the event to all listeners.  And send it to the package	 //
		// variables.															 //
		//***********************************************************************//
		@Override
		public void onLocationChanged(Location newLocation) 
		{
			secchiPackageVariables.setCurrentLocation(newLocation);
			Toast.makeText(getApplicationContext(),
					       R.string.positionAcquired,
					       Toast.LENGTH_SHORT).show();

			secchiPackageVariables.cancelWakeLockTimer();
			secchiPackageVariables.releaseWakeLock();
		}
		

		@Override
		public void onProviderDisabled(String provider) 
		{
			
			String explanation = getString(R.string.locationProvider) 
			           + " "
			           + provider 
			           + " "
			           + getString(R.string.hasBeenDisabled);
			String title = getString(R.string.locationProvider);
			
			Toast.makeText(context, explanation, Toast.LENGTH_SHORT).show();
			if (D.debug) Log.i(activityName, explanation);
		}

		
		@Override
		public void onProviderEnabled(String provider) 
		{
			String explanation = getString(R.string.locationProvider) 
	        + " "
	        + provider 
	        + " "
	        + getString(R.string.hasBeenEnabled);
			String title = getString(R.string.locationProvider);
			
			Toast.makeText(context, explanation, Toast.LENGTH_SHORT).show();
			if (D.debug) Log.i(activityName, explanation);		
		}

		

		@Override
		public void onGpsStatusChanged(int s) 
		{
			GpsStatus status = locationManager.getGpsStatus(null);
		    satellites       = null;
			satellites       = status.getSatellites();
		    invalidate();
		}





		@Override
		public void onStatusChanged(String provider, int s, Bundle extras) 
		{
		}
	}	//End of classy inner class.

	
}	// End of classy class.
