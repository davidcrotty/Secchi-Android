package uk.ac.plymouth.matmutt.secchi.view.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.graphics.Paint.ANTI_ALIAS_FLAG
import uk.ac.plymouth.matmutt.secchi.R
import android.support.v4.content.ContextCompat
import android.view.View


/**
 * Created by David Crotty on 24/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class AnglePhotoView : RelativeLayout {
    private val path = Path()
    private val paint = Paint(ANTI_ALIAS_FLAG)
    private var viewWidth: Int? = null
    private var viewHeight: Int? = null
    private var callback: (Float) -> Unit? = {}
    private var triangleHeight: Float? = null

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        View.inflate(context, R.layout.angle_photo_view, this)
    }

    init {
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        paint.color = ContextCompat.getColor(context, R.color.about_blue)
    }

    fun addTriangleHeightCallback(callback: (Float) -> Unit) {
        this.callback = callback
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        viewHeight = h
        viewWidth = w
        val triangle = (viewHeight!!.toFloat() * 0.17f)
        triangleHeight = viewHeight!!.toFloat() - triangle
        callback.invoke(triangle)
        super.onSizeChanged(w, h, oldw, oldh)
    }

    //dispatch callback to get height so we

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        path.moveTo(0f ,viewHeight!!.toFloat())
        path.lineTo(viewWidth!!.toFloat(), viewHeight!!.toFloat())
        path.lineTo(viewWidth!!.toFloat(), triangleHeight!!)
        path.lineTo(0f, viewHeight!!.toFloat())
        canvas?.drawPath(path, paint)
    }
}