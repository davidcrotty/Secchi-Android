//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Custom activity for Secchi depth with a dragable depth wossit.			//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2013.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.view.components;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.interfaces.ISecchiDepthlistener;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View.OnTouchListener;

public class SecchiDepthView extends View implements OnTouchListener,
                                                     OnGestureListener,
                                                     OnScaleGestureListener,
                                                     Runnable

{

   		
  		
	//******************************************************************//
	// We need this constructor if the viewI is to be included in an     //
  	// Android XML viewI layout.                                         //
	//																	//
	// No need to retain package variables, that should be done by our  //
	// containing activity.												//
	//******************************************************************//
	public SecchiDepthView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		gestureDetector        = new GestureDetector(context, this);
		scaleGestureDetector   = new ScaleGestureDetector(context, this);
		resources              = getResources();
		bitmapDiskAndRope      = BitmapFactory.decodeResource(resources, R.drawable.secchi_and_rope);
		matTransform           = new Matrix();
		diskHeight             = bitmapDiskAndRope.getHeight();
		diskWidth              = bitmapDiskAndRope.getWidth();
		
		setOnTouchListener(this);
   	}
   		
   		
   		
   	//**********************************************************************//
   	// Instance variables.													//
   	//**********************************************************************//
	private String                 activityName         = "Secchi Depth View";
 	private int                    width                = 0;
	private int                    height               = 0;
	private Paint                  painter              = new Paint();
	private GestureDetector        gestureDetector      = null;
	private ScaleGestureDetector   scaleGestureDetector = null;
	private ISecchiDepthlistener   secchiDepthListener  = null;
	private Bitmap                 bitmapDiskAndRope    = null;
	private Resources              resources            = null;
	private Matrix                 matTransform			= null;
	private int                    diskX                = 0;
	private int                    diskY                = 0;
	private int					   offsetY				= 0;
	private int                    diskHeight           = 0;
	private int                    diskWidth            = 0;
	private float                  diskDepth            = 25;	//Depth in m
	private float                  maxDiskDepth         = 100;
	private final float            logmaxDepthAnd1		= (float) Math.log10(maxDiskDepth+1);
	private float                  minDiskDepth         = 0f;
	private boolean                animationRunning     = false;
	private Thread 			       animationThread      = null;
	private float                  seaLevelOnGraphic    = 3.3f / 16.5f;   // I measured the boundary from the image
	private volatile boolean       sliderScrolled       = false;		
	
	
	//**********************************************************************//
	// As the name suggests, this is called when the Canvas needs to be 	//
	// re-drawn.															//
	//**********************************************************************//
	@Override
	public void onDraw(Canvas canvas)
	{
		width   = canvas.getWidth();
		height  = canvas.getHeight();					 		//Seems to return full height of screen 
		offsetY = (int) ( (float) height * seaLevelOnGraphic);  // I measured the boundary from the image
		
		drawDiskAndRope(canvas);
		
		float diskLogDepth = calcLogDepth();
		
		if (secchiDepthListener != null)
			secchiDepthListener.onDepthChanged(diskLogDepth, 
					                           sliderScrolled);
		sliderScrolled = false;
	}

   		
   		
	
	//**********************************************************************//
	// Draw the disk and the rope.											//
	//**********************************************************************//
	private void drawDiskAndRope(Canvas canvas)
   	{
		float diskScale  = (float) height / (float) diskHeight; 

	/*	diskY = (int) ((diskiDepth / maxDiskDepth 
				       * height - diskHeight)*diskScale*0.8)
				       + offsetY;
	*/
		diskY = (int) (offsetY								// Sea level
				       - diskHeight*diskScale*0.91			// weight on the bottom of the disk
				       + diskDepth / maxDiskDepth * height	// factor in height
				       * (1 - seaLevelOnGraphic)			// Factor in sea level != y=0.
				       *0.9);			 					// Fuddle factor.
		diskX = (width / 2) - (diskWidth / 2);
	
		matTransform.reset();
		matTransform.postScale(diskScale, diskScale);
		matTransform.postTranslate(diskX, diskY);
		canvas.drawBitmap(bitmapDiskAndRope, matTransform, painter);
   	}
	
	
	
	
	//**********************************************************************//
	// Use Nick's magic formula to calculate the logarithmic relationship	//
	// between our slider and the number we return							//
	//**********************************************************************//
	private float calcLogDepth()
	{
		
		float result = (float) 
			   (Math.pow(10, diskDepth*logmaxDepthAnd1/maxDiskDepth) - 1); 

		result *= 10;
		result = Math.round(result);	// Round to 1dp
		result /= 10;
		
		if (result < minDiskDepth) result = minDiskDepth;
		if (result > maxDiskDepth) result = maxDiskDepth;
		return result;
	}
	
	
	
	//**********************************************************************//
	// Use Nick's magic formula to calculate the logarithmic relationship	//
	// between our slider and the number we return							//
	//**********************************************************************//
	private float calcLogDepth(float depthIn)
	{
		
		float result = (float) 
			   (Math.pow(10, depthIn*logmaxDepthAnd1/maxDiskDepth) - 1); 

		result *= 10;
		result = Math.round(result);	// Round to 1dp
		result /= 10;
		
		if (result < minDiskDepth) result = minDiskDepth;
		if (result > maxDiskDepth) result = maxDiskDepth;
		return result;
	}
	
	
	

	
	private float calcInversLogDepth(float logDepth)
	{
		float  inverse =   (float) Math.log(logDepth+ 1) * maxDiskDepth / logmaxDepthAnd1 + (float)Math.log(1);
		//inverse       -= 1;
		inverse       /= 3f;
		
		if (inverse < minDiskDepth) inverse = minDiskDepth;
		if (inverse > maxDiskDepth) inverse = maxDiskDepth;

		return inverse;
	}
	
	
	
	//**********************************************************************//
	// Do wee animation.													//
	//**********************************************************************//
	public void doWeeAnimation()
	{
		animationThread = new Thread(this);
		animationThread.start();
	}
	
	
	
	
	//**********************************************************************//
	// Stop wee animation.													//
	//**********************************************************************//
	public void stopWeeAnimation()
	{
		
		if (animationThread != null)
		{
			animationRunning = false;
			animationThread.interrupt();
			animationThread = null;
		}
	}
	
	

	
	//***********************************************************************//
	// Call-backs overridden from OnTouchListener.							 //
	//***********************************************************************//
	@Override
	public boolean onTouch(View view, MotionEvent event) 
	{
		gestureDetector.onTouchEvent(event);
		scaleGestureDetector.onTouchEvent(event);
		return true;
	}

		
	//***********************************************************************//
	// Call-backs overridden from the Gesture Detector.						 //
	//***********************************************************************//
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2,
			float distanceX, float distanceY) 
	{
		
		diskDepth -= distanceY * (float) maxDiskDepth / (float) height;

		if (diskDepth > maxDiskDepth)
			diskDepth = maxDiskDepth;
		if (diskDepth < minDiskDepth)
			diskDepth = minDiskDepth;

		if (Math.abs(distanceX) > 0.0001) sliderScrolled = true;
		invalidate();
		return true;
	}

		
		
	//**********************************************************************//
	// Increment the depth, in steps of 0.1.  We also round to the nearest  //
	// 1/10th.																//
	//**********************************************************************//
	public void incrementSecchiDepth()
	{
		float deltaDepth = (float) (0.1f * maxDiskDepth *Math.log(diskDepth + 1) / diskDepth );
		if (Math.abs(deltaDepth) > 0.1)
			deltaDepth = 0.1f;
		
		diskDepth += deltaDepth;
		diskDepth *= 10f;						
		diskDepth = Math.round(diskDepth);	//Round to 1dp
		diskDepth /= 10f;
		
		if (diskDepth  > maxDiskDepth)
			diskDepth  = maxDiskDepth;
		sliderScrolled = true;
		invalidate();
	}
	

	
	
	
	//**********************************************************************//
	// Increment the depth, in steps of 0.1.  We also round to the nearest  //
	// 1/10th.																//
	//**********************************************************************//
	public void decrementSecchiDepth()
	{
		float deltaDepth = (float) (0.1f * maxDiskDepth *Math.log(diskDepth + 1) / diskDepth );
		if (Math.abs(deltaDepth) > 0.1)
			deltaDepth = 0.1f;
		
		diskDepth -= deltaDepth;
		diskDepth *= 10f;
		diskDepth = Math.round(diskDepth);	//Round to 1dp
		diskDepth /= 10f;
		
		if (diskDepth  < minDiskDepth)
			diskDepth  = minDiskDepth;
		sliderScrolled = true;
		invalidate();
	}

	
	
	
		
	//***********************************************************************//
	// The rest of the call-backs are unused.								 //
	//***********************************************************************//		
	@Override
	public boolean onDown(MotionEvent motionEvent) 
	{
		return true;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) 
	{
			return false;
	}

	@Override
	public void onLongPress(MotionEvent e) 
	{
	}

	@Override
	public void onShowPress(MotionEvent e) 
	{
			
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) 
	{
		return false;
	}
	

	//***********************************************************************//
	// Run() method.  We turn ourselves into a Thread and do a wee animation //
	//***********************************************************************//
	@Override
	public void run() 
	{
		if (diskDepth < 0) return;			//Roy look away.
		animationRunning = true;
		
		int    numFrames  = 50;
		float  startDepth = diskDepth;
		
		for (int i= 0; i < numFrames; i++)
		{
			diskDepth = startDepth + (float) Math.sin(((float) i/ (float) numFrames)*(float) Math.PI)*50;
			if (diskDepth < minDiskDepth) diskDepth = minDiskDepth;
			if (diskDepth > maxDiskDepth) diskDepth = maxDiskDepth;
			
			if (!animationRunning) break;
			try 
			{
				Thread.sleep(30);
			} 
			catch (InterruptedException e) 
			{
			}
			sliderScrolled = true;
			postInvalidate();
		}
		animationThread = null;
	}


	
	
	
	//***********************************************************************//
	// Call-backs overridden from OnScaleGestureListener.					 //
	//***********************************************************************//
	@Override
	public boolean onScale(ScaleGestureDetector detector) 
	{
		return true;
	}
	@Override
	public boolean onScaleBegin(ScaleGestureDetector detector) 
	{
		return true;
	}
	@Override
	public void onScaleEnd(ScaleGestureDetector detector) 
	{
	}


   		
	//**********************************************************************//
	// Getters and setters; Mary would approve (and Roy).					//
	//**********************************************************************//

	
	
	
	//***********************************************************************//
	// Set the depth listener.												 //
	//***********************************************************************//
	public void setSccchiDepthListener(ISecchiDepthlistener listener)
	{
		secchiDepthListener = listener;
	}



	/**
	 * @return the secchiDepth
	 */
	public float getSecchiDepth() 
	{
		return diskDepth;
	}



	/**
	 * @param secchiDepth the secchiDepth to set
	 */
	public void setSecchiDepth(float secchiDepth) 
	{
		if (secchiDepth > maxDiskDepth)
			secchiDepth = maxDiskDepth;
		if (secchiDepth < minDiskDepth)
			secchiDepth = minDiskDepth;
		diskDepth = calcInversLogDepth(secchiDepth);		 
		sliderScrolled = false;
		invalidate();
	}



	

		

}	//End of classy class
