package uk.ac.plymouth.matmutt.secchi.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_documentation.*
import uk.ac.plymouth.matmutt.secchi.R

/**
 * Created by David Crotty on 18/06/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class DocumentFragment : Fragment() {

    companion object {
        val RESOURCE_KEY: String = "RESOURCE_KEY"

        fun newInstance(pageResourceID: Int?) : DocumentFragment {
            val fragment = DocumentFragment()
            val bundle = Bundle()
            if(pageResourceID != null)  {
                bundle.putInt(RESOURCE_KEY, pageResourceID)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_documentation, container, false)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val resourceID = arguments.getInt(RESOURCE_KEY)
        document_image.setImageResource(resourceID)
    }
}