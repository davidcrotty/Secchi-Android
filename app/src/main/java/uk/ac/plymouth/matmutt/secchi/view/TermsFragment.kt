package uk.ac.plymouth.matmutt.secchi.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import android.view.KeyEvent
import uk.ac.plymouth.matmutt.secchi.R

/**
 * Created by David Crotty on 06/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class TermsFragment : AppCompatDialogFragment() {

    companion object {
        val TAG = "TermsFragment"
    }

    private var termsDismissListener: DialogProvider.TermsDismissListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is DialogProvider.TermsDismissListener) {
            termsDismissListener = context
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setTitle(resources.getString(R.string.terms_conditions_title))
                .setMessage(resources.getString(R.string.privacyPolicyStatement))
                .setPositiveButton(resources.getString(R.string.terms_conditions_accept), { dialog, which ->
                    //TODO rx listener here
                    dismiss()
                    termsDismissListener?.termsAccepted()
                })
                .setCancelable(false)
                .setOnKeyListener { dialog, keyCode, event ->
                    if(keyCode == KeyEvent.KEYCODE_BACK && event.action == KeyEvent.ACTION_UP) {
                        activity.finish()
                    }
                    false
                }
                .create()
    }
}