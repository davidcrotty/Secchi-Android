package uk.ac.plymouth.matmutt.secchi.network

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import com.firebase.jobdispatcher.JobParameters
import com.firebase.jobdispatcher.JobService
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import okhttp3.ResponseBody
import uk.ac.plymouth.matmutt.secchi.App
import uk.ac.plymouth.matmutt.secchi.domain.SecchiReading
import uk.ac.plymouth.matmutt.secchi.domain.UploadStatus
import uk.ac.plymouth.matmutt.secchi.network.model.UploadModel
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import java.io.IOException

/**
 * Created by David Crotty on 01/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class UploadService : JobService() {

    private var disposable : Disposable? = null

    companion object {
        val UPLOAD_ACTION = "UPLOAD_ACTION"
        val SUBMIT_SUCCESS = "SUBMIT_SUCCESS"
        val SUBMIT_FAILED = "SUBMIT_FAILED"
    }

    override fun onStopJob(job: JobParameters): Boolean {
        disposable?.dispose()
        return false //no more work
    }

    override fun onStartJob(job: JobParameters?): Boolean {
       if(job?.extras?.containsKey(UploadModel.INTENT_KEY) == false) {
           //Do nothing and finish job there's no ID to associate with
           return false
       }

        val uploadJson = job?.extras?.getString(UploadModel.INTENT_KEY) ?: return false
        val app = applicationContext as App
        val upload = app.gson.fromJson(uploadJson, UploadModel::class.java)

        UploadAPI.configure()
        UploadAPI.getUserID()
                ?.subscribeOn(Schedulers.io())
                ?.flatMap { userID ->
                    return@flatMap UploadAPI.upload(userID, upload)
                }
                ?.map { result ->
                    //API is not restful so it is entirely possible the upload still failed
                    validate(result)
                }
                ?.map { result ->
                    val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
                    val upload = work.getRealmContext().find<SecchiReading>{ reading -> reading.primaryKey == job.tag }
                    if(upload == null) {
                        throw NullPointerException("Job has no ID")
                    }

                    upload.uploadStatus = UploadStatus.UPLOADED
                    work.getRealmContext().add(RealmTransaction(upload))
                    work.complete()
                }
                ?.subscribe({ result ->
                    broadcastSuccess(job.tag)
                    jobFinished(job, false)
                }, { ex ->
                    ex.printStackTrace()
                    broadcastError(job.tag)
                    jobFinished(job, false)
                })

        return true //more work to be done
    }

    private fun validate(result: ResponseBody) {
        val result = result.string()
        val sanitizedResult = result.replace("\"", "")
        if(sanitizedResult.equals("ok", true) == false) {
            throw IOException("Server response indicated failure: $sanitizedResult")
        }
    }

    private fun broadcastSuccess(primaryKey: String) {
        val intent = Intent()
        intent.putExtra(SUBMIT_SUCCESS, primaryKey)
        intent.action = UPLOAD_ACTION
        LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
    }

    private fun broadcastError(primaryKey: String) {
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val upload = work.getRealmContext().find<SecchiReading> { reading -> reading.primaryKey == primaryKey }
        if (upload != null) {
            upload.uploadStatus = UploadStatus.PENDING
            work.getRealmContext().add(RealmTransaction(upload))
            work.complete()

            val intent = Intent()
            intent.putExtra(SUBMIT_FAILED, primaryKey)
            intent.action = UPLOAD_ACTION
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
        }
    }
}