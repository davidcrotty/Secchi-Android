package uk.ac.plymouth.matmutt.secchi.domain;

/**
 * Created by David Crotty on 03/09/2017.
 * <p>
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
public interface UploadStatus {
    int PENDING = 0;
    int PROCESSING = 1;
    int UPLOADED = 2;
}
