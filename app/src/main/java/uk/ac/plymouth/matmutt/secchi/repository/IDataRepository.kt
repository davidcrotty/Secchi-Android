package uk.ac.plymouth.matmutt.secchi.repository

import io.realm.RealmObject
import io.realm.RealmQuery

/**
 * Created by David Crotty on 09/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
interface IDataRepository {
    fun <T : RealmObject>add(realmObject : T)
    fun <T : List<RealmObject>>addAll(objectList: T)
    fun <T : RealmObject>get(clazz: Class<T>, result: (RealmQuery<T>) -> T) : T?
    fun <T: RealmObject>update(realmObject: T, updateDelegate: (T) -> T)
}