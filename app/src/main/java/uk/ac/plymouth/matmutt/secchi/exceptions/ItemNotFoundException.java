//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.exceptions;

//**************************************************************************//
//An exception class thrown when searching for an tem that isn't there.		//
//**************************************************************************//
public class ItemNotFoundException extends Exception
{
	private static final long serialVersionUID = 1L;


	//**********************************************************************//
	// Constructors.														//
	//**********************************************************************//
	
	public ItemNotFoundException(String message)
	{
		super(message);
	}
	
}	// End of exceptional exception.
