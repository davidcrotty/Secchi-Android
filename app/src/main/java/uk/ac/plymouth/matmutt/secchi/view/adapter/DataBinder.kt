package uk.ac.plymouth.matmutt.secchi.view.adapter

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide
import uk.ac.plymouth.matmutt.secchi.view.components.CircleTransform
import java.io.File

/**
 * Created by David Crotty on 10/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String?) {
    if(url == null) return
    val file = File(url)
    val context = imageView.context
    Glide.with(context).load(file).transform(CircleTransform(imageView.context)).into(imageView)
}