package uk.ac.plymouth.matmutt.secchi.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.ContextThemeWrapper
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.network.PushNotificationService
import uk.ac.plymouth.matmutt.secchi.network.model.PushModel

/**
 * Created by David Crotty on 14/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class PushDialogListener {

    companion object {
        val SHOW_DIALOG_ACTION = "SHOW_DIALOG_ACTION"
        private var alertDialog: AlertDialog? = null
        private var activityContext: AppCompatActivity? = null
        private val dialogReceiver: BroadcastReceiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if(alertDialog?.isShowing == true) {
                    alertDialog?.dismiss()
                }

                val model = getModelFrom(intent) ?: return

                val builder = AlertDialog.Builder(ContextThemeWrapper(activityContext, R.style.PushAlert))
                        .setTitle(model.alertTitle)
                        .setMessage(model.alertBody)

                builder.takeIf { model.URL == null || model.buttonTitle == null } ?: let {
                    builder.setPositiveButton(model.buttonTitle, {x, y ->
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.data = Uri.parse(model.URL)
                        activityContext?.startActivity(intent)
                    })
                    builder.setNegativeButton("cancel", {x, y ->
                        alertDialog?.dismiss()
                    })
                }
                builder?.takeUnless { model.URL == null || model.buttonTitle == null } ?: let {
                    builder.setPositiveButton("ok", { x, y ->
                        alertDialog?.dismiss()
                    })
                }

                alertDialog = builder?.create()
                alertDialog?.show()
            }
        }

        fun register(activityContext: AppCompatActivity) {
            this.activityContext = activityContext
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(dialogReceiver,
                    IntentFilter(SHOW_DIALOG_ACTION))
        }

        fun unregister(activityContext: AppCompatActivity) {
            LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(dialogReceiver)
            alertDialog = null
            this.activityContext = null
        }

        private fun getModelFrom(intent: Intent?) : PushModel? {
            val intent = intent ?: return null
            if(intent.hasExtra(PushNotificationService.PUSH_MODEL_KEY)) {
                val model = intent.getSerializableExtra(PushNotificationService.PUSH_MODEL_KEY) as PushModel
                if(model.isValid() == true) {
                    return model
                }
            }

            return null
        }
    }
}