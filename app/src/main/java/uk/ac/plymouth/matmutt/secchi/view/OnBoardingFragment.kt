package uk.ac.plymouth.matmutt.secchi.view

import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_onboarding.*
import uk.ac.plymouth.matmutt.secchi.R
import android.view.WindowManager
import android.os.Build
import android.support.v4.content.ContextCompat
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.GPS
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.INTRO
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.PUSH_NOTIFICATIONS
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.UPLOAD
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.USER_INTERFACE


/**
 * Created by David Crotty on 15/11/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class OnBoardingFragment : Fragment() {

    companion object {

        val PAGE_NUMBER = "PAGE_NUMBER"

        fun newInstance(pageNumber: Int) : Fragment{
            val fragment = OnBoardingFragment()
            val bundle = Bundle().apply {
                this.putInt(PAGE_NUMBER, pageNumber)
            }
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.fragment_onboarding, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val resources = activity.resources
        val background = ContextCompat.getDrawable(activity, R.drawable.onboarding_circle)

        when(arguments.getInt(PAGE_NUMBER, -1)) {
            INTRO -> {
                onboarding_title.text = resources.getString(R.string.onboarding_title_1)
                onboarding_description.text = resources.getString(R.string.onboarding_description_1)
                content_image.setImageResource(R.drawable.onboarding_secchi_logo)
                background.colorFilter = PorterDuffColorFilter(
                        0xFF2980B9.toInt(),
                        PorterDuff.Mode.SRC_ATOP
                )
                if(android.os.Build.VERSION.SDK_INT >= 16) {
                    background_circle.background = background
                }
            }
            GPS -> {
                onboarding_title.text = resources.getString(R.string.onboarding_title_2)
                onboarding_description.text = resources.getString(R.string.onboarding_description_2)
                content_image.setImageResource(R.drawable.onboarding_gps)
                background.colorFilter = PorterDuffColorFilter(
                        0xFF20334C.toInt(),
                        PorterDuff.Mode.SRC_ATOP
                )
                if(android.os.Build.VERSION.SDK_INT >= 16) {
                    background_circle.background = background
                }
            }
            USER_INTERFACE -> {
                onboarding_title.text = resources.getString(R.string.onboarding_title_3)
                onboarding_description.text = resources.getString(R.string.onboarding_description_3)
                content_image.setImageResource(R.drawable.onboarding_ui)
                background.colorFilter = PorterDuffColorFilter(
                        0xFF00BC62.toInt(),
                        PorterDuff.Mode.SRC_ATOP
                )
                if(android.os.Build.VERSION.SDK_INT >= 16) {
                    background_circle.background = background
                }
            }
            UPLOAD -> {
                onboarding_title.text = resources.getString(R.string.onboarding_title_4)
                onboarding_description.text = resources.getString(R.string.onboarding_description_4)
                content_image.setImageResource(R.drawable.onboarding_upload)
                background.colorFilter = PorterDuffColorFilter(
                        0xFFFF4100.toInt(),
                        PorterDuff.Mode.SRC_ATOP
                )
                if(android.os.Build.VERSION.SDK_INT >= 16) {
                    background_circle.background = background
                }
            }
            PUSH_NOTIFICATIONS -> {
                onboarding_title.text = resources.getString(R.string.onboarding_title_5)
                onboarding_description.text = resources.getString(R.string.onboarding_description_5)
                content_image.setImageResource(R.drawable.onboarding_push)
                background.colorFilter = PorterDuffColorFilter(
                        0xFF650B96.toInt(),
                        PorterDuff.Mode.SRC_ATOP
                )
                if(android.os.Build.VERSION.SDK_INT >= 16) {
                    background_circle.background = background
                }
            }
            else -> {
                //NO-OP
            }
        }
    }
}