package uk.ac.plymouth.matmutt.secchi.domain

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import uk.ac.plymouth.matmutt.secchi.BuildConfig

/**
 * Created by David Crotty on 06/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
open class User : RealmObject() {
    @PrimaryKey
    var primaryKey: String? = null
    var APIID: String? = null //Not assumed to be unique and fully random
    var boatNameDefault: String = BuildConfig.BOAT_NAME_DEFAULT //Users last entered boat name preference
    var readWhatsNew: Boolean = false
}