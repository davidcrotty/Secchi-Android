//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Take notes Activity.														//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import io.realm.Realm;
import io.realm.RealmQuery;
import kotlin.jvm.functions.Function1;
import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork;
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;



//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class TakeNotesActivity extends Activity implements
					 						    OnClickListener
{

	public static String		  NOTES_INTENT_KEY = "NOTES_INTENT_KEY";
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName             = "";
	private Button                 buttonSave               = null; 
	private Button                 buttonCancel             = null;
	private EditText               editTextNotes            = null;
	private UnitOfWork unitOfWork = null;

	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_notes_activity);

        activityName           = getClass().getSimpleName();


        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        buttonSave     = (Button)   findViewById(R.id.buttonSave);
        buttonCancel   = (Button)   findViewById(R.id.buttonCancel);
        editTextNotes  = (EditText) findViewById(R.id.editTextNotes);
        
        buttonSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        editTextNotes.setOnClickListener(this);
		unitOfWork = new UnitOfWork(new RealmContext(Realm.getDefaultInstance()));
    }

   
    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		ApplicationState applicationState = unitOfWork.getRealmContext().getApplicationState();

		String notes = null;
		if(applicationState != null) {
			if (applicationState.getInProgressReading() != null) {
				notes = applicationState.getInProgressReading().getNotes();

				if (notes != null) {
					notes = "";
				}
			}
		}
		editTextNotes.setText(notes);
		
		//*********************************************************//
		// Blank the name if visiting for the first time.			//
		//*********************************************************//
		String blank = getString(R.string.noNotes);
		if (editTextNotes
				 .getText()
				 .toString()
				 .equalsIgnoreCase(blank))
			 			editTextNotes.setText("");

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		unitOfWork.complete();
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }


     
     
        
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		Intent intent = null;
 		
 		switch (view.getId())
 		{
 			case R.id.buttonSave:
 				 String notes = editTextNotes.getText().toString();
				 intent = new Intent();
				 intent.putExtra(NOTES_INTENT_KEY, notes);
 				 setResult(Activity.RESULT_OK);
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.buttonCancel:
 				 setResult(Activity.RESULT_CANCELED);
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.editTextNotes:
 				 break;
  		}
 	}



 

}	// End of classy class.
