//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Meet the team / who we are Activity.										//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2013.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import uk.ac.plymouth.matmutt.secchi.helpers.VesselParticularsX;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;



//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class MeetTheTeamActivity extends Activity implements
											      OnClickListener
{

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName             = "";
	private SecchiPackageVariables secchiPackageVariables   = null;
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.project_team_layout);
               
        secchiPackageVariables = SecchiPackageVariables.getInstance(this);
        activityName           = getClass().getSimpleName();


        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        ImageView bg        = (ImageView) findViewById(R.id.imageViewBackground);
        TextView  tvWebsite = (TextView)  findViewById(R.id.textViewPlanktonPundit);
        bg.setOnClickListener(this);
        tvWebsite.setOnClickListener(this);
    }

   
    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiPackageVariables.release();
	   
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }


     
     
        
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		Intent intent = null;
 		
 		switch (view.getId())
 		{
 			case R.id.buttonSave:
 				 EditText etBoat = (EditText) findViewById(R.id.editTextBoatName);
 				 VesselParticularsX vesselParticulars = 
 						 	secchiPackageVariables.getVesselParticulars();
 				 vesselParticulars.setName(etBoat.getText().toString());
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.buttonCancel:
 				 finish();
 				 break;		// Unreachable.
 				
 				 
 			//**************************************************************//
 			// Fire up website.												//
 			//**************************************************************//
 			case R.id.textViewPlanktonPundit:
 				String url    = getString(R.string.planktonPunditWebsite);
 				String select = getString(R.string.selectBrowser);
 				intent     = new Intent(Intent.ACTION_VIEW);
 				intent.setData(Uri.parse(url));
 				startActivity(Intent.createChooser(intent, select));
 					
 				break;		// Unreachable.

 			//**************************************************************//
 			// Default is the viewI itself.									//
 			//**************************************************************//
 			default:
				 finish();
				 break;		// Unreachable.  			
 		}
 		
 	}



 

}	// End of classy class.
