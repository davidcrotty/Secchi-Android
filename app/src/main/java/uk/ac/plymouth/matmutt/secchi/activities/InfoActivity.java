//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Info / who we are Activity.												//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2013.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import uk.ac.plymouth.matmutt.secchi.helpers.VesselParticularsX;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;



//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class InfoActivity extends Activity implements
										   OnClickListener
{

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName             = "";
	private SecchiPackageVariables secchiPackageVariables   = null;
	private ImageButton            buttonWhoWeAre           = null;
	private ImageButton            buttonEmail              = null;
	private ImageButton            buttonWebSite            = null;
	private ImageButton            buttonFaceBook           = null;
	
	
	//**********************************************************************//
	// Constants.															//
	//**********************************************************************//
	private final String emailSecchi    = "contact@secchidisk.org";
	private final String webSiteSecchi  = "http://www.secchidisk.org";
	private final String faceBookSecchi = "http://www.facebook.com/secchidisk";
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.who_we_are_layout);
               
        secchiPackageVariables = SecchiPackageVariables.getInstance(this);
        activityName           = getClass().getSimpleName();

        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        buttonWhoWeAre = (ImageButton) findViewById(R.id.buttonWhoWeAre);
        buttonEmail    = (ImageButton) findViewById(R.id.buttonEmail);
        buttonWebSite  = (ImageButton) findViewById(R.id.buttonWWWSecchiDisk);
        buttonFaceBook = (ImageButton) findViewById(R.id.buttonFindUsOnFaceBook);
        
        buttonWhoWeAre.setOnClickListener(this);
        buttonEmail.setOnClickListener(this);
        buttonWebSite.setOnClickListener(this);
        buttonFaceBook.setOnClickListener(this);
    }

   
    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiPackageVariables.release();
	   
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
    	MenuInflater inflater = getMenuInflater();
     	inflater.inflate(R.menu.info_menu, menu);
     	
     	return true;
     }


     
     
 	 //*******************************************************************//
     // Another overridden method of an Activity.  It happens when we 	 //
     // press the "menu" button or use the action bar.					 //
     //*******************************************************************//
     @Override
     public boolean onOptionsItemSelected(MenuItem item) 
     {
     	int    itemId = item.getItemId();
        	
     	switch (itemId)
     	{
     		case R.id.menuAppInfo:
     			 String versionName     = secchiPackageVariables.getVersionName();
     			 String version         = getString(R.string.versionCondensed);
     			 AlertDialog.Builder al = new AlertDialog.Builder(this);
     			 
     			 al.setTitle(R.string.app_name);
     			 al.setMessage(version + " " + versionName);
     			 al.setPositiveButton(R.string.OK, null); 
     			 al.show();
     			 break;
     			 
     	}
     	return true;
     }

     
     
        
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		Intent intent = null;
 		String title  = null;
 		
 		switch (view.getId())
 		{
 			case R.id.buttonWhoWeAre:
 				 intent = new Intent(this, MeetTheTeamActivity.class);
 				 startActivity(intent);
 				 break;		
 				 
 			case R.id.buttonEmail:
 				 String[] to    = new String[1];
 				 to[0]          = emailSecchi;
 				 String subject = "";
 				 String content = "";
 				 
 				 Enumerators.shareAsEmail(this, to, subject, content, null);
 				 break;		// Unreachable.
 	
 				 
 			case R.id.buttonWWWSecchiDisk:
 				 title = getString(R.string.viewSecchiWebSite);
 				 Enumerators.shareWithWebSite(this, title, webSiteSecchi);
 				 break;
 				 
 				 
 			case R.id.buttonFindUsOnFaceBook:
 				 title = getString(R.string.viewSecchiOnFacebook);
 				 Enumerators.shareWithWebSite(this, title, faceBookSecchi);
 				 break;
  		}
 	}



 

}	// End of classy class.
