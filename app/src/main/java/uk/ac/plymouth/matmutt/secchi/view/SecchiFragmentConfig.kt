package uk.ac.plymouth.matmutt.secchi.view

import android.os.Bundle
import android.support.v7.app.AppCompatDialogFragment
import uk.ac.plymouth.matmutt.secchi.view.model.Experiment

/**
 * Created by David Crotty on 25/10/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
abstract class SecchiFragmentConfig {
    companion object {
        val TAG = "SecchiDialog"
        val TITLE = "title"
        val DESCRIPTION = "description"
        val POSITIVE_TEXT = "positive"
        val NEGATIVE_TEXT = "negative"
        val EXPERIMENT = "experiment"
    }

    open fun newInstance(title: String,
                             description: String,
                             positiveText: String,
                             negativeText: String,
                             fragment: AppCompatDialogFragment) : AppCompatDialogFragment {
        val bundle = Bundle()
        bundle.putString(TITLE, title)
        bundle.putString(DESCRIPTION, description)
        bundle.putString(POSITIVE_TEXT, positiveText)
        bundle.putString(NEGATIVE_TEXT, negativeText)
        fragment.arguments = bundle
        return fragment
    }

    open fun newInstance(title: String,
                         description: String,
                         positiveText: String,
                         negativeText: String,
                         experiment: Experiment,
                         fragment: AppCompatDialogFragment) : AppCompatDialogFragment {
        val bundle = Bundle()
        bundle.putString(TITLE, title)
        bundle.putString(DESCRIPTION, description)
        bundle.putString(POSITIVE_TEXT, positiveText)
        bundle.putString(NEGATIVE_TEXT, negativeText)
        bundle.putParcelable(EXPERIMENT, experiment)
        fragment.arguments = bundle
        return fragment
    }
}