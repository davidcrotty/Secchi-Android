package uk.ac.plymouth.matmutt.secchi

import android.app.Application
import com.google.gson.Gson
import io.realm.Realm
import io.realm.RealmConfiguration
import timber.log.Timber

/**
 * Created by David Crotty on 22/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class App : Application() {

    val gson: Gson by lazy { Gson() }
    var acknowledgedRoamingWarning : Boolean = false

    override fun onCreate() {
        super.onCreate()

        if(BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        Realm.init(this)
        val config = RealmConfiguration.Builder()
                .schemaVersion(5)
                .deleteRealmIfMigrationNeeded()
                .build()
        Realm.setDefaultConfiguration(config)
    }
}