package uk.ac.plymouth.matmutt.secchi

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import uk.ac.plymouth.matmutt.secchi.service.GPSService
import uk.ac.plymouth.matmutt.secchi.service.GPSService.Companion.GPS_BROADCAST

/**
 * Created by David Crotty on 28/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class NotificationReceiver : BroadcastReceiver() {

    private val TAG = "NotificationReceiver"


    override fun onReceive(context: Context?, intent: Intent?) {
        if(intent?.hasExtra(GPSService.NOTIFICATION_DISMISSED) == false) return
        LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(GPSService.NOTIFICATION_DISMISSED)) //need cancel to hide spinner
    }
}