package uk.ac.plymouth.matmutt.secchi.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.GooglePlayDriver
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_upload.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import timber.log.Timber
import uk.ac.plymouth.matmutt.secchi.App
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState
import uk.ac.plymouth.matmutt.secchi.domain.SecchiReading
import uk.ac.plymouth.matmutt.secchi.domain.UploadStatus
import uk.ac.plymouth.matmutt.secchi.domain.User
import uk.ac.plymouth.matmutt.secchi.network.UploadService
import uk.ac.plymouth.matmutt.secchi.presenter.UploadActivityPresenter
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import uk.ac.plymouth.matmutt.secchi.view.DialogProvider
import uk.ac.plymouth.matmutt.secchi.view.IUploadView
import uk.ac.plymouth.matmutt.secchi.view.PushDialogListener
import uk.ac.plymouth.matmutt.secchi.view.adapter.IAdaptable
import uk.ac.plymouth.matmutt.secchi.view.adapter.UploadAdapter
import uk.ac.plymouth.matmutt.secchi.view.model.Experiment
import uk.ac.plymouth.matmutt.secchi.view.model.Heading
import uk.ac.plymouth.matmutt.secchi.view.model.UploadItem
import java.util.*


class UploadActivity : AppCompatActivity(), IUploadView, DialogProvider.RoamingWarningDialogListener {

    private val dateFormat = DateTimeFormat.forPattern("HH:mm")
    private var adapter: UploadAdapter? = null
    private var presenter: UploadActivityPresenter? = null
    private var uploadReceiver: BroadcastReceiver? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = resources.getString(R.string.upload_title)
        presenter = UploadActivityPresenter(FirebaseJobDispatcher(GooglePlayDriver(this)), applicationContext as App)

        edit_button.setOnClickListener {
            adapter?.toggleEdit()
            if(adapter?.canEdit == true) {
                edit_button.text = resources.getString(R.string.done_edit_upload)
            } else {
                edit_button.text = resources.getString(R.string.edit_upload)
            }
        }

        uploadReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if(intent?.hasExtra(UploadService.SUBMIT_SUCCESS) == true) {
                    Timber.d("Upload succeeded")
                    updateAdapter()
                } else {
                    Timber.d("Upload failed")
                    Snackbar.make(root, "Upload failed", Snackbar.LENGTH_SHORT).show()
                    updateAdapter()
                }
            }
        }

        LocalBroadcastManager.getInstance(this).registerReceiver(uploadReceiver,
                IntentFilter(UploadService.UPLOAD_ACTION))
        //Comment below method in to add test data for uploading
//        seedData()
    }

    override fun renderRoamingPrompt(experiment: Experiment, isEditable: Boolean) {
        DialogProvider.showRoamingWith( supportFragmentManager,
                getString(R.string.roaming_title),
                getString(R.string.roaming_description),
                getString(R.string.roaming_positive),
                getString(R.string.roaming_negative),
                experiment)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                processEditFunctionality()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun finish() {
        val uploadReceiver = uploadReceiver ?: return
        LocalBroadcastManager.getInstance(this).unregisterReceiver(uploadReceiver)
        super.finish()
    }

    override fun continueUpload(experiment: Experiment) {
        adapter?.notifyDataSetChanged()
        val app = application as App
        app.acknowledgedRoamingWarning = true
        presenter?.upload(experiment)
    }

    override fun dontUpload() {
    }

    private fun processEditFunctionality() {
        if(adapter?.canEdit == true) {
            adapter?.toggleEdit()
            edit_button.text = resources.getString(R.string.edit_upload)
        } else {
            finish()
        }
    }

    private fun seedData() {
        var work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val reading1 = SecchiReading()
        reading1.primaryKey = UUID.randomUUID().toString()
        reading1.depth = 2.0f
        reading1.timeOfLocation = DateTime.now().millis
        reading1.temperature = 5.0f
        reading1.timeOfTemperature = DateTime.now().millis
        reading1.vesselName = "test1"
        reading1.timeOfDepth = DateTime.now().millis
        reading1.lattiutde = 1f
        reading1.longitude = 2f

        val reading2 = SecchiReading()
        reading2.primaryKey = UUID.randomUUID().toString()
        reading2.depth = 2.0f
        reading2.timeOfLocation = DateTime.now().millis
        reading2.temperature = 5.0f
        reading2.timeOfTemperature = DateTime.now().millis
        reading2.timeOfDepth = DateTime.now().millis
        reading2.vesselName = "test2"
        reading2.lattiutde = 1f
        reading2.longitude = 2f

        work.getRealmContext().add(RealmTransaction(reading1, reading2))
        work.complete()

        work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val state = ApplicationState()
        val user = User()
        user.primaryKey = UUID.randomUUID().toString()
        state.user = user

        work.getRealmContext().add(RealmTransaction(state))
        work.complete()
    }

    override fun onResume() {
        super.onResume()
        updateAdapter()
        PushDialogListener.register(this@UploadActivity)
    }

    override fun onPause() {
        if(adapter?.canEdit == true) {
            adapter?.toggleEdit()
            edit_button.text = resources.getString(R.string.edit_upload)
        }
        super.onPause()
        PushDialogListener.unregister(this@UploadActivity)
    }

    fun onClick(experiment: Experiment, isEditable: Boolean) {
        val app = application as App
        presenter?.showUploadState(experiment = experiment,
                isEditable = isEditable,
                adapter = adapter,
                uploadView = this,
                context = this,
                roamingWarningAcknowledged = app.acknowledgedRoamingWarning)
    }

    override fun onBackPressed() {
        processEditFunctionality()
    }

    private fun updateAdapter() {
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val readingList = work.getRealmContext().findAll<SecchiReading>({reading -> reading.isValidModel()})

        val uploadItemList = ArrayList<UploadItem>()
        populateFrom(readingList, uploadItemList)
        work.complete()
        adapter = UploadAdapter(uploadItemList as ArrayList<IAdaptable>, this, presenter!!)
        uploads_list.layoutManager = LinearLayoutManager(this)
        uploads_list.adapter = adapter
    }

    private fun populateFrom(copy: List<SecchiReading>?, to: ArrayList<UploadItem>){
        var hasCreatedSubmittedHeading = false
        to.add(UploadItem(
                Heading(resources.getString(R.string.pending_heading),
                        ContextCompat.getColor(this, R.color.pending_blue),
                        false))
        )
        if(copy == null) return
        val sorted = copy.sortedWith(compareBy { it.uploadStatus == UploadStatus.UPLOADED })
        for(item in sorted) {
            val dateTime = DateTime(item.timeOfDepth)
            val printedDate = "${dateTime.dayOfMonth().asText}${getDateSuffix(dateTime.dayOfMonth().get())}" +
                    " ${dateTime.monthOfYear().asShortText}" +
                    " ${dateTime.year().asText}" +
                    " - ${dateFormat.print(dateTime)}"
            if(item.uploadStatus == UploadStatus.UPLOADED && hasCreatedSubmittedHeading == false) {
                addUploadHeading(to)
                hasCreatedSubmittedHeading = true
            }

            to.add(UploadItem(Experiment(item.primaryKey!!,
                    printedDate,
                    item.vesselName,
                    item.temperature.toString(),
                    item.depth.toString(),
                    item.uploadStatus,
                    item.photoPath)
                )
            )
        }

        if(hasCreatedSubmittedHeading == false) {
            addUploadHeading(to)
        }
    }

    private fun addUploadHeading(to: ArrayList<UploadItem>) {
        to.add(UploadItem(
                Heading(resources.getString(R.string.uploaded_heading),
                        ContextCompat.getColor(this, R.color.upload_green),
                        true))
        )
    }

    private fun getDateSuffix(dayOfMonth: Int) : String {
        when(dayOfMonth % 10) {
            1 -> {
                return "st"
            }
            2 -> {
                return "nd"
            }
            3 -> {
                return "rd"
            }
            else -> {
                return "th"
            }
        }
    }
}
