package uk.ac.plymouth.matmutt.secchi.repository.realm

import io.realm.RealmObject

/**
 * Created by David Crotty on 24/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class RealmTransaction<T : RealmObject> {

    val holder = ArrayList<T>()

    constructor(vararg itemList : T) {
        holder.addAll(itemList)
    }

    constructor(itemList : List<T>) {
        holder.addAll(itemList)
    }

    fun add(item: T) {
        holder.add(item)
    }

    fun remove(item: T) {
        holder.remove(item)
    }
}