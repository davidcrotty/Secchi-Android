package uk.ac.plymouth.matmutt.secchi.network

import io.reactivex.Single
import io.realm.Realm
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.network.model.UploadModel
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import java.io.File
import retrofit2.http.Part


/**
 * Created by David Crotty on 05/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
object UploadAPI {

    var configured: Boolean = false
    var webAPI: UploadWebAPI? = null

    fun configure() {
       if(configured) return
       val logging = HttpLoggingInterceptor()
       if(BuildConfig.DEBUG) {
           logging.level = HttpLoggingInterceptor.Level.BODY
       } else {
           logging.level = HttpLoggingInterceptor.Level.NONE
       }

       val rxAdapter = RxJava2CallAdapterFactory.create()
       val httpClient =  HttpClient.createTimeoutInterceptorWith(BuildConfig.API_TIMEOUT_SECONDS)
               .addInterceptor(logging)
       webAPI = Retrofit.Builder()
               .baseUrl(BuildConfig.API_HOST)
               .addConverterFactory(ScalarsConverterFactory.create())
               .addCallAdapterFactory(rxAdapter)
               .client(httpClient.build())
               .build()
               .create(UploadWebAPI::class.java)
        configured = true
    }

    fun upload(userID: String, upload: UploadModel) : Single<ResponseBody>? {

        var body: MultipartBody.Part? = null
        if(upload.photoPath.isNullOrEmpty()) {

        } else {
            val file = File(upload.photoPath)
            if(file.exists()) {
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                body = MultipartBody.Part.createFormData("file", file.name, requestFile)
            }
        }

        return webAPI?.submit(userID, userID, upload.boatName, userID, upload.gpsDay, upload.gpsMonth, upload.gpsYear,
                upload.gpsHour, upload.gpsMin, upload.gpsSec, upload.lattitude, upload.longitude, upload.depthDay,
                upload.depthMonth, upload.depthYear, upload.depthHour, upload.depthMin, upload.depthSec,
                upload.depth, upload.temperatureDay, upload.temperatureMonth, upload.temperatureYear, upload.temperatureHour,
                upload.temperatureMin, upload.temperatureSec, upload.notes, upload.temperature, body)
    }

    fun getUserID() : Single<String>? {
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val user = work.getRealmContext().getApplicationState()?.user ?: throw NullPointerException("No user found")
        val APIID = user.APIID
        work.complete()

        if(APIID == null) {
            return webAPI?.getUserID()?.flatMap { response ->
                val sanitizedResponse = response.string().replace("\"", "")
                //get a new user due to Realm limitations
                val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
                val user = work.getRealmContext().getApplicationState()?.user ?: throw NullPointerException("No user found")
                user.APIID = sanitizedResponse
                work.getRealmContext().add(RealmTransaction(user))
                work.complete()
                return@flatMap Single.just(sanitizedResponse)
            }
        } else {
            return Single.just(APIID)
        }
    }
}