package uk.ac.plymouth.matmutt.secchi.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_onboarding.*
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.domain.User
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.GPS
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.INTRO
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.PUSH_NOTIFICATIONS
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.UPLOAD
import uk.ac.plymouth.matmutt.secchi.view.components.OnboardingPager.Companion.USER_INTERFACE

/**
 * Created by David Crotty on 15/11/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class OnBoardingActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding)
        setStatusColour(ContextCompat.getColor(this, R.color.colour_primary_dark))
        window.decorView.setBackgroundColor(
                ContextCompat.getColor(this, R.color.colour_primary)
        )

        onboarding_pager.adapter = OnboardingPager(supportFragmentManager)
        onboarding_pager.addOnPageChangeListener(this)

        page_indicator.setViewPager(onboarding_pager)

        skip_button.setOnClickListener({
            launchMainMenu()
        })
        begin_button.setOnClickListener({
            launchMainMenu()
        })
    }

    private fun launchMainMenu() {
        val user = Realm.getDefaultInstance().where(User::class.java).findFirst() ?: return
        Realm.getDefaultInstance().beginTransaction()
        user.readWhatsNew = true
        Realm.getDefaultInstance().insertOrUpdate(user)
        Realm.getDefaultInstance().commitTransaction()
        Realm.getDefaultInstance().close()
        startActivity(Intent(this, MainMenuActivity::class.java))
    }

    private fun setStatusColour(resourceId: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resourceId
        }
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {

        if(position == PUSH_NOTIFICATIONS) {
            skip_button.visibility = View.GONE
            begin_button.visibility = View.VISIBLE
        } else {
            skip_button.visibility = View.VISIBLE
            begin_button.visibility = View.INVISIBLE
        }

        when(position) {
            INTRO -> {
                setStatusColour(ContextCompat.getColor(this, R.color.colour_primary_dark))
                window.decorView.setBackgroundColor(
                        ContextCompat.getColor(this, R.color.colour_primary)
                )
            }
            GPS -> {
                setStatusColour(ContextCompat.getColor(this, R.color.onboarding_gps_primary_dark))
                window.decorView.setBackgroundColor(
                        ContextCompat.getColor(this, R.color.onboarding_gps_primary)
                )
            }
            USER_INTERFACE -> {
                setStatusColour(ContextCompat.getColor(this, R.color.onboarding_ui_primary_dark))
                window.decorView.setBackgroundColor(
                        ContextCompat.getColor(this, R.color.onboarding_ui_primary)
                )
            }
            UPLOAD -> {
                setStatusColour(ContextCompat.getColor(this, R.color.onboarding_upload_primary_dark))
                window.decorView.setBackgroundColor(
                        ContextCompat.getColor(this, R.color.onboarding_upload_primary)
                )
            }
            PUSH_NOTIFICATIONS -> {
                setStatusColour(ContextCompat.getColor(this, R.color.onboarding_push_primary_dark))
                window.decorView.setBackgroundColor(
                        ContextCompat.getColor(this, R.color.onboarding_push_primary)
                )
            }
        }
    }
}