package uk.ac.plymouth.matmutt.secchi.view.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.os.Build
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import kotlinx.android.synthetic.main.card_container.view.*
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.activities.MainMenuActivity


/**
 * Created by David Crotty on 10/06/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class NotificationCardView : RelativeLayout {

    private val TAG = "NotificationCardView"
    private var cardTop: Float? = null
    private var cardRight: Float? = null
    private var circlePaint: Paint? = null
    private var textPaint: Paint? = null
    private var numberOfPagesRemaining: String? = null
    private var cardPadding: Int? = null
    private val UNICODE_TICK = "\u2713"
    private var shouldDrawNotification = true

    constructor(context: Context) : this(context, null)

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        View.inflate(context, R.layout.card_container, this)
        initialise()
    }

    fun hideNotification() {
        pages_remaining.visibility = GONE
        done_image.visibility = GONE
        floating_action_button.visibility = GONE
        shouldDrawNotification = false
        invalidate()
    }

    fun notificationNumber(pages: Int) {
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            done_image.visibility = GONE
            floating_action_button.visibility = View.VISIBLE
            pages_remaining.visibility = VISIBLE
        }
        pages_remaining.text = pages.toString()
        numberOfPagesRemaining = pages.toString()
        invalidate()
    }

    fun showNotificiationTick() {
        if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            floating_action_button.visibility = View.VISIBLE
            pages_remaining.visibility = GONE
            done_image.visibility = VISIBLE
        }
        if(android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.KITKAT) {
            numberOfPagesRemaining = "\u2713"
        }
        invalidate()
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        if (changed) {
            val padding = cardPadding ?: return
            for (i in 0..childCount) {
                val child = getChildAt(i) ?: return
                if (child.id == R.id.card_container) {
                    continue
                } else {
                    child.setPadding(padding, padding, padding, 1)
                }
            }
        }
    }

    override fun dispatchDraw(canvas: Canvas?) {
        super.dispatchDraw(canvas)
        if(android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.KITKAT) return
        if(shouldDrawNotification == false) return

        canvas?.drawCircle(cardRight!!, cardTop!!, calculateCircleHeight(), circlePaint)

        val textToRender = if(numberOfPagesRemaining == "0") {
            UNICODE_TICK
        } else {
            " " + numberOfPagesRemaining.toString()
        }

        canvas?.drawText(textToRender,
                cardRight!! - resources.getDimension(R.dimen.notification_padding_right),
                cardTop!! + resources.getDimension(R.dimen.notification_padding_top),
                textPaint)
    }

    /**
     * Overriding super classes enabled causes a delay in rendering, bug in super class implm
     */
    fun disableWidget() {
        card.setCardBackgroundColor(Color.WHITE)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            card.alpha = MainMenuActivity.ALPHA_VISIBLE
        }
        invalidate()
    }

    fun enableWidget() {
        card.setCardBackgroundColor(Color.TRANSPARENT)
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            card.alpha = MainMenuActivity.ALPHA_INVISIBLE
        }
        invalidate()
    }

    private fun initialise() {
        cardPadding = resources.getDimensionPixelSize(R.dimen.notification_margin)

        if(android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.KITKAT) {
            done_image.visibility = GONE
            floating_action_button.visibility = GONE
            pages_remaining.visibility = GONE

            circlePaint = Paint()
            circlePaint?.color = ContextCompat.getColor(context, R.color.colour_primary)
            circlePaint?.style = Paint.Style.FILL
            circlePaint?.isAntiAlias = true

            textPaint = Paint()
            textPaint?.color = ContextCompat.getColor(context, android.R.color.white)
            textPaint?.style = Paint.Style.FILL
            textPaint?.isAntiAlias = true
            textPaint?.textSize = resources.getDimension(R.dimen.notification_text_size)

            card.viewTreeObserver.addOnGlobalLayoutListener {
                //unable to remove listener as this is not supported on < 14
                val padding = resources.getDimension(R.dimen.notification_padding_top)
                cardTop = card.top.toFloat() + padding
                cardRight = card.right.toFloat() - padding
            }
        }
    }

    private fun calculateCircleHeight() : Float {
        return resources.getDimension(R.dimen.notification_margin)
    }

}