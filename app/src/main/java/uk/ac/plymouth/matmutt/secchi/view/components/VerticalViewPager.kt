package uk.ac.plymouth.matmutt.secchi.view.components

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent


/**
 * Created by David Crotty on 20/08/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class VerticalViewPager(context: Context,
                        transformer: VerticalPageTransformer) : ViewPager(context) {

    init {
        setPageTransformer(true, transformer)
        overScrollMode = OVER_SCROLL_NEVER
    }

    /**
     * Swaps the X and Y coordinates of your touch event.
     */
    private fun swapXY(ev: MotionEvent): MotionEvent {
        val width = width.toFloat()
        val height = height.toFloat()

        val newX = ev.y / height * width
        val newY = ev.x / width * height

        ev.setLocation(newX, newY)

        return ev
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        val intercepted = super.onInterceptTouchEvent(swapXY(ev))
        swapXY(ev) // return touch coordinates to original reference frame for any child views
        return intercepted
    }

    override fun onTouchEvent(ev: MotionEvent): Boolean {
        return super.onTouchEvent(swapXY(ev))
    }
}