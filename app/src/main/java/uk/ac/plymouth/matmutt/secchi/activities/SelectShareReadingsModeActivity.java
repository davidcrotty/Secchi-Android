//**************************************************************************//
// Nigel's attempt at a simple Marine GPS.									//
//																			//
// A megga-simple Activity that allows the user to share their data.        //
//																			//
// You will see that I am not using the "JavaDoc" style of comments, as this//
// program isn't intended to be self-documenting.  When you write your code,//
// I suggest that you do indeed use the "JavaDoc" type comments.			//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.activities;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StreamCorruptedException;
import java.io.Writer;
import java.util.ArrayList;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiReadingX;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Toast;

public class SelectShareReadingsModeActivity extends    Activity
                                             implements OnClickListener, 
                                                        OnCheckedChangeListener
{
	//**********************************************************************//
	// Enums.																//
	//**********************************************************************//
	public enum ShareAction {email, googleEarth, KML, nothing};

	
	//*********************************************************************//
	// Instance variables.												   //
	//*********************************************************************//
	private SecchiPackageVariables secchiPackageVariables = null; 
	private String                 activityName           = "";
	private ShareAction            shareAction            = ShareAction.email;		//Must be consistent with radio buttons default.
	private volatile boolean       exportThreadRunning    = false;
	private boolean                exportDepths    		  = false;
	private boolean                exportNotes     		  = false;
	private final String           exportFileName		  = "readings.kml";
	
	//**********************************************************************//
	// Roy's Threading variables.											//
	//**********************************************************************//
	private ExportThread   exportThread        = null;;
	private boolean        blnProcessingResult = false;
	private String 		   strDestFileName     = "";

	
	
	//**********************************************************************//
	//As the name suggests; called when the activity is first created. 		//
	//**********************************************************************//
	@Override
    public void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share_readings_mode_layout);
        activityName           = getClass().getSimpleName();

        //******************************************************************//
    	// This is how we get the components from the declarative layout.	//
        //******************************************************************//
        Button   buttonShare  = (Button)   findViewById(R.id.buttonShare);
        Button   buttonCancel = (Button)   findViewById(R.id.buttonCancel);
        buttonShare.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);

        RadioButton radioButtonShareByEmail     = (RadioButton) findViewById(R.id.radioButtonShareReadingsByEmail);
        RadioButton radioButtonShareByGoogleUrf = (RadioButton) findViewById(R.id.radioButtonShareReadingsByGoogleEarth);
        
        radioButtonShareByEmail.setOnCheckedChangeListener(this);
        radioButtonShareByGoogleUrf.setOnCheckedChangeListener(this);
       	}
	
	
	
	
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
	// If we are to avoid flattening the battery, we must remove the status	 //
	// listeners.  It is possible that we might want to keep logging position//
	// at a reduced rate, though.											 //
	//***********************************************************************//
	@Override
	public void onPause()
   {
	   super.onPause();
	   secchiPackageVariables.release();
	   if (D.debug) Log.i(activityName, "Activity paused");
   }

	
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
		
  	    //Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
	
		setTitle(getString(R.string.shareSecchiReadings));
	}

	
	
	//***********************************************************************//
	// Back button pressed.  Just quit (finish) the activity.  We don't want //
	// to leave it knocking about in memory.								 //
	//***********************************************************************//
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		if (exportThreadRunning) 
		{
			exportThreadRunning = false;	//kill off
			exportThread        = null;		//export Thread.
		}
		finish();
	}

	
	
	
	//**********************************************************************//
	// Overriden from OnClickListener.										//
	//**********************************************************************//
	@Override
	public void onClick(View view) 
	{
		//******************************************************************//
		// Which button was it?												//
		//******************************************************************//
		switch (view.getId()) 
		{
		case R.id.buttonCancel:
			 if (exportThreadRunning) 
			 {
				exportThreadRunning = false;	//Kill off the
				exportThread        = null;		//export Thread.
				blnProcessingResult = false;	//Belt and braces.
			 }
			 finish();
			 break;		//Unreachable
			 
		case R.id.buttonShare:
			 makeKML();
			 break;
		}
	}
	
	

	
	//**********************************************************************//
	// CallBack for all radio buttons.                                      //
	//**********************************************************************//
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) 
	{
		switch (buttonView.getId())
		{
			//**************************************************************//
			// Get the radio buttons.  Hopefully one of these buttons will  //
			// always be checked.											//
			//**************************************************************//
			case R.id.radioButtonShareReadingsByEmail:
				 if (isChecked) shareAction = ShareAction.email; 
				 break;
				 
			case R.id.radioButtonShareReadingsByGoogleEarth:
				 if (isChecked) shareAction = ShareAction.googleEarth;
				 break;
		}
	}
	
	

	
	//**********************************************************************//
	// Share as email.														//
	//																		//
	// This is megga easy except that not all the intent extra this, that   //
	// and the other are intuitively named.									//
	//																		//
	// Intent.ACTION_SEND does not fire up email particularly, it fires up	//
	// any application that knows what to do with the action we specify.	//
	//**********************************************************************//
	public void shareAsEmail()
	{
		//******************************************************************//
		// Prepare strings for to (not used), subject, content and such.	//
		//******************************************************************//
		String[] to      = new String[]{""};
		String   subject = getString(R.string.shareReadingsEmailSubject); 
		String   content = getString(R.string.shareReadingsEmailContent);

		Enumerators.shareAsEmail(this, to, subject, content, strDestFileName);
       	}
	
	
	
	
	//**********************************************************************//
	// Share with Google Urf.												//
	//																		//
	// Intent.ACTION_SEND does not fire up email particularly, it fires up	//
	// any application that knows what to do with the action we specify.	//
	//																		//
	// Now Wirking; functionality in Enumerators.							//
	//**********************************************************************//
	private void shareWithGoogleEarth()
	{
		Enumerators.shareWithGoogleEarth(this, strDestFileName);
	}
	

	
	//**********************************************************************//
	// Make a progress bar and start the Thread that does the processing    //
	//																		//
	// Also grey out the share button to prevent repeatedly pressing it.	//
	//**********************************************************************//
	private void makeKML()
	{
		Button   buttonShare                 = (Button)   findViewById(R.id.buttonShare);
		CheckBox checkBoxExportWaypointNames = (CheckBox) findViewById(R.id.checkBoxExportReadingsNames);
		CheckBox checkboxExportWaypointDesc  = (CheckBox) findViewById(R.id.checkBoxExportReadingsNotes);
		exportDepths = checkBoxExportWaypointNames.isChecked();
		exportNotes  = checkboxExportWaypointDesc.isChecked();
		
		buttonShare.setEnabled(false);
		ProgressBar progressBarShare = (ProgressBar) findViewById(R.id.progressBarShare);
		progressBarShare.setVisibility(ProgressBar.VISIBLE);
		
		exportThread = new ExportThread(objHandler);
		exportThread.start();
	}
	
	
	
	
	
	
	//***********************************************************************************//
	// Handler for the KML Export Dialog.  This looks like an anonymous inner class, but //
	// Roy wrote it.  Can this be true?													 //
	//                                                                                   //
	// So far as I can see, this is just to get the result of the export back into the   //
	// user Thread.  If the export failed, any file it created is deleted.				 //
	//***********************************************************************************//
	private Handler objHandler = new Handler() 
	{
		//*******************************************************************************//
		// We must override this one, for pretty obvious reasons.						 //
		//*******************************************************************************//
		@Override
		public void handleMessage(Message msg) 
		{
			String strResult = msg.getData().getString("Complete");
			
			if ((shareAction == ShareAction.KML) && (blnProcessingResult))
			{
				Toast.makeText(getApplicationContext(), strResult,
							   Toast.LENGTH_SHORT).show();
			}
			

			//****************************************************************************//
			// Hide the progress bar.													  //
			//****************************************************************************//
			ProgressBar progressBarShare = (ProgressBar) findViewById(R.id.progressBarShare);
			progressBarShare.setVisibility(ProgressBar.GONE);

			
			//****************************************************************************//
			// Remove any file we created.												  //
			//****************************************************************************//
			if (!blnProcessingResult)
			{
				Toast.makeText(getApplicationContext(), strResult, Toast.LENGTH_SHORT).show();
				try
				{
					File toDelete = new File(strDestFileName);				
					toDelete.delete();
				}
				catch (Exception doh)
				{
					if (D.debug) Log.e(activityName, "Couldn't delete file after export cancelled " 
				                        + doh.getMessage());
				}
			}
			else
			//**************************************************************************//
			// If we get this far we can share by email.  We can only do that if the 	//
			// KML file has been created.												//
			//**************************************************************************//
			switch (shareAction)
			{
				case email:
					 shareAsEmail();
					 break;
					 
				case googleEarth:
					 shareWithGoogleEarth();
					 break;
			}

			//**************************************************************************//
			// Make the share button visible again.										//
			//**************************************************************************//
			Button buttonShare = (Button) findViewById(R.id.buttonShare);
			buttonShare.setEnabled(true);
		}
	};	//End of inner class.

	
	
	
	
	// **********************************************************************************//
	// Thread Class to perform Export.  This is a Roy class, which I find strange as I   //
	// know Roy's views on inner classes - Nigel.	In fact, that's two inner classes.	 //
	// **********************************************************************************//
	private class ExportThread extends Thread 
	{
		private Handler mHandler             = null;;

		
		//******************************************************************************//
		// Constructor.																	//
		//******************************************************************************//
		public ExportThread(Handler objNewHandler) 
		{
			mHandler             = objNewHandler;
		}

		
		//******************************************************************************//
		// Run method, as this is a Thread.												//
		//******************************************************************************//
		@Override
		public void run() 
		{
			String exceptionMsg = "";
			exportThreadRunning = true;
			String iconKMLStyle = "<Style id=\"default0\">\n"
					+ "<IconStyle>\n"
					+ "<scale>0.8</scale>\n"
					+ "<Icon>"
					+ "<href>http://maps.google.com/mapfiles/kml/paddle/blu-blank.png</href>\n"
					+ "</Icon>\n"
					+ "<hotSpot x=\"0.5\" y=\"0\" xunits=\"fraction\" yunits=\"fraction\"/>\n"
					+ "</IconStyle>\n"
					+ "</Style>\n";

			
		try 
		{
			//**********************************************************************//
			// Perform KMLExport													//
			// Build source file path												//
			//**********************************************************************//
				
				
			//**********************************************************************//
			// Check Some readings exist.                        					//
			//**********************************************************************//
			ArrayList <SecchiReadingX> listSecchiReadings = secchiPackageVariables
					                     .getListOfOfSumbittedSecchiReadings(
					                    		 getApplicationContext());
				
			if ((listSecchiReadings != null ) && (!listSecchiReadings.isEmpty()))
			{
				//*****************************************************************//
				// Generate destination file name.                                 //
				//*****************************************************************//
				strDestFileName       = exportFileName;
				int intExtensionStart = strDestFileName.indexOf(".");
				strDestFileName       = strDestFileName.substring(0, intExtensionStart);
				strDestFileName       = strDestFileName + ".kml";
				strDestFileName       = secchiPackageVariables
		                                   .buildFullPathForFileOnSDCard(strDestFileName);
				
				
				boolean blnFileExists = false;
				File objDestFile = new File(strDestFileName);
				
				
				try 
				{
					objDestFile.createNewFile();
					blnFileExists = true;
				} 
				catch (IOException e) 
				{
					blnFileExists = false;
				}


				if (!(null != objDestFile && blnFileExists
						&& objDestFile.canWrite()) )
				{
					blnProcessingResult = false;
					if (D.debug) 
						Log.e(activityName, "couldn't make output file; sending bundle: ");
					//*****************************************************//
					// Send  message to dismiss progress window.           //
					//*****************************************************//
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("Complete", getApplicationContext().getString(R.string.exportFailed)
						    + " " + exceptionMsg);
			
					msg.setData(b);
					mHandler.sendMessage(msg);
					return;	
				}
				

				try 
				{
					//**************************************************************//
					// Valid source and destination files, setup streams			//
					//**************************************************************//
					try 
					{
						//Now throws stream corrupted exception. Replace with ShipsLog object
						/**ObjectInputStream objInStream = new ObjectInputStream(
								new FileInputStream(objSourceFile));*/
						OutputStream objOutStream = new FileOutputStream(objDestFile);
	
						Writer write = new OutputStreamWriter(objOutStream);

							
				    	//******************************************************//
					    // Write out kml header.								//
						//******************************************************//
				    	String strHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
				    			           +"<kml xmlns=\"http://www.opengis.net/kml/2.2\">\n";
				    	write.write(strHeader);
				    	String strKml = "<Document>"
						    		+ "<name>"
									+ strDestFileName
									+ "</name>\n";
			    	
				    	write.write(strKml);

						//**************************************************//
						// Input and Output streams created.				//
						// Go for it!										//
					    //**************************************************//

						for (SecchiReadingX reading : listSecchiReadings) 
						{			    	
								
						//**********************************************//
						// Cancel the thread if no longer needed.       //
						// This code appears many times as the Thread 	//
						// may be cancelled in many places.				//
						//**********************************************//
						if (!exportThreadRunning) 
							throw new StreamCorruptedException(
									   getApplicationContext().getString(R.string.cancelled));
		
										
						//**************************************************//
						// Generate KML for this reading.					//
						//**************************************************//
						float  lat   = reading.getLatitude();
						float  lon   = reading.getLongitude();
						long   date  = reading.getTimeOfLocation();
						String depth = Enumerators
								       .roundFloatToNdp(
								    		  reading.getDepth(), 1);
						String notes = reading.getNotes();
										
								
						//**********************************************//
						// Build a string for depth.					//
						//**********************************************//
						String depthStr = null;	
						if (exportDepths)
						{
							depthStr = "<name>" 
									  + getApplicationContext().getString(R.string.depth)
									  + ": " + depth 
									  + "</name>";
						}
						else 	
							depthStr = "";
						

						//**********************************************//
						// Build a string for notes.					//
						//**********************************************//
						String notesStr = "";
						if (exportNotes)
						{
							if (notes != null)
							{
								notesStr = getApplicationContext().getString(R.string.notes)
									     + "\n"
									     + notes;
							}
						}
								
						strKml = "<Placemark>" 
								+ depthStr
								+ "\n"
								+ iconKMLStyle
								+ "<description>\n"
								+ getApplicationContext().getString(R.string.depth)
								+ ": "
								+ depth 
								+ "\n"
								+ getApplicationContext().getString(R.string.dateCreated)
								+ ": "
								+ Enumerators.buildDateString(date)
								+ "\n"
								+ notesStr
								+ "</description>\n<Point><coordinates>\n"
								+ Float.toString(lon)
								+ ","
								+ Float.toString(lat)
								+ ",0"
								+ "</coordinates></Point>\n"
								+ "</Placemark>\n";
								write.write(strKml);
						}

									
						//**************************************************//
						// Write out kml footer								//
						//**************************************************//								
						strKml ="</Document>";
						write.write(strKml.toString());
						blnProcessingResult = true;
						write.write("</kml>\n");
						write.close();
					} 
					catch (StreamCorruptedException e) 
					{
						blnProcessingResult = false;
						exceptionMsg = e.getMessage();
						if (D.debug) Log.e(activityName, "KML export failed stream corrupted " + e.getMessage());
					} 
					catch (FileNotFoundException e) 
					{
						blnProcessingResult = false;
						exceptionMsg = e.getMessage();
						if (D.debug) Log.e(activityName, "KML export failed file not found" + e.getMessage());
					} 
					catch (IOException e) 
					{
						blnProcessingResult = false;
						exceptionMsg = e.getMessage();
						if (D.debug) Log.e(activityName, "KML export failed unknown exception" + e.getMessage());
					}
					
				

					//*****************************************************//
					// Send  message to dismiss progress window.           //
					//*****************************************************//
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					if (blnProcessingResult) 
					{
						b.putString("Complete", 
								    getApplicationContext().getString(R.string.exportSuccessful) 
								    + " " + strDestFileName);
					} 
					else 
					{
						b.putString("Complete", getApplicationContext().getString(R.string.exportFailed)
								    + " " + exceptionMsg);
					}
					msg.setData(b);
					mHandler.sendMessage(msg);
				} 
				catch (Exception ex) 
				{
					blnProcessingResult = false;
					if (D.debug) 
						Log.e(activityName, "Exception in export thread; sending bundle: " + ex.getMessage());
	
					//*****************************************************//
					// Send  message to dismiss progress window.           //
					//*****************************************************//
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("Complete", getApplicationContext().getString(R.string.exportFailed)
						    + " " + exceptionMsg);
			
					msg.setData(b);
					mHandler.sendMessage(msg);
				}
			}
		}
		catch (Exception ex) 
		{
			blnProcessingResult = false;
			if (D.debug) Log.e(activityName, "Exception in export thread; sending bundle: " + ex.getMessage());
			//*****************************************************//
			// Send  message to dismiss progress window.           //
			//*****************************************************//
			Message msg = mHandler.obtainMessage();
			Bundle b = new Bundle();
			b.putString("Complete", getApplicationContext().getString(R.string.exportFailed)
				    + " " + exceptionMsg);
	
			msg.setData(b);
			mHandler.sendMessage(msg);
		}

		}
	} // End of Export Thread Class

	
	
}	// End of classy class.
