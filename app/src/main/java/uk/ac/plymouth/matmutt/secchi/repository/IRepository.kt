package uk.ac.plymouth.matmutt.secchi.repository

import io.realm.RealmObject

/**
 * Created by David Crotty on 14/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
interface IRepository<T> {
    fun add(entity: T)
    fun addAll(entityList : List<T>)
    fun get(uuid: String) : T?
    fun getAll() : List<T>
    fun find(query : (Any) -> Boolean)
}