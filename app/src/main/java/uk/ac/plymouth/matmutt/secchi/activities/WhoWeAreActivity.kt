package uk.ac.plymouth.matmutt.secchi.activities

import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import uk.ac.plymouth.matmutt.secchi.R
import android.view.WindowManager
import android.os.Build
import android.support.constraint.ConstraintLayout
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import kotlinx.android.synthetic.main.activity_who_we_are.*
import android.view.Window.ID_ANDROID_CONTENT
import kotlinx.android.synthetic.main.information_view.*
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.view.PushDialogListener


/**
 * Created by DavidHome on 26/03/2017.
 */
class WhoWeAreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_who_we_are)
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }

        val params = toolbar.layoutParams as ConstraintLayout.LayoutParams
        params.topMargin = getStatusBarHeight()
        toolbar.layoutParams = params
        triangle_view.addTriangleHeightCallback { height ->
            val params = web_site_button.layoutParams as ConstraintLayout.LayoutParams
            params.bottomMargin = (height.toInt() / 3)
            web_site_button.layoutParams = params
        }
        web_site_button.setOnClickListener {
            launchWebSiteIntent()
        }
    }

    fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    private fun launchWebSiteIntent() {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(BuildConfig.PROJECT_WEBSITE_URL)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        PushDialogListener.register(this@WhoWeAreActivity)
    }

    override fun onPause() {
        super.onPause()
        PushDialogListener.unregister(this@WhoWeAreActivity)
    }
}