//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Class full of static methods to do the web stuff.						//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//
package uk.ac.plymouth.matmutt.secchi.interfaces;

public interface IHTTPResultStringListener 
{
	public void onStringReceived(String result);

}	// End of interesting interface.
