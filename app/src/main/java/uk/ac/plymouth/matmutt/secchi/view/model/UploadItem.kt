package uk.ac.plymouth.matmutt.secchi.view.model

import uk.ac.plymouth.matmutt.secchi.view.adapter.IAdaptable
import uk.ac.plymouth.matmutt.secchi.view.adapter.UploadAdapter

/**
 * Created by David Crotty on 27/08/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class UploadItem : IAdaptable {

    var heading: Heading? = null
    var experiment: Experiment? = null
    private val type: Int

    constructor(heading: Heading) {
        type = UploadAdapter.HEADING
        this.heading = heading
    }

    constructor(experiment: Experiment) {
        type = UploadAdapter.EXPERIMENT
        this.experiment = experiment
    }

    override fun getViewType(): Int {
        return type
    }
}