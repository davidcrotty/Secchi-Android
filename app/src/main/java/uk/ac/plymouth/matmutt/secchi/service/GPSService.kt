package uk.ac.plymouth.matmutt.secchi.service

import android.Manifest
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.NotificationCompat
import android.util.Log
import android.widget.Toast
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import uk.ac.plymouth.matmutt.secchi.NotificationReceiver
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.activities.MainMenuActivity
import uk.ac.plymouth.matmutt.secchi.domain.SecchiReading
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import java.util.concurrent.TimeUnit

/**
 * Created by David Crotty on 07/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class GPSService : Service(), LocationListener {

    private val TAG = "GPSService"
    private val LOCATION_REQUEST_TIME = 100L
    private val NOTIFICATION_RESPONSE_CODE = 1
    private var locationManager: LocationManager? = null
    private var mockLocationDisposable: Disposable? = null

    companion object {
        val START = "START_GPS"
        val START_MOCK = "START_GPS_MOCK"
        val GPS_BROADCAST = "GPS_BROADCAST"
        val NOTIFICATION_DISMISSED = "NOTIFICATION_DISMISSED"
        val NOTIFICATION_ID = 1
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if(intent == null) return super.onStartCommand(intent, flags, startId)

        if(intent.action == START) {
            if(hasGPSRuntimePermission()) {
                getGPSPosition()
                postNotification()
            }
        } else if(intent.action == START_MOCK) {
            Log.d(TAG, "starting")
            mockLocationDisposable = Observable.create<Any>({ e ->
                Log.d(TAG, "postNotification")
                postNotification()
                e.onNext(Any())
            }).delay(3000, TimeUnit.MILLISECONDS)
            .map { e ->
                Log.d(TAG, "commitLocationChangesWith")
                commitLocationChangesWith(9.99, 9.99)
            }
            .subscribeOn(Schedulers.io())
            .subscribe()
        }

        return START_STICKY_COMPATIBILITY
    }

    override fun onDestroy() {
        super.onDestroy()
        mockLocationDisposable?.dispose()
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)
        stopForeground(true)
        stopSelf()
    }

    private fun postNotification() {
        //TODO loading bar (after effects)
        val blankIntent = PendingIntent.getActivity(this,
                NOTIFICATION_RESPONSE_CODE,
                Intent(this, MainMenuActivity::class.java),
                PendingIntent.FLAG_UPDATE_CURRENT)

        val failedGPS = Intent(applicationContext, NotificationReceiver::class.java)
        failedGPS.putExtra(NOTIFICATION_DISMISSED, NOTIFICATION_DISMISSED)
        val deleteIntent = PendingIntent.getBroadcast(applicationContext, 0, failedGPS, 0)

        val notification = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.service_notification)
                .setLargeIcon(BitmapFactory.decodeResource(resources,
                R.drawable.app_icon))
                .setContentTitle(resources.getString(R.string.app_name))
                .setContentText("Searching for GPS")
                .setContentIntent(blankIntent)
                .setDeleteIntent(deleteIntent)
                .build()

        //TODO test on gingerbread
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(NOTIFICATION_ID, notification)
    }

    override fun onLocationChanged(location: Location) {
        /**
         * Issue with emulator, will reboot when acquiring GPS, https://issuetracker.google.com/issues/36923454
         * To ensure this works, trigger on location changed when 'searching'. To ensure rest of flow works.
         */
        locationManager?.removeUpdates(this)
        commitLocationChangesWith(location.latitude, location.longitude)
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
    }

    override fun onProviderEnabled(provider: String?) {
    }

    override fun onProviderDisabled(provider: String?) {
    }

    private fun commitLocationChangesWith(latitude: Double, longitude: Double) {
        val unitOfWork = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val applicationState = unitOfWork.getRealmContext().getApplicationState() ?: return
        if(applicationState.inProgressReading == null) {
            applicationState.inProgressReading = SecchiReading()
        }

        applicationState.inProgressReading?.lattiutde = latitude.toFloat()
        applicationState.inProgressReading?.longitude = longitude.toFloat()
        applicationState.inProgressReading?.timeOfLocation = System.currentTimeMillis()

        unitOfWork.getRealmContext().add(RealmTransaction(applicationState))
        unitOfWork.complete()
        LocalBroadcastManager.getInstance(this).sendBroadcast(Intent(GPS_BROADCAST))
        val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.cancel(NOTIFICATION_ID)
        stopForeground(true)
        stopSelf()
    }

    private fun hasGPSRuntimePermission() : Boolean {
        val courseLocationPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)

        return courseLocationPermission == PackageManager.PERMISSION_GRANTED
    }

    private fun getGPSPosition() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val manager = locationManager?: return
        val provider = if(manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            LocationManager.GPS_PROVIDER
        } else if(manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            LocationManager.NETWORK_PROVIDER
        } else {
            return
        }

        if(provider == LocationManager.GPS_PROVIDER) {
            manager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    LOCATION_REQUEST_TIME,
                    0f,
                    this)
        } else if(provider == LocationManager.NETWORK_PROVIDER) {
            manager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    LOCATION_REQUEST_TIME,
                    0f,
                    this)
        }
    }
}