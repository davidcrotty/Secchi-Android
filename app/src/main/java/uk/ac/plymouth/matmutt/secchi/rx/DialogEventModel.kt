package uk.ac.plymouth.matmutt.secchi.rx

/**
 * Created by David Crotty on 07/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
data class DialogEventModel(val hasReadTerms: Boolean = false,
                            val hasReadAllDoc: Boolean = false)