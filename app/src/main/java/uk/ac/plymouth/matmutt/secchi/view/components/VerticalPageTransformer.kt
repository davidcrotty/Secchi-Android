package uk.ac.plymouth.matmutt.secchi.view.components

import android.support.v4.view.ViewPager
import android.view.View


/**
 * Created by David Crotty on 20/08/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class VerticalPageTransformer : ViewPager.PageTransformer {
    override fun transformPage(view: View, position: Float) {
        if (position < -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(0F)

        } else if (position <= 1) { // [-1,1]
            view.setAlpha(1F)

            // Counteract the default slide transition
            view.setTranslationX(view.getWidth() * -position)

            //set Y position to swipe in from top
            val yPosition = position * view.getHeight()
            view.setTranslationY(yPosition)

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(0F)
        }
    }

}