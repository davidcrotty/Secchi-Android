package uk.ac.plymouth.matmutt.secchi.view.components

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.domain.DocumentationPage
import uk.ac.plymouth.matmutt.secchi.view.DocumentFragment

/**
 * Created by David Crotty on 18/06/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 *
 * Use FragmentPagerAdapter as we're not dynamic
 */
class DocumentPager(fragmentManager: FragmentManager,
                    val documentPageList: List<DocumentationPage>) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        val page = documentPageList[position]
        return DocumentFragment.newInstance(page.resourceID)
    }

    override fun getCount(): Int {
        return BuildConfig.NUMBER_OF_DOC_PAGES
    }


}