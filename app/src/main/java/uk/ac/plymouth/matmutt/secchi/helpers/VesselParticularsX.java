//**************************************************************************//
// Nigel's attempt at a simple Marine GPS.									//
//																			//
// This class represents a vessel's particulars.							//
// 																			//
// All data that is stored, like speed and heading, is stored in the Android//
// default units of metres and m/s.											//
//																			//
// You will see that I am not using the "JavaDoc" style of comments, as this//
// program isn't intended to be self-documenting.  When you write your code,//
// I suggest that you do indeed use the "JavaDoc" type comments.			//
// 																			//
// The output with this demo is much in the LogCat window, though I have now//
// given it a crude UI.														//
//																			//
//   Nigel@soc.plymouth.ac.uk 2011.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.helpers;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.interfaces.IXMLSerializable;




import android.content.Context;

public class VesselParticularsX implements Serializable,
		                                   IXMLSerializable<VesselParticularsX> 
{
	private static final long serialVersionUID = 1;

	//**********************************************************************//
	// Instance variables.                                                  //
	//**********************************************************************//

	private String name = "default";
	private String SSR = "default";
	private ArrayList<String> crew = new ArrayList<String>();

	
	
	//**********************************************************************//
	// Constructors.                                                        //
	//**********************************************************************//
	public VesselParticularsX() 
	{
	}

	public VesselParticularsX(String name, String sSR, String[] crew) 
	{
		super();
		this.name = name;
		this.SSR =  sSR;

		for (String crewMember : crew)
			this.crew.add(crewMember);
	}

	
	//**************************************************************************//
	// Add a crew member.                                                       //
	//**************************************************************************//
	public void addCrewMember(String crewMember) 
	{
		crew.add(crewMember);
	}

	

	
	//**********************************************************************//
	// Roy stuff.															//
	//**********************************************************************//
	@Override
	public boolean writeXMLToFile(XmlSerializer serializer) throws IOException 
	{

		boolean blnWritten = false;
		if (null != serializer) {
			serializer.startTag("", "VesselParticulars");
			serializer.attribute("", "Name", this.name);
			serializer.attribute("", "SSR", this.SSR);
			serializer.attribute("", "NoOfCrew",
					String.valueOf(this.crew.size()));
			for (String strCurrCrewman : this.crew) {
				serializer.startTag("", "Crewman");
				serializer.text(strCurrCrewman);
				serializer.endTag("", "Crewman");
			}
			serializer.endTag("", "VesselParticulars");
			blnWritten = true;
		}
		return blnWritten;
	}

	
	
	//**********************************************************************//
	// Roy stuff.															//
	//**********************************************************************//
	@Override
	public VesselParticularsX loadFromXMLFile(XmlPullParser xmlParser)
			throws IOException 
	{
		VesselParticularsX objResult = null;
		if (null != xmlParser) {
			try {
				if ("VesselParticulars".equalsIgnoreCase(xmlParser.getName())) {
					// Parse the Vessel Particulars Entry
					ArrayList<String> arlCrew = new ArrayList<String>();
					String strVesselName = xmlParser.getAttributeValue("",
							"Name");
					String strVesselSSR = xmlParser
							.getAttributeValue("", "SSR");
					String strNoOfCrew = xmlParser.getAttributeValue("",
							"NoOfCrew");
					Integer intNoCrew = Integer.parseInt(strNoOfCrew);
					if (0 < intNoCrew) {
						for (int i = 0; i < intNoCrew; i++) {
							xmlParser.next();
							arlCrew.add(xmlParser.nextText());
						}
					}
					arlCrew.trimToSize();
					String[] strArrayNewCrew = new String[arlCrew.size()];
					for(int i = 0; i < arlCrew.size(); i++){
						strArrayNewCrew[i] = arlCrew.get(i);
					}
					objResult = new VesselParticularsX(strVesselName, strVesselSSR, strArrayNewCrew);
				} else 
				{
					// Not a Vessel Particulars Entry
					objResult = null;
				}
			} catch (Exception ex) 
			{
				objResult = null;
			}
		}
		return objResult;
	}


	
	//**************************************************************************//
	// Getters and setters; Mary would approve.                                 //
	//**************************************************************************//

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the sSR
	 */
	public String getSSR() {
		return SSR;
	}

	/**
	 * @param sSR
	 *            the sSR to set
	 */
	public void setSSR(String sSR) {
		SSR = sSR;
	}

	// **************************************************************************//
	// We must always return something, even if there are no crew. //
	// **************************************************************************//
	/**
	 * @return the crew
	 */
	public String[] getCrew(Context context) {
		if (crew.size() == 0) {
			String[] something = new String[1];
			something[0] = context.getString(R.string.noCrewAssigned);
			return something;
		}

		String[] result = new String[crew.size()];
		crew.toArray(result);
		return result;
	}

	/**
	 * @param crew
	 *            the crew to set
	 */
	public void setCrew(String[] crew) {
		for (String crewMember : crew)
			this.crew.add(crewMember);
	}

} // End of classy class.
