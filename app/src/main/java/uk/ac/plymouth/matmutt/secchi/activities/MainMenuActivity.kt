package uk.ac.plymouth.matmutt.secchi.activities

import android.Manifest
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.KeyEvent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_main_menu.*
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators
import uk.ac.plymouth.matmutt.secchi.presenter.MainMenuActivityPresenter
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.rx.MenuWizard
import uk.ac.plymouth.matmutt.secchi.service.GPSService
import uk.ac.plymouth.matmutt.secchi.store.ApplicationStateStore
import uk.ac.plymouth.matmutt.secchi.view.DialogProvider
import uk.ac.plymouth.matmutt.secchi.view.IMainMenuView
import uk.ac.plymouth.matmutt.secchi.view.InformationFragment
import uk.ac.plymouth.matmutt.secchi.view.PushDialogListener


/**
* Created by David Crotty on 19/03/2017.
*
* Copyright © 2017 David Crotty - All Rights Reserved
*/
class MainMenuActivity : AppCompatActivity(),
        IMainMenuView,
        DialogProvider.TermsDismissListener,
        DialogProvider.SateliteDialogListener {

    companion object {
        val ALPHA_VISIBLE = 1f
        val ALPHA_INVISIBLE = 0.4f
    }

    val TAG = "MainMenuActivity"
    val LOCATION_CODE = 1
    val CAMERA_CODE = 2
    val DEPTH_ACTIVITY_REQUEST = 2
    val TEMPERATURE_ACTIVITY_REQUEST = 3
    val CAMERA_ACTIVITY_REQUEST = 4
    val NOTES_ACTIVITY_REQUEST = 5

    private var informationSheet: InformationFragment? = null
    private var presenter: MainMenuActivityPresenter? = null
    private var menuWizard: MenuWizard? = null
    private var unitOfWork: UnitOfWork? = null
    private var termsWizardDisposable: Disposable? = null
    private val termsDialogSubject: PublishSubject<Boolean> = PublishSubject.create<Boolean>()
    private var systemPhotoUri: Uri? = null //so we can revoke permissions later
    var gpsServiceUpdate: BehaviorSubject<Intent> = BehaviorSubject.create()

    private val GPS_NOTIFICATION_DISMISSED_RECEIVER = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            gpsServiceUpdate.onNext(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        setSupportActionBar(toolbar)
        val title = resources.getString(R.string.app_name)
        val styledTitle = SpannableString(title)
        styledTitle.setSpan(ForegroundColorSpan(Color.WHITE), 0, title.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        supportActionBar?.title = styledTitle
        disableAll()
        informationSheet = InformationFragment()
        unitOfWork = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        presenter = MainMenuActivityPresenter(this, ApplicationStateStore(), unitOfWork!!)
        menuWizard = MenuWizard(unitOfWork!!, this, termsDialogSubject)
        bindEventListeners()
        LocalBroadcastManager.getInstance(this).registerReceiver(GPS_NOTIFICATION_DISMISSED_RECEIVER,
                IntentFilter(GPSService.GPS_BROADCAST))
    }


    override fun onKeyUp(keycode: Int, e: KeyEvent) : Boolean {
        when (keycode) {
           KeyEvent.KEYCODE_MENU -> {
               if (supportActionBar != null ) {
                   supportActionBar?.openOptionsMenu()
                   return true
               }
           }
        }

        return super.onKeyUp(keycode, e);
    }

    override fun termsAccepted() {
        termsDialogSubject.onNext(true)
    }

    override fun showTerms() {
        DialogProvider.showTermsWith(fragmentManager = supportFragmentManager)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            DEPTH_ACTIVITY_REQUEST -> {
                if(resultCode == Activity.RESULT_OK) {
                    val depth = data?.getFloatExtra(SecchiDepthActivity.DEPTH_INTENT_KEY, 0.0f)
                    val time = data?.getLongExtra(SecchiDepthActivity.TIME_OF_READING_KEY, 0L)
                    if(depth == null || time == 0L) return

                    presenter?.updateDepthWith(depth, time!!)
                }
            }
            TEMPERATURE_ACTIVITY_REQUEST -> {
                if(resultCode == Activity.RESULT_OK) {
                    val temperature = data?.getFloatExtra(SeaTemperatureActivity.TEMPERATURE_INTENT_KEY, 0.0f)
                    val time = data?.getLongExtra(SeaTemperatureActivity.TIME_OF_READING_KEY, 0L)
                    if(temperature == null || time == 0L) return

                    presenter?.updateTemperatureWith(temperature, time!!)
                }
            }
            CAMERA_ACTIVITY_REQUEST -> {
                if(resultCode == Activity.RESULT_OK) {
                    presenter?.updatePhotoPath(didTake = true)
                    systemPhotoUri?.let {
                        revokeUriPermission(it,
                                Intent.FLAG_GRANT_WRITE_URI_PERMISSION
                                        .or(Intent.FLAG_GRANT_READ_URI_PERMISSION))
                    }
                } else {
                    presenter?.updatePhotoPath(didTake = false)
                }
            }
            NOTES_ACTIVITY_REQUEST -> {
                if(resultCode == Activity.RESULT_OK) {
                    val notes = data?.getStringExtra(TakeNotesActivity.NOTES_INTENT_KEY) ?: return
                    presenter?.updateNotesWith(notes)
                }
            }
        }
    }

    override fun hideGPSSpinner() {
        gps_progress.visibility = View.INVISIBLE
    }

    override fun showGPSSpinner() {
        gps_progress.visibility = View.VISIBLE
    }

    override fun updateCoordinatesWith(lattitude: String, longitude: String, timeOfReading: String) {
        latitude_longitude_text.text = "Lat: $lattitude Lon: $longitude"
        minutes_old_text.text = "$timeOfReading"
    }

    override fun updateDepthWith(depth: Float) {
        depth_reading_text.text = "$depth meters"
    }

    override fun updateTemperatureWith(temperature: Float) {
        temperature_reading_text.text = "$temperature\u00b0"
    }

    override fun blockSubmission() {
        val alpha = 0.4f

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            submit_card.alpha = alpha
        }

        submit_card.isEnabled = false
    }

    override fun allowSubmission() {
        val alpha = 1f

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            submit_card.alpha = alpha
        }

        submit_card.isEnabled = true
    }

    override fun clearDepth() {
        depth_reading_text.text = ""
    }

    override fun clearTemperature() {
        temperature_reading_text.text = ""
    }

    override fun updateBoatNameWith(name: String) {
        boat_name_title.setText(name, TextView.BufferType.EDITABLE)
    }

    override fun resetBoatName() {
        boat_name_title.setText(resources.getString(R.string.menu_boat_name_default))
    }

    fun launchCameraActivity() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            try {
                val photoFile = presenter?.generateUriForImage()
                val contentPath = FileProvider.getUriForFile(this,
                        BuildConfig.MAIN_MENU_FILE_PROVIDER,
                        photoFile)
                systemPhotoUri = contentPath

                //temporarily grants permissions that can use this intent so photo can take,
                //see https://medium.com/@a1cooke/using-v4-support-library-fileprovider-and-camera-intent-a45f76879d61 for more info.
                val intentActivityList = packageManager.queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY)
                for(resolvedIntent in intentActivityList) {
                    val packageName = resolvedIntent.activityInfo.packageName
                    grantUriPermission(packageName,
                            contentPath,
                            Intent.FLAG_GRANT_WRITE_URI_PERMISSION.or(Intent.FLAG_GRANT_READ_URI_PERMISSION))
                }

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, contentPath)
                startActivityForResult(takePictureIntent, CAMERA_ACTIVITY_REQUEST)
            } catch (ex : Exception) {
                DialogProvider.showWith(supportFragmentManager, "Error",
                        "Unable to take photo, please check your device storage")
            }
        } else {
            DialogProvider.showWith(supportFragmentManager, "Error",
                    "This Device does not support features required by this app")
        }
    }

    override fun beginGPSSearch(mockReading: Boolean) {
        presenter?.observeGPSChanges(gpsServiceUpdate)
        showGPSSpinner()
        val gpsIntent = Intent(this, GPSService::class.java)
        if(mockReading) {
            gpsIntent.action = GPSService.START_MOCK
        } else {
            gpsIntent.action = GPSService.START
        }
        startService(gpsIntent)
    }

    override fun showSatelliteStatus() {
        startSatelliteActivity()
    }

    override fun initiateGPSSearch() {
        beginGPSSearch(mockReading = false)
    }

    override fun showSatelliteDialog() {
        runOnUiThread({
            DialogProvider.showWith(supportFragmentManager,
                    getString(R.string.satellite_prompt_title),
                    getString(R.string.satellite_prompt_message),
                    getString(R.string.viewSatelliteStatus),
                    getString(R.string.returnSt))
        })
    }

    override fun onSaveInstanceState(outState: Bundle?) {

    }

    override fun startSatelliteActivity() {
        startActivity(Intent(applicationContext,
                SatellitesStatusActivity::class.java))
    }

    override fun finish() {
        super.finish()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(GPS_NOTIFICATION_DISMISSED_RECEIVER)
    }

    fun hasPhotoPermission() : Boolean {
        val cameraPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)

        return cameraPermission == PackageManager.PERMISSION_GRANTED
    }

    fun requestCameraRuntimePermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            DialogProvider.showWith(supportFragmentManager,
                    "Notice",
                    "Application will not work until permission to access camera has been given."
                            + "This can be granted in App Settings")
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA),
                    CAMERA_CODE)
        }
    }

    private fun hasGPSRuntimePermission() : Boolean {
        val courseLocationPermission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)

        return courseLocationPermission == PackageManager.PERMISSION_GRANTED
    }

    fun requestGPSRuntimePermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            DialogProvider.showWith(supportFragmentManager,
                    "Notice",
                    "Application will not work until permission to access location has been given."
                            + "This can be granted in App Settings")
            disableGPS()
            disableDepth()
            disableOptionalAndSubmission()
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            LOCATION_CODE -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    presenter?.checkIfCanStartSearchingGPSWith(getSystemService(Context.LOCATION_SERVICE) as LocationManager?)
                }
            }
            CAMERA_CODE -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchCameraActivity()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.onActivityResume()
        presenter?.updateUIFromPreviousSession()
        presenter?.observeGPSChanges(gpsServiceUpdate)
        presenter?.fetchPreferedBoatName()?.subscribe { name ->
            boat_name_title.setText(name, TextView.BufferType.EDITABLE)
        }
        termsWizardDisposable = menuWizard?.observeDialogReading()?.subscribe({ state ->
            if(state.hasReadAllDocumentationPages()) {
                presenter?.enableReadingComponentsWith(state)
            } else {
                disableAll()
                promptUserToReadDocumentation()
            }
        })
        PushDialogListener.register(this@MainMenuActivity)
    }

    override fun onPause() {
        super.onPause()
        presenter?.onActivityPause()
        termsWizardDisposable?.dispose()
        PushDialogListener.unregister(this@MainMenuActivity)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)

        val inflater = menuInflater
        inflater.inflate(R.menu.start_activity_menu, menu)
        return true
    }

    override fun onBackPressed() {
        //Ensure backstack not popped so splash relaunches
        return
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val itemId = item.itemId
        val intent: Intent?

        when (itemId) {
            R.id.menuDisclaimer -> Enumerators.showDisclaimer(this)

            R.id.menuGetID -> {
                intent = Intent(this, GetIDActivity::class.java)
                startActivity(intent)
            }

            R.id.menuShowID -> {
                intent = Intent(this, ShowEnterIDActivity::class.java)
                startActivity(intent)
            }

            R.id.menuInfo -> {
                informationSheet?.let { bottomSheet ->
                    bottomSheet.show(supportFragmentManager, bottomSheet.tag)
                }
            }

            R.id.menuSubmit -> {
                intent = Intent(this, UploadActivity::class.java)
                startActivity(intent)
            }
        }
        return true
    }

    override fun promptUserToReadDocumentation() {
        runOnUiThread {
            DialogProvider.showWith(supportFragmentManager, resources.getString(R.string.generic_title),
                    resources.getString(R.string.read_all_chapters_message))
        }
    }

    override fun enableAll() {
        enableGPS()
        enableDepth()
        enableOptionalAndSubmission()
    }
    override fun disableAll() {
        disableGPS()
        disableDepth()
        disableOptionalAndSubmission()
    }

    override fun enableGPS() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            gps_card.alpha = ALPHA_VISIBLE
        }
        gps_card.isEnabled = true
    }

    override fun disableGPS() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            gps_card.alpha = ALPHA_INVISIBLE
        }
        gps_card.isEnabled = false
    }

    override fun enableDepth() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            depth_card.alpha = ALPHA_VISIBLE
        }
        depth_card.isEnabled = true
    }

    override fun disableDepth() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            depth_card.alpha = ALPHA_INVISIBLE
        }

        depth_card.isEnabled = false
    }

    override fun enableOptionalAndSubmission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            temperature_card.alpha = ALPHA_VISIBLE
            photograph_card.alpha = ALPHA_VISIBLE
            boat_name_card.alpha = ALPHA_VISIBLE
            notes_card.alpha = ALPHA_VISIBLE
            submit_card.alpha = ALPHA_VISIBLE
        }

        temperature_card.isEnabled = true
        photograph_card.disableWidget()
        photograph_card.isEnabled = true
        boat_name_card.isEnabled = true
        notes_card.isEnabled = true
        submit_card.isEnabled = true
        boat_name_title.isEnabled = true
    }

    override fun disableOptionalAndSubmission() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            temperature_card.alpha = ALPHA_INVISIBLE
            photograph_card.alpha = ALPHA_INVISIBLE
            boat_name_card.alpha = ALPHA_INVISIBLE
            notes_card.alpha = ALPHA_INVISIBLE
            submit_card.alpha = ALPHA_INVISIBLE
        }

        temperature_card.isEnabled = false
        photograph_card.enableWidget()
        photograph_card.isEnabled = false
        boat_name_card.isEnabled = false
        notes_card.isEnabled = false
        submit_card.isEnabled = false
        boat_name_title.isEnabled = false
    }

    override fun getActivity(): AppCompatActivity {
        return this
    }

    private fun bindEventListeners() {
        information_card.setOnClickListener {
            DocumentationMenuActivity.startFrom(this)
        }
        gps_card.setOnClickListener {
            if(hasGPSRuntimePermission()) {
                presenter?.checkIfCanStartSearchingGPSWith(getSystemService(Context.LOCATION_SERVICE) as LocationManager?)
            } else {
                requestGPSRuntimePermission()
            }
        }
        depth_card.setOnClickListener {
            val intent = Intent(this, SecchiDepthActivity::class.java)
            startActivityForResult(intent, DEPTH_ACTIVITY_REQUEST)
        }
        temperature_card.setOnClickListener {
            val intent = Intent(this, SeaTemperatureActivity::class.java)
            startActivityForResult(intent, TEMPERATURE_ACTIVITY_REQUEST)
        }
        photograph_card.setOnClickListener {
            if(hasPhotoPermission()) {
                launchCameraActivity()
            } else {
                requestCameraRuntimePermission()
            }
        }
        notes_card.setOnClickListener {
            val intent = Intent(this, TakeNotesActivity::class.java)
            startActivityForResult(intent, NOTES_ACTIVITY_REQUEST)
        }

        boat_name_card.setOnClickListener {
            boat_name_title.post {
                boat_name_title.requestFocusFromTouch()
                val lManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager;
                lManager.showSoftInput(boat_name_title, 0)
            }
        }

        boat_name_title.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus) {
                presenter?.dataStoreSubscription?.dispose()
                boat_name_title.setText("", TextView.BufferType.EDITABLE)
            } else {
                if(boat_name_title.text.isEmpty()) {
                    boat_name_title.setText(BuildConfig.BOAT_NAME_DEFAULT, TextView.BufferType.EDITABLE)
                }
                presenter?.periodicUIUpdate()
            }
        }
        boat_name_title.setOnEditorActionListener { view, actionId, _ ->
            if(actionId == EditorInfo.IME_ACTION_DONE) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
                scroll_view?.requestFocus()
                presenter?.updateBoatNameWith(view.text.toString())
            }

            false
        }
        submit_card.setOnClickListener {
            presenter?.newReading()
            startActivity(Intent(this, UploadActivity::class.java))
        }
    }

    override fun showPhotoComplete() {
        photograph_card.showNotificiationTick()
    }

    override fun showPhotoIncomplete() {
        photograph_card.hideNotification()
    }
}