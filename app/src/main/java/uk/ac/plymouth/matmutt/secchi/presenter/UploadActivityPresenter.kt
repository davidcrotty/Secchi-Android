package uk.ac.plymouth.matmutt.secchi.presenter

import android.content.Context
import android.os.Bundle
import android.provider.Settings
import com.firebase.jobdispatcher.Constraint
import com.firebase.jobdispatcher.FirebaseJobDispatcher
import com.firebase.jobdispatcher.JobTrigger
import com.firebase.jobdispatcher.Trigger
import com.google.gson.Gson
import io.realm.Realm
import org.joda.time.DateTime
import uk.ac.plymouth.matmutt.secchi.App
import uk.ac.plymouth.matmutt.secchi.activities.UploadActivity
import uk.ac.plymouth.matmutt.secchi.domain.SecchiReading
import uk.ac.plymouth.matmutt.secchi.domain.UploadStatus
import uk.ac.plymouth.matmutt.secchi.network.UploadService
import uk.ac.plymouth.matmutt.secchi.network.model.UploadModel
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import uk.ac.plymouth.matmutt.secchi.view.adapter.UploadAdapter
import uk.ac.plymouth.matmutt.secchi.view.model.Experiment
import android.provider.Settings.SettingNotFoundException
import android.provider.Settings.Secure
import uk.ac.plymouth.matmutt.secchi.view.IUploadView


/**
 * Created by David Crotty on 03/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class UploadActivityPresenter(val dispatcher: FirebaseJobDispatcher,
                              val app: App) {

    companion object {
        val NOT_SET = -1
        val NOT_SET_FLOAT = -1f
    }

    fun shouldShowEditView(isEditable: Boolean, experiment: Experiment) : Boolean {
        return isEditable && experiment.getState() == UploadStatus.PENDING
    }

    fun processPendingState(experiment: Experiment, isEditable: Boolean, adapter: UploadAdapter) {
        if(isEditable) {
            delete(experiment, adapter)
        } else {
            upload(experiment)
        }
    }

    fun upload(experiment: Experiment) {
        experiment.setState(UploadStatus.PROCESSING)
        setItemToProcessing(experiment)
        submitReading(experiment)
    }

    fun hasValidTemperature(temperature: String) : Boolean {
        if(temperature == null) return false
        val sanitized = temperature.toFloatOrNull() ?: return false
        if(sanitized > -99) return true //Due to legacy code setting temperature to -100.0f when not set
        return false
    }

    fun submitReading(experiment: Experiment) {
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val domainReading = work.getRealmContext().find<SecchiReading> { reading -> reading.primaryKey == experiment.id } ?: return
        val mappedReading = createMappedReading(domainReading)
        work.complete()

        val bundle = Bundle()
        bundle.putString(UploadModel.INTENT_KEY, mappedReading)

        val uploadJob = dispatcher.newJobBuilder()
                .setService(UploadService::class.java)
                .setTag(domainReading.primaryKey)
                .setExtras(bundle)
                .setTrigger(Trigger.executionWindow(0, 0))
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build()
        dispatcher.mustSchedule(uploadJob)
        setItemToPending(experiment)
    }

    fun showUploadState(experiment: Experiment,
                        isEditable: Boolean,
                        adapter: UploadAdapter?,
                        context: Context,
                        uploadView: IUploadView,
                        roamingWarningAcknowledged: Boolean) {
        when(experiment.getState()) {
            UploadStatus.UPLOADED -> {
                experiment.setState(UploadStatus.UPLOADED)
                return
            }
            UploadStatus.PROCESSING -> {
                experiment.setState(UploadStatus.PENDING)
                cancelUpload(experiment)
            }
            UploadStatus.PENDING -> {
                val adapter = adapter ?: return
                if(isRoaming(context) && isEditable == false && roamingWarningAcknowledged == false) {
                    uploadView.renderRoamingPrompt(experiment, isEditable)
                } else {
                    processPendingState(experiment, isEditable, adapter)
                }
            }
        }
        adapter?.notifyDataSetChanged() //Notifying individual items currently doesn't work
    }

    private fun isRoaming(context: Context) : Boolean {
        return try {
            Settings.Secure.getInt(context.contentResolver, android.provider.Settings.Global.DATA_ROAMING) === 1
        } catch (e: SettingNotFoundException) {
            // return false if no such settings exist (device with no radio data)
            false
        }

    }

    private fun cancelUpload(experiment: Experiment) {
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val domainReading = work.getRealmContext().find<SecchiReading> { reading -> reading.primaryKey == experiment.id } ?: return
        work.complete()
        val key = domainReading.primaryKey ?: return
        dispatcher.cancel(key)
    }

    /**
     * Because Firebase Job dispatcher uses play services, custom Parcelables will not work.
     */
    private fun createMappedReading(from: SecchiReading) : String {
        val locationIsValid = from.timeOfLocation != null
        val locationDateTime = DateTime(from.timeOfLocation ?: 0)

        //Ugly, because API wants a non standard time format we need to count items we wont send as null, but data types cannot be null
        var gpsDay = NOT_SET
        var gpsMonth = NOT_SET
        var gpsYear = NOT_SET
        var gpsHour = NOT_SET
        var gpsMin = NOT_SET
        var gpsSec = NOT_SET

        if(locationIsValid) {
            gpsDay = locationDateTime.dayOfMonth
            gpsMonth = locationDateTime.monthOfYear
            gpsYear = locationDateTime.year
            gpsHour = locationDateTime.hourOfDay
            gpsMin = locationDateTime.minuteOfHour
            gpsSec = locationDateTime.secondOfMinute
        }

        val depthIsValid = from.timeOfDepth != null
        val depthTime = DateTime(from.timeOfDepth ?: 0)

        var depthDay = NOT_SET
        var depthMonth = NOT_SET
        var depthYear = NOT_SET
        var depthHour = NOT_SET
        var depthMin = NOT_SET
        var depthSec = NOT_SET

        if(depthIsValid) {
            depthDay = depthTime.dayOfMonth
            depthMonth = depthTime.monthOfYear
            depthYear = depthTime.year
            depthHour = depthTime.hourOfDay
            depthMin = depthTime.minuteOfHour
            depthSec = depthTime.secondOfMinute
        }

        val temperatureIsValid = from.timeOfTemperature != null
        val temperatureTime = DateTime(from.timeOfTemperature ?: 0)

        var temperatureDay = NOT_SET
        var temperatureMonth = NOT_SET
        var temperatureYear = NOT_SET
        var temperatureHour = NOT_SET
        var temperatureMin = NOT_SET
        var temperatureSec = NOT_SET

        if(temperatureIsValid) {
            temperatureDay = temperatureTime.dayOfMonth
            temperatureMonth = temperatureTime.monthOfYear
            temperatureYear = temperatureTime.year
            temperatureHour = temperatureTime.hourOfDay
            temperatureMin = temperatureTime.minuteOfHour
            temperatureSec = temperatureTime.secondOfMinute
        }

        return app.gson.toJson(UploadModel(from.vesselName,
                gpsDay,
                gpsMonth,
                gpsYear,
                gpsHour,
                gpsMin,
                gpsSec,
                from.lattiutde ?: NOT_SET_FLOAT,
                from.longitude ?: NOT_SET_FLOAT,
                depthDay,
                depthMonth,
                depthYear,
                depthHour,
                depthMin,
                depthSec,
                from.depth?.toInt() ?: NOT_SET,
                from.temperature?.toInt() ?: NOT_SET,
                temperatureDay,
                temperatureMonth,
                temperatureYear,
                temperatureHour,
                temperatureMin,
                temperatureSec,
                from.notes,
                from.photoPath))
    }

    private fun setItemToPending(experiment: Experiment) {
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val reading = work.getRealmContext().find<SecchiReading> { reading -> reading.primaryKey == experiment.id } ?: return
        reading.uploadStatus = UploadStatus.PENDING
        work.getRealmContext().add(RealmTransaction(reading))
        work.complete()
    }

    private fun setItemToProcessing(experiment: Experiment) {
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        val reading = work.getRealmContext().find<SecchiReading> { reading -> reading.primaryKey == experiment.id } ?: return
        reading.uploadStatus = UploadStatus.PROCESSING
        work.getRealmContext().add(RealmTransaction(reading))
        work.complete()
    }

    private fun delete(experiment: Experiment, adapter: UploadAdapter) {
        //Atomic delete
        try {
            val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
            val position = adapter.getPositionFor(experiment.id)
            if (position == -1) return

            val reading = work.getRealmContext().find<SecchiReading> { reading -> reading.primaryKey == experiment.id }
            if(reading == null) {
                throw NullPointerException("Item does not exist")
            }
            work.getRealmContext().delete(RealmTransaction(reading))
            work.complete()

            adapter.remove(position)
            adapter.notifyItemRemoved(position)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}