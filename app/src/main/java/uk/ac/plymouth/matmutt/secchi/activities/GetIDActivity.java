//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Get the user ID Activity.												//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import uk.ac.plymouth.matmutt.secchi.helpers.VesselParticularsX;
import uk.ac.plymouth.matmutt.secchi.helpers.WebStuff;
import uk.ac.plymouth.matmutt.secchi.interfaces.IHTTPResultStringListener;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;




//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//
//																			//
// The Activity can also return 0 / 1 if the operation is successful or not //
//**************************************************************************//
public class GetIDActivity extends Activity implements
											OnClickListener,
											IHTTPResultStringListener
{

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName             = "Get ID Activity";
	private SecchiPackageVariables secchiPackageVariables   = null;
	private Button                 buttonGetID              = null; 
	private Button                 buttonEnterID            = null;
	private Button                 buttonDismiss		    = null;
	private Button                 buttonPrivacyPolicy      = null;
	private ProgressBar            progressBarWaiting       = null;
	
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.get_user_id);
               
        secchiPackageVariables = SecchiPackageVariables.getInstance(this);
        

        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        buttonGetID         = (Button)      findViewById(R.id.buttonGetNewID);
        buttonEnterID       = (Button)      findViewById(R.id.buttonEnterExistingID);
        buttonDismiss       = (Button) 	    findViewById(R.id.buttonDismiss);
        buttonPrivacyPolicy = (Button)      findViewById(R.id.buttonPrivacyPolicy);
        progressBarWaiting  = (ProgressBar) findViewById(R.id.progressBarGetID);
        
        buttonGetID.setOnClickListener(this);
        buttonEnterID.setOnClickListener(this);
        buttonDismiss.setOnClickListener(this);
        buttonPrivacyPolicy.setOnClickListener(this);
    }

   
    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiPackageVariables.release();
	   
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
		
		//******************************************************************//
		// Is there a valid Secchi address?  If so, we can't get a new		//
		// one.																//
		//******************************************************************//
		boolean validID = secchiPackageVariables.isHaveSecchiID();
		if (validID)
		{
			buttonGetID.setText(R.string.youAlreadyHaveAnID);
			buttonGetID.setEnabled(false);
		}
		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
  
     	return true;
     }



        
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		Intent intent = null;
 		WebStuff webStuff = new WebStuff();
 		
 		switch (view.getId())
 		{
 			case R.id.buttonGetNewID:
 				 boolean serverReachable = webStuff.isServerReachable();
 				 if (D.debug)
 					 Log.i(activityName, "Server reachable is " 
 							             + serverReachable);
 				 
 				 webStuff.setHttpResultStringListener(this); 				
 				 webStuff.getNewID();
 				 progressBarWaiting.setVisibility(View.VISIBLE);
 				 buttonEnterID.setEnabled(false); 	
 				 buttonGetID.setEnabled(false);
 				 break;		
 				 

 			case R.id.buttonEnterExistingID:
 				 intent = new Intent(this, ShowEnterIDActivity.class);
 				 startActivity(intent);
 				 finish();
 				 break;		// Unreachable.
 			
 			
 			case R.id.buttonDismiss:
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.buttonPrivacyPolicy:
 			     Enumerators.showPrivacyPolicy(this);
 			     break;
  		}
 	}



  	//***********************************************************************//
 	// Overriden from IHTTPResultStringListener.							 //
 	//***********************************************************************//
 	@Override
	public void onStringReceived(String newID) 
 	{
		progressBarWaiting.setVisibility(View.GONE);
		buttonEnterID.setEnabled(true);


		//******************************************************************//
 		// Bad ID?  Or can't connect. Just use length for the moment.		//
 		//******************************************************************//
 		if (newID.length() != secchiPackageVariables.getLengthSecchiID())
 		{
 			Toast.makeText(this, R.string.connectionError, 
 					Toast.LENGTH_LONG).show();
 			buttonGetID.setEnabled(true);
 			buttonGetID.setText(R.string.getANewID);
 			setResult(0);
 			return;
 		}
 		
 		//******************************************************************//
 		// Success if we get this far.										//
 		//******************************************************************//
 		TextView tvSuccess = (TextView) findViewById(R.id.textViewSuccessIDObtained);
 		tvSuccess.setVisibility(View.VISIBLE);
		buttonGetID.setEnabled(false);
		buttonGetID.setText(R.string.youAlreadyHaveAnID);
		 
		//**********************************************************//
		// Use SSR in the vessel particulars to store the Secchi	//
		// ID.														//
		//**********************************************************//
		VesselParticularsX vesselParticulars = 
			 	secchiPackageVariables.getVesselParticulars();
		vesselParticulars.setSSR(newID);
        secchiPackageVariables.saveStatus(this);
        setResult(1);

 		if (D.debug)
 			Log.i(activityName, "Got new ID " + newID);
	}


}	// End of classy class.
