//**************************************************************************//
// Nigel's attempt at a simple Marine GPS.									//
//																			//
// A Roy interface for reading and writing XML.								//
//                                                                          //
//   Nigel@soc.plymouth.ac.uk 2012.											//
// XML support   roy.tucker@plymouth.ac.uk 2012.							//
//**************************************************************************//
/**
 * 
 */
package uk.ac.plymouth.matmutt.secchi.interfaces;

import java.io.IOException;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

/**
 * Objects that implement this interface can be serialized as XML
 * @author Roy
 *
 */
public interface IXMLSerializable<T> 
{
	
	boolean writeXMLToFile(XmlSerializer serializer) throws IOException;
	
	T loadFromXMLFile(XmlPullParser xmlParser) throws IOException;

}
