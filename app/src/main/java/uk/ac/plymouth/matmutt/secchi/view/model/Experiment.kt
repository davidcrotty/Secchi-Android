package uk.ac.plymouth.matmutt.secchi.view.model

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

import uk.ac.plymouth.matmutt.secchi.BR

/**
 * Created by David Crotty on 28/08/2017.
 *
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
@Parcelize
class Experiment(var id: String, //UUID so we can reconcile with UI when broadcasts are received
                 var date: String, //Custom format, ie: "9th Oct 2016 - 20:18"
                 var boatName: String?,
                 var temperature: String,
                 var depth: String,
                 private var state: Int,
                 var photoPath: String?) : BaseObservable(), Parcelable {

    fun setState(isSubmitted: Int) {
        this.state = isSubmitted
        notifyPropertyChanged(BR.state)
    }

    @Bindable
    fun getState(): Int {
        return state
    }
}
