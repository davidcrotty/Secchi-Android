package uk.ac.plymouth.matmutt.secchi.view

import android.support.v4.app.FragmentManager
import uk.ac.plymouth.matmutt.secchi.view.model.Experiment

/**
 * Created by David Crotty on 17/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
object DialogProvider {

    interface TermsDismissListener {
        fun termsAccepted()
    }

    interface SateliteDialogListener {
        fun showSatelliteStatus()
        fun initiateGPSSearch()
    }

    interface RoamingWarningDialogListener {
        fun continueUpload(experiment: Experiment)
        fun dontUpload()
    }

    fun showWith(fragmentManager: FragmentManager,
                 title: String,
                 description: String) {
        val frag = fragmentManager.findFragmentByTag(SecchiDialog.TAG)
        if (frag != null) {
            fragmentManager.beginTransaction().remove(frag).commit()
        }
        val dialog = SecchiDialog.newInstance(title, description)
        dialog.show(fragmentManager, SecchiDialog.TAG)
    }

    fun showWith(fragmentManager: FragmentManager,
                 title: String,
                 description: String,
                 positiveText: String,
                 negativeText: String) {
        val frag = fragmentManager.findFragmentByTag(SecchiDialogFragment.TAG)
        if (frag != null) {
            fragmentManager.beginTransaction().remove(frag).commit()
        }
        val dialog = SecchiDialogFragment.newInstance(title,
                description,
                positiveText,
                negativeText,
                SecchiDialogFragment())
        dialog.show(fragmentManager, SecchiDialogFragment.TAG)
    }

    fun showRoamingWith(fragmentManager: FragmentManager,
                        title: String,
                        description: String,
                        positiveText: String,
                        negativeText: String,
                        experiment: Experiment) {
        val frag = fragmentManager.findFragmentByTag(RoamingWarningDialog.TAG)
        if (frag != null) {
            fragmentManager.beginTransaction().remove(frag).commit()
        }
        val dialog = RoamingWarningDialog.newInstance(title,
                description,
                positiveText,
                negativeText,
                experiment,
                RoamingWarningDialog())
        dialog.show(fragmentManager, RoamingWarningDialog.TAG)
    }

    fun showTermsWith(fragmentManager: FragmentManager) {
        val frag = fragmentManager.findFragmentByTag(TermsFragment.TAG)
        if (frag != null) {
            fragmentManager.beginTransaction().remove(frag).commit()
        }
        val dialog = TermsFragment()
        dialog.show(fragmentManager, TermsFragment.TAG)
        fragmentManager.executePendingTransactions()
    }
}