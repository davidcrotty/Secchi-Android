//**************************************************************************//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "nigel style comments for methods.							//
//																			//
// A class that holds (mostly status) variables that are global to the		//
// whole package.															//
//																			//
// You will see that this is a singleton, a common pattern in Android, and  //
// that it manages itself using the reference counter pattern.				//
//																			//
// You will see that I am not using the "JavaDoc" style of comments, as this//
// program isn't intended to be self-documenting.  When you write your code,//
// I suggest that you do indeed use the "JavaDoc" type comments.			//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//
package uk.ac.plymouth.matmutt.secchi.helpers;



import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;


import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;


import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.exceptions.ItemNotFoundException;
import uk.ac.plymouth.matmutt.secchi.exceptions.ItemOutOfDateException;
import uk.ac.plymouth.matmutt.secchi.interfaces.IXMLSerializable;
import android.content.Context;
import android.graphics.PorterDuff.Mode;

import android.location.Location;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.PowerManager;
import android.text.InputFilter.LengthFilter;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;



//******************************************************************//
//This is a singleton class.										//
//It uses an Apple style retain counter to know when it is safe to //
//trash itself.                                                    //
//******************************************************************//
public class SecchiPackageVariables  implements 
                                     IXMLSerializable<SecchiPackageVariables> 
{
	
	//**************************************************************//
	// Enums.                                                       //
	//**************************************************************//
	public enum NorthSouth { north, south;

		public String toString() 
		{
			String strResult = "NORTH";
			switch (this) 
			{
				case north:
					 strResult = "NORTH";
					 break;
				case south:
					 strResult = "SOUTH";
					 break;
				
				default:
					strResult = "NORTH";
			}
			return strResult;
		}

		
		
		public static NorthSouth parseNorthSouth(String strToParse) 
		{
			NorthSouth objResult = NorthSouth.north;
			if (null != strToParse) 
			{
				String strTarget = strToParse.toUpperCase();
				if (strTarget.contains("SOUTH")) 
				{
					objResult = NorthSouth.south;
				}
			}
			return objResult;
		}
	};

	public enum EastWest { east, west;

		public String toString() 
		{
			String strResult = "EAST";
			switch (this) 
			{
				case east:
					 strResult = "EAST";
					 break;
				case west:
					 strResult = "WEST";
					 break;
				default:
					 strResult = "EAST";
			}
			return strResult;
		}

		
		public static EastWest parseEastWest(String strToParse) 
		{
			EastWest objResult = EastWest.east;
			if (null != strToParse) 
			{
				String strTarget = strToParse.toUpperCase();
				if (strTarget.contains("WEST")) 
				{
					objResult = EastWest.west;
				}
			}
			return objResult;
		}
	}

	public enum North {	magneticNorth, trueNorth;

		public String toString() 
		{
			String strResult = "MAGNETICNORTH";
			switch (this) 
			{
				case magneticNorth:
					 strResult = "MAGNETICNORTH";
					 break;
				case trueNorth:
					 strResult = "TRUENORTH";
					 break;
				default:
					 strResult = "MAGNETICNORTH";
			}
			return strResult;
		}

		public static North parseNorth(String strToParse) 
		{
			North objResult = North.magneticNorth;
			if (null != strToParse) 
			{
				String strTarget = strToParse.toUpperCase();
				if (strTarget.contains("TRUENORTH")) 
				{
					objResult = North.trueNorth;
				}
			}
			return objResult;
		}
	}


	public enum SpeedUnits { knots, MPH, KPH;

		public String toString() 
		{
			String strResult = "KNOTS";
			switch (this) 
			{
				case knots:
					 strResult = "KNOTS";
					 break;
				case MPH:
					 strResult = "MPH";
					 break;
				 case KPH:
					 strResult = "KPH";
					 break;
				 default:
					 strResult = "KNOTS";
			}
			return strResult;
		}

		public static SpeedUnits parseSpeedUnits(String strToParse) 
		{
			SpeedUnits objResult = SpeedUnits.knots;
			if (null != strToParse) 
			{
				String strTarget = strToParse.toUpperCase();
				if (strTarget.contains("MPH")) 
				{
					objResult = SpeedUnits.MPH;
				} 
				else if (strTarget.contains("KPH")) 
				{
					objResult = SpeedUnits.KPH;
				}
			}
			return objResult;
		}
	}


	public enum DistanceUnits { nauticleMile, mile, kilometer, metre;

		public String toString() 
		{
			String strResult = "NAUTICLEMILE";
			switch (this) 
			{
				case nauticleMile:
					 strResult = "NAUTICLEMILE";
					 break;
				case mile:
					 strResult = "MILE";
					 break;
				case kilometer:
					 strResult = "KILOMETER";
					 break;
				case metre:
					 strResult = "METER";
					 break;
				default:
					 strResult = "NAUTICLEMILE";
			}
			return strResult;
		}

		public static DistanceUnits parseDistanceUnits(String strToParse) 
		{
			DistanceUnits objResult = DistanceUnits.nauticleMile;
			if (null != strToParse) 
			{
				String strTarget = strToParse.toUpperCase();
				if (strTarget.contains("NAUTICLEMILE")) 
				{
					objResult = DistanceUnits.nauticleMile;
				} 
				else if (strTarget.contains("MILE")) 
				{
					objResult = DistanceUnits.mile;
				} 
				else if (strTarget.contains("KILOMETER")) 
				{
					objResult = DistanceUnits.kilometer;
				} 
				else if (strTarget.contains("METER")) 
				{
					objResult = DistanceUnits.metre;
				}
			}
			return objResult;
		}
	}

	public enum BearingUnits { degreesDecimal, degreesMinutesDecimal, degreesMinutesSeconds;

		public String toString() 
		{
			String strResult = "DEGREESDECIMAL";
			switch (this) 
			{
				case degreesDecimal:
					 strResult = "DEGREESDECIMAL";
					 break;
				 case degreesMinutesDecimal:
					 strResult = "DEGREESMINUTESDECIMAL";
					 break;
				case degreesMinutesSeconds:
					 strResult = "DEGREESMINUTESSECONDS";
					 break;
				default:
					 strResult = "DEGREESDECIMAL";
			}
			return strResult;
		}

		public static BearingUnits parseBearingUnits(String strToParse) 
		{
			BearingUnits objResult = BearingUnits.degreesDecimal;
			if (null != strToParse) 
			{
				String strTarget = strToParse.toUpperCase();
				if (strTarget.contains("DEGREESMINUTESSECONDS")) 
				{
					objResult = BearingUnits.degreesMinutesSeconds;
				} 
				else if (strTarget.contains("DEGREESMINUTESDECIMAL")) 
				{
					objResult = BearingUnits.degreesMinutesDecimal;
				}
			}
			return objResult;
		}
		
		
	}

	//**************************************************************//
	// XMLified by Roy.                                             //
	//**************************************************************//
	public enum Orientation   {vertical, horizontal;

	@Override
	public String toString() {
		String strResult = "VERTICAL";
		switch (this)
		{
		case horizontal:
			strResult = "HORIZONTAL";
		}
		return strResult;
	}

	public static Orientation parseOrientation(String strToParse){
		Orientation objResult = Orientation.vertical;
		if(null != strToParse){
			if(strToParse.contains("HORIZONTAL")){
				objResult = Orientation.horizontal;
			}
		}
		return objResult;
	}
	}

	
	//**************************************************************//
	// Statics, just to define global stuff.						//
	//**************************************************************//
	public static final String  FileNameCurrentPhoto      = "current_secchi_image.jpeg";
	public static final String  FileNameReadingPhotoStart = "reading_photo_";      
	public static final String  FileNamePendingReadings   = "pending_readings.xml";
	public static final String  FileNameSubmittedReadings = "submitted_readings.xml";

	
	private final int LengthSecchiID     = 20;

	
	//**************************************************************//
	// Instance variables.											//
	//																//
	// Initial values are mostly spurious.  Those that are not set  //
	// here are assigned in setDefaults().							//
	//**************************************************************//
	private String        activityName            = null;
	private Orientation   orientation             = Orientation.vertical;              
	private String        bestLocationProvider    = null;
	private int			  locationLifetimeSecs    = 0;		//Set in getInstance
	private North         northUnits;
	private SpeedUnits    speedUnits;
	private DistanceUnits distanceUnits;
	private BearingUnits  bearingUnits;
	private float		  magneticVariation;
	private int           roundBearingsToNumDP;
	private int           roundSpeedToNumDP;
	private int           roundLatLongToNumDP;
	private int           roundRangeToNumDP;
	private String        fileNameStatus			   = null;
	private String        sdCardPrefix				   = null;
	private String		  sdCardFolderName             = null;
	
	
	//**************************************************************//
	// Secchi reading stuff.										//
	//**************************************************************//
	private Location           currentLocation         = null;
	private VesselParticularsX vesselParticulars       = null;
	private float              secchiDepth			   = 0;
	private float              seaTemperature          = 0;
	private String             notes                   = "";
	private boolean            photoTaken              = false;
	private String			   photoHighResLocation	   = null;
	private int                secchiReadingID		   = 0;
	private String             extra                   = "";
	private long               timeOfDepth             = 0;
	private long               timeOfTempreature       = 0;		//Time of location is in the location class.
	

	//**************************************************************//
	// Lists of pending and submitted readings.						//
	// Must be preserved.											//
	//																//
	// Also record whether to show the most recent submitted Secchi //
	// readings first.												//
	//**************************************************************//
	private ArrayList<SecchiReadingX> listPendingSecchiReadings   = null;
	private ArrayList<SecchiReadingX> listSubmittedSecchiReadings = null;
	private boolean                   showMostRecentFirst         = false;
	
	
	
	//**************************************************************//
	// Status variables to indicate whether the various sections	//
	// that must be read have actually been read.					//
	//**************************************************************//
	private boolean      userHasReadSecchiInfo;
	private boolean      userHasReadWhatisASecchiDisk;
	private boolean      userHasReadMakingASecchiDisk;
	private boolean      userHasReadUsingASecchiDisk;
	private boolean      userHasReadOptionalObservations;
	
	
	//**************************************************************//
	// These two are always set in setDefaults() or the setUnit()	//
	// methods and nowhere else.									//
	//**************************************************************//
	private float		 speedConversionFactor		   = 0;
	private float        distanceConversionFactor	   = 0;
	
	//**************************************************************//
	// Use the keyboard for numeric input.							//
	//**************************************************************//
	private boolean useKeyboardForNumericInput		   = false;
	
	
	//**************************************************************//
	// First run variable;  used to see if we have ever been 		//
	// invoked before.  For the Terms and Conditions.				//
	// It is set or cleared in loadStatus().						//
	//**************************************************************//
	private boolean             firstRun			= true;
	private boolean             splashScreenShown   = false;
	private boolean				whatsNewShown		= false;
	private int					lastVersionCode     = 0;
	

	//**************************************************************//
	// Wake lock stuff.												//
	//**************************************************************//
	private PowerManager          powerManager     = null;
	private PowerManager.WakeLock wakeLock         = null;
	private CountDownTimer        wakeLockTimer    = null;
	private final long			  wakeLockTimeMins = 15;
	
	//**************************************************************//
	// No need to preserve these.									//
	//**************************************************************//
	private String 				  versionName		= "Default";
	

	//**************************************************************//
	// Retain count for singleton.									//
	//**************************************************************//
	private volatile int                retainCount             = 0;

	
	//**************************************************************//
	// Static class variables.  Only one as we are a singleton.		//
	//**************************************************************//
	private static SecchiPackageVariables instance  = null;

	
	
	//**************************************************************//
	// Constructor.  Private as this is a singleton					//
	//**************************************************************//
	private SecchiPackageVariables()
	{
			retainCount = 0;
	}

	
	
	
	//**************************************************************//
	// Destructor.  Here as this is a singleton.					//
	//																//
	// Just like Nick's iPhone, we can only destroy the object when //
	// the retain count hits zero.  Destroy is a separate method    //
	// because of the transition between Activities; the pause() of //
	// one is invoked before the resume of the other.  So destroy() //
	// is invoked by the parent (tab) activity and the various 		//
	// services, but only when they quit.							//
	//**************************************************************//
	public void destroy()
	{
		if (retainCount == 0) 
		   {
				if (D.debug) Log.i(activityName, "Destroy called OK to destroy; Class destroyed");
		   try 
		   {
			   Thread.sleep(100);			//Again, bloody buffered 
		   } 								//IO.
		   catch (InterruptedException e) 
		   { }
		   if (wakeLockTimer != null)
		   {
			   wakeLockTimer.cancel();
			   wakeLockTimer = null;
		   }
		   releaseWakeLock();
		   instance = null;
		   }
		else 
		   if (D.debug) Log.i(activityName, "Destroy called; retain count = " + retainCount);
	}

	

	
	//**************************************************************//
	// Retain and release; just like Nick's iPhone.					//
	//**************************************************************//
	public synchronized void retain()
	{
		retainCount++;
	}
	
	public synchronized void release()
	{
		retainCount--;
		if (retainCount < 0) retainCount = 0;
	}
	
	
	
	
    //******************************************************************//
	// Acquire a wake lock.  For the moment, use a dim wake lock.		// 
	//******************************************************************//
	public void acquireWakeLock(Context context)
	{
		if (wakeLock == null)
		{
			powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE); 
			wakeLock     = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, 
	                                                activityName);
		}
		if (!wakeLock.isHeld())
		{
			wakeLock.acquire();
			if (D.debug)
				Log.i(activityName, "Wakelock acquired");
		}
	}
	
	
	
	
    //******************************************************************//
	// Release a wake lock.  Does nothing is the wake lock was never	//
	// acquired, shouldn't bomb.										//
	//******************************************************************//
	public void releaseWakeLock()
	{
		try		// Just in case the wake lock was never created, catch
		{		// a null pointer.
			if (wakeLock.isHeld())
			{
				wakeLock.release();
				if (D.debug)
					Log.i(activityName, "Wakelock released");
			}
		}
		catch (Exception e) 
		{
			if (D.debug)
				Log.e(activityName, "Can't release wake lock "
						            + e.getMessage());
		}
	}
	
	

	//******************************************************************//
	// Starts a count down timer that will release the wake lock, 		//
	// presumably because a location can't be found.					//
	//																	//
	// Ugh, I see we are using anonymous inner classes here.			//
	//******************************************************************//
	public void startWakeLockTimer()
	{
		long time = wakeLockTimeMins * 60 * 1000;
		wakeLockTimer = new CountDownTimer(time, 10000) 
		{
				 @Override
			     public void onTick(long millisUntilFinished) 
			     {
			     }
		
				 @Override
			     public void onFinish() 
			     {
			         releaseWakeLock();
			     }
		}.start();
	}

	
	
	
	//******************************************************************//
	// Stops a count down timer that will release the wake lock, 		//
	// presumably because a location can't be found.					//
	//******************************************************************//
	public void cancelWakeLockTimer()
	{
		if (wakeLockTimer != null)
		{
			wakeLockTimer.cancel();
			wakeLockTimer = null;
			if (D.debug)
				Log.i(activityName, "Wakelock timer cancelled");
		}
	}
	
	
	
	//**********************************************************************//
	// Reset readings to null / impossible values to indicate that no valid //
	// data is there.														//
	//**********************************************************************//
	public void resetSecchiData(Context context)
	{
		secchiDepth       = -100;
		seaTemperature    = -100;
		photoTaken        = false;
		notes             = context.getString(R.string.noNotes);
		timeOfDepth       = 0;
		timeOfTempreature = 0;
		if (currentLocation == null) currentLocation = new Location(bestLocationProvider);
		currentLocation.setTime(0);		//Out of date.
	}
	
	
	
	
	
	
	//**********************************************************************//
	// Submit a Secchi reading by adding it to the list of pending readings.//
	//**********************************************************************//
	public void submitSecchiReading (Context context)
	{
		if (listPendingSecchiReadings == null)
			listPendingSecchiReadings = new ArrayList<SecchiReadingX>();
		
		SecchiReadingX reading = new SecchiReadingX(secchiReadingID, 
									 vesselParticulars.getName(),
									 (float) currentLocation.getLatitude(),
									 (float) currentLocation.getLongitude(),
									 currentLocation.getTime(),
									 secchiDepth, timeOfDepth, 
									 seaTemperature, timeOfTempreature,
									 notes, "null", extra);
									
									
		
		
		//******************************************************************//
		// If a photo is taken, change the name of the current image to		//
		//  photo<id>.jpeg.													//
		//******************************************************************//
		if (isPhotoTaken()) try
		{
			File   filePhoto = null;
			String where     = context.getFilesDir().getAbsolutePath();

			if (D.photosOnSDCard)
			{
				filePhoto = new File(buildFullPathForFileOnSDCard(FileNameCurrentPhoto));
			}
			else 
			{
				filePhoto = new File(where + "/" + FileNameCurrentPhoto);
			}
			
			
			if (filePhoto.exists())
				{
					String newName = FileNameReadingPhotoStart 
							         + String.valueOf(secchiReadingID)
							         + ".jpeg";
					
					String newPath = null;
					if (D.photosOnSDCard)
						newPath = buildFullPathForFileOnSDCard(newName);
					else
						newPath = where + "/" +newName;
					
					File newFile   = new File(newPath);
					
					if (newFile.exists()) newFile.delete();
					filePhoto.renameTo(newFile);
					reading.setPhotoLocation(newPath);
				}
		}
		catch (Exception doh)
		{
			if (D.debug)
				Log.e(activityName, "Error renaming photo "
						             + doh.getMessage());
		}

		listPendingSecchiReadings.add(reading);
		secchiReadingID++;
		resetSecchiData(context);
	}
	

	
	//**********************************************************************//
	// Remove a Secchi reading from the list of pending readings.  Any photo//
	// that is associated with the reading is not deleted.					//
	//**********************************************************************//
	public void removeSecchiReadingFromPendinglist(Context context,
			                                       int     readingNumber)
												   throws  ItemNotFoundException
	{
		try
		{
			listPendingSecchiReadings.remove(readingNumber);
		}
		catch (Exception doh)
		{
			throw new ItemNotFoundException(
					context.getString(
							R.string.readingCouldNotBeRemovedFromlist));
		}
	}
	
	
	

	
	//**********************************************************************//
	// Delete a Secchi reading from the list of pending readings.  The 		//
	// difference between this method and the remove method above is that	//
	// any photo associated with the reading is also deleted.				//
	//**********************************************************************//
	public void deleteSecchiReadingFromPendinglist(Context context,
			                                       int     readingNumber)
												   throws  ItemNotFoundException
	{
		try
		{
			SecchiReadingX reading;
			reading = listPendingSecchiReadings.get(readingNumber);
			
			//**************************************************************//
			// If there is a photo delete it.								//
			//**************************************************************//
			String photoLocation = reading.getPhotoLocation();
			if (!photoLocation.equalsIgnoreCase("null"))
			{
				File file = new File(photoLocation);
				if (file.exists()) file.delete();
			}
			listPendingSecchiReadings.remove(readingNumber);
		}
		catch (Exception doh)
		{
			throw new ItemNotFoundException(
					context.getString(
							R.string.readingCouldNotBeRemovedFromlist));
		}
	}
	

	
	
	//**********************************************************************//
	// Delete all Secchi readings from the list of pending readings.  all 	//
	// photos associated with the pending readings are also deleted.		//
	//**********************************************************************//
	public void deleteAllSecchiReadingsFromPendinglist(Context context)
												   throws  ItemNotFoundException
	{
		try
		{
			//**************************************************************//
			// Delete all photos.											//
			//**************************************************************//
			for(SecchiReadingX reading : listPendingSecchiReadings)
			{
				String photoName = reading.getPhotoLocation();
				if (!photoName.equalsIgnoreCase("null"))
				{
					File photo = new File(photoName);
					if (photo.exists()) photo.delete();
				}
			}
			
			//**************************************************************//
			// Now clear the list of pending readings.						//
			//**************************************************************//
			listPendingSecchiReadings.clear();
		}
		catch (Exception doh)
		{
			throw new ItemNotFoundException(
					context.getString(
							R.string.readingCouldNotBeRemovedFromlist));
		}
	}

	
		
	
	
	
	
	//**********************************************************************//
	// Delete a Secchi reading from the list of submitted readings.  		//
	// any photo associated with the reading is also deleted.				//
	//**********************************************************************//
	public void deleteSecchiReadingFromSubmittedlist(Context context,
			                                       int     readingNumber)
												   throws  ItemNotFoundException
	{
		try
		{
			SecchiReadingX reading;
			reading = listSubmittedSecchiReadings.get(readingNumber);
			
			//**************************************************************//
			// If there is a photo delete it.								//
			//**************************************************************//
			String photoLocation = reading.getPhotoLocation();
			if (!photoLocation.equalsIgnoreCase("null"))
			{
				File file = new File(photoLocation);
				if (file.exists()) file.delete();
			}
			listSubmittedSecchiReadings.remove(readingNumber);
		}
		catch (Exception doh)
		{
			if (D.debug)
				Log.e(activityName, "Couldn't delete submitted reading " + doh.getMessage());
				
			throw new ItemNotFoundException(
					context.getString(
							R.string.readingCouldNotBeRemovedFromlist));
		}
	}
	
	
	

	
	
	
	
	//**********************************************************************//
	// Delete all Secchi readings from the list of submitted readings.  all //
	// photos associated with the pending readings are also deleted.		//
	//**********************************************************************//
	public void deleteAllSecchiReadingsFromSubmittedlist(Context context)
												   throws  ItemNotFoundException
	{
		try
		{
			//**************************************************************//
			// Delete all photos.											//
			//**************************************************************//
			for(SecchiReadingX reading : listSubmittedSecchiReadings)
			{
				String photoName = reading.getPhotoLocation();
				if (!photoName.equalsIgnoreCase("null"))
				{
					File photo = new File(photoName);
					if (photo.exists()) photo.delete();
				}
			}
			
			//**************************************************************//
			// Now clear the list of submitted readings.					//
			//**************************************************************//
			listSubmittedSecchiReadings.clear();
		}
		catch (Exception doh)
		{
			throw new ItemNotFoundException(
					context.getString(
							R.string.readingCouldNotBeRemovedFromlist));
		}
	}

	
	
	
	
	//**********************************************************************//
	// Add a Secchi reading to the list of successful submissions.			//
	//**********************************************************************//
	public void addSecchiReadingtoSuccessfulSubmissions(SecchiReadingX reading)
	{
		if (listSubmittedSecchiReadings == null)
		{
			listSubmittedSecchiReadings = new ArrayList<SecchiReadingX>();
			if (D.debug)
				Log.e(activityName, 
						"No list of successful submissions exists; creating a new list");
		}
		listSubmittedSecchiReadings.add(reading);
	}
	
	

	
	//**********************************************************************//
	// Load lists of Secchi readings from XML documents.					//
	//**********************************************************************//
	public void loadSecchiReadings(Context context)
	{
		listPendingSecchiReadings   = new ArrayList<SecchiReadingX>();
		listSubmittedSecchiReadings = new ArrayList<SecchiReadingX>();
		loadSecchReadingsFromXML(context, listPendingSecchiReadings, FileNamePendingReadings);
		loadSecchReadingsFromXML(context, listSubmittedSecchiReadings, FileNameSubmittedReadings);
	}
	
	
	

	//**********************************************************************//
	// Load a list of Secchi readings from a file.  Both things are 		//
	// passed as parameters.												//
	//**********************************************************************//
	private void loadSecchReadingsFromXML(Context context, 
			                              ArrayList<SecchiReadingX> listReadings,
			                              String fileName)
	{
		try 
		{
			FileInputStream fiInput = null;

			//**************************************************************//
			// Build the input stream conditionally							//
			//**************************************************************//
			if (D.useSDCardFolder)
			{
				fiInput = new FileInputStream(
									new File(buildFullPathForFileOnSDCard(
					             		       fileName)));
			}
			else
			{
				fiInput = context.openFileInput(fileName);
			}
			
			XmlPullParser xmlParser = XmlPullParserFactory.newInstance().newPullParser();
			xmlParser.setInput(fiInput, null);
			
			//**************************************************//
			// Extract attributed from the file, like the 		//
			// number of readings.								//
			//**************************************************//			
			if (xmlParser.next() != XmlPullParser.END_DOCUMENT) 
			{
				if (xmlParser.getName().equalsIgnoreCase("SecchiReadings")) 
				{
					// Extract number of Waypoints to read in
					String strNumReadings = xmlParser.getAttributeValue(0);
					int    numReadings    = Integer.parseInt(strNumReadings);
					// Advance to first waypoint in the list
					xmlParser.next();  //Skip closing tag.
					
					
					//**************************************************//
					// Iterate numReadings times and read the readings	//
					//**************************************************//
					for (int i = 0; i < numReadings; i++) 
					{
						SecchiReadingX entry = new SecchiReadingX();
						entry = entry.loadFromXMLFile(xmlParser);
						
						if (null != entry) 
						{
							listReadings.add(entry);
						}
					}
				}
			}
			if (D.debug) 
				Log.i(instance.activityName, "Loaded " 
									         + listReadings.size()
									         + " Secchi readings from "
									         + fileName);
		} 
		catch (Exception doh) 
		{
			if (D.debug)
				Log.e(instance.activityName, "Error loading  secchi file: "
						+ doh.getMessage()
						+ " " + fileName);
			listReadings = new ArrayList<SecchiReadingX>();
			if (D.debug)
				Log.i(instance.activityName, "Blank list of Secchi readings created");
		}
		
		
	}
	

	//**********************************************************************//
	// Load lists of Secchi readings from XML documents.					//
	//**********************************************************************//
	public void saveSecchiReadings(Context context)
	{
		saveSecchireadingstoXML(context, listPendingSecchiReadings, FileNamePendingReadings);
		saveSecchireadingstoXML(context, listSubmittedSecchiReadings, FileNameSubmittedReadings);
	}

	
	//**********************************************************************//
	// Save a list of Secchi readings to a file.  Both things are 			//
	// passed as parameters. 												//
	//**********************************************************************;;
	private void saveSecchireadingstoXML(Context context,
			                             ArrayList<SecchiReadingX> listReadings,
			                             String fileName)
	{
		if (listReadings != null) try
		{
			FileOutputStream outStream = null;
			
			//**************************************************************//
			// Build the output stream conditionally.						//
			//**************************************************************//
			if (D.useSDCardFolder)
			{	
			outStream = new FileOutputStream(new File(
					buildFullPathForFileOnSDCard(fileName)));
			}
			else
			{
				File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Android/data/SecchiApp", fileName);
				if(file.exists() == false) {
					file.createNewFile();
				}
				outStream = new FileOutputStream(file);
			}
			
			
			FileWriter objFileWriter = new FileWriter(outStream.getFD());
			BufferedWriter buf = new BufferedWriter(objFileWriter);
			// Write string to file
	
	
			XmlSerializer serializer = Xml.newSerializer();
			StringWriter writer = new StringWriter();
			serializer.setOutput(writer);
			serializer.startDocument("UTF-8", true);
	
			//**************************************************************//
			// Start the output file off with attributes like the number of	//
			// readings.													// 
			//**************************************************************//		
			if ((listReadings != null) && !listReadings.isEmpty()) 
			{
				serializer.startTag("", "SecchiReadings");
				serializer.attribute("", "NoOfReadings",
						String.valueOf(listReadings.size()));
	
				//**********************************************************//
				// Iterate round the list and serialise each reading.		//
				//**********************************************************//				
				for (SecchiReadingX reading : listReadings) 
				{
					reading.writeXMLToFile(serializer);
				}
				serializer.endTag("", "SecchiReadings");
			}
			
			// End XML Document
			serializer.endDocument();
			
			// Convert StringWriter contents to string
			String strResult = writer.toString();
			buf.write(strResult);
			
			// Close file stream
			buf.flush();
			buf.close();
	
			if (D.debug) Log.i(instance.activityName, "Secchi data saved "
					           + fileName);
		} 
		catch (Exception doh) 
		{
			if (D.debug)
				Log.e(instance.activityName, "Error saving Secchi data file : "
						+ fileName + " "
						+ doh.getMessage());
		}
	}
	
	
	
	
	//**********************************************************************//
	// Create folder on the SD card.										//
	//**********************************************************************//
	public boolean createSdCardFolder(Context context) 
	{
		if ( (!D.photosOnSDCard) && (!D.allowExportToSDCard))
		{
			if (D.debug)
				Log.i(instance.activityName, 
						"Not creating folder on SD card, not using SD card");
			return false;
		}							// Roy look away.
		
		
		
		String folderName = instance.sdCardPrefix + "/" + instance.sdCardFolderName;
		boolean doneIt     = new File(folderName).mkdir();
		
		if (doneIt)
		{
			if (D.debug) 
			{
				Log.i(instance.activityName, "Created folder " + instance.sdCardFolderName
					                       + " on SD card");
				Toast.makeText(context, "First run?  Created folder " + 
			              instance.sdCardFolderName
			              + " on SD card", Toast.LENGTH_LONG).show();
			}
		}
		else
		{	
			if (D.debug) Log.i(instance.activityName, "Failed to make folder " 
								 + instance.sdCardFolderName
					             + " on SD card");
		}
		return doneIt;
	}

	
	
	
	
	//**********************************************************************//
	// Return the full path to the file specified, using the package 	    //
	// settings for folder name etc.										//
	//																		//
	// also remove any characters that may cause problems with illegal 		//
	// file names and such.													//		
	//**********************************************************************//
	public String buildFullPathForFileOnSDCard(String file)
	{
		file = file.replaceAll("/", "_");
		file = file.replaceAll(":", "");
		
		return instance.sdCardPrefix 
               + "/"
               + instance.sdCardFolderName
               + "/"
               + file;
	}

	
	
	
	//**********************************************************************//
	// Load status.                                                         //
	// Sets defaults if the status file can't be loaded.                    //
	//                                                                      //
	// Roy's XML stuff now used :-)						                    //
	//**********************************************************************//
	public synchronized void loadStatus(Context context) 
	{
		try 
		{
			
			FileInputStream fi = null;

			//**************************************************************//
			// Build the input stream conditionally							//
			//**************************************************************//
			if (D.useSDCardFolder)
			{
				String fileName = buildFullPathForFileOnSDCard(instance.fileNameStatus);
				fi              = new FileInputStream(new File(fileName));
			}
			else
			fi = context.openFileInput(instance.fileNameStatus);
			
			
			XmlPullParser xmlParser = XmlPullParserFactory.newInstance().newPullParser();
			xmlParser.setInput(fi, null);
			loadFromXMLFile(xmlParser);
			
			//Load from XML File - END
			
			firstRun = false;
			if (D.debug) Log.i(instance.activityName, "Status loaded");
		} 
		catch (Exception doh) 
		{
			if (D.debug)
				Log.e(instance.activityName, "Error loading status file: "
						+ doh.getMessage());

			firstRun = true;
			setDefaults(context);
		}
	}


	
	//**********************************************************************//
	// Save status.                                                         //
	//                                                                      //]
	// Now operating on XML.												//
	//**********************************************************************//
	public synchronized void saveStatus(Context context) 
	{
		try 
		{

			/*
			 * FileOutputStream fo = context.openFileOutput(
					instance.fileNameStatus, 0);

			 //FileOutputStream fo = new FileOutputStream(
			 //                new File(buildFullPathForFileOnSDCard(instance.fileNameStatus)));

			ObjectOutputStream oo = new ObjectOutputStream(fo);
			oo.writeObject(instance);
			oo.complete();
			*/

			// Roy CODE TO SAVE AS XML DOCUMENT
			writeCurrentPackageVariables(context);
			firstRun = false;
			if (D.debug) Log.i(instance.activityName, "Status saved");
		} 
		catch (Exception doh) 
		{
			if (D.debug)
				Log.e("Package variables",
						"Error saving status file: " + doh.getMessage());
		}

		
		
	}
	
	
	

	
	
	
	
		//**********************************************************************//
		// Roy stuff.  Write XML to file.                                       //
		//**********************************************************************//
		
		//Single method to call to write all ackageVariables to XML file
		//ROY CHANGED THIS
		public boolean writeCurrentPackageVariables(Context context)
		{
			boolean blnResult = false;
			//Setup file stream to write to
			BufferedWriter bufWriter;
			XmlSerializer serializer = Xml.newSerializer();
			StringWriter writer = new StringWriter();
			
			try
			{
				FileOutputStream fo = null;
				
				//**************************************************************//
				// Build the output stream conditionally.						//
				//**************************************************************//
				if (D.useSDCardFolder)
				{
					
				String fileName = buildFullPathForFileOnSDCard(instance.fileNameStatus);
				fo              = new FileOutputStream(new File(fileName));
				}
				else
					fo = context.openFileOutput(instance.fileNameStatus, 0);
				
				
				FileWriter objFileWriter = new FileWriter(fo.getFD());
				bufWriter = new BufferedWriter(objFileWriter);
				serializer.setOutput(writer);
				serializer.startDocument("UTF-8", true);
				serializer.startTag("", "SceehiPackageVariables");
				blnResult = this.writeXMLToFile(serializer);
				serializer.endTag("", "SceehiPackageVariables");
				serializer.endDocument();
				String strXML = writer.toString();
				bufWriter.write(strXML);
				bufWriter.flush();
				bufWriter.close();
				fo.close();
			}catch (IOException ex)
			{
				if (D.debug) Log.e(activityName, "I/O Exception writing status: " + ex.getMessage());
			}
			return blnResult;
		}
		
		//ROY CHANGED THIS
		@Override
		public boolean writeXMLToFile(XmlSerializer serializer) throws 
		                                                        IOException 
		{

			boolean blnResult = false;
			if (null != serializer) 
			{
				// Do Here all XML serialisation passing the XmlSerializer as
				// required
				// Generate XML for all attributes
				// fileNameWaypoints - KEEP
				// fileNameStatus - KEEP
				serializer.startTag("", "fileNameStatus");
				serializer.text(this.fileNameStatus);
				serializer.endTag("", "fileNameStatus");
				// sdCardPrefix - KEEP
				serializer.startTag("", "sdCardPrefix");
				serializer.text(this.sdCardPrefix);
				serializer.endTag("", "sdCardPrefix");
				// sdCardFolderName - KEEP
				serializer.startTag("", "sdCardFolderName");
				serializer.text(this.sdCardFolderName);
				serializer.endTag("", "sdCardFolderName");

				//**************************************************************//
				// Location, in the form of lat / lon and time.  If there is no	//
				// current location, then invent one to avoid a null pointer.	//
				//**************************************************************//
				if (currentLocation == null)
				{
					currentLocation = new Location(bestLocationProvider);
					currentLocation.setLatitude(0);
					currentLocation.setLongitude(0);
					currentLocation.setTime(0);
				}
				
				serializer.startTag("", "Lat");
				serializer.text(Double.toString(currentLocation.getLatitude()));
				serializer.endTag("", "Lat");
				serializer.startTag("", "Lon");
				serializer.text(Double.toString(currentLocation.getLongitude()));
				serializer.endTag("", "Lon");
				serializer.startTag("", "timeOfLocation");
				serializer.text(Long.toString(currentLocation.getTime()));
				serializer.endTag("", "timeOfLocation");
				//Time of Secchi depth
				serializer.startTag("", "timeOfDepth");
				serializer.text(String.valueOf(timeOfDepth));
				serializer.endTag("", "timeOfDepth");
				// time of sea temperature
				serializer.startTag("", "timeOfTemperature");
				serializer.text(String.valueOf(timeOfTempreature));
				serializer.endTag("", "timeOfTemperature");
				// orientation - KEEP
				serializer.startTag("", "orientation");
				serializer.text(this.orientation.toString());
				serializer.endTag("", "orientation");
				//roundBearingsToNumDP
				serializer.startTag("", "roundBearingsToNumDP");
				serializer.text(String.valueOf(this.roundBearingsToNumDP));
				serializer.endTag("", "roundBearingsToNumDP");
				// roundSpeedToNumDP - KEEP
				serializer.startTag("", "roundSpeedToNumDP");
				serializer.text(String.valueOf(this.roundSpeedToNumDP));
				serializer.endTag("", "roundSpeedToNumDP");
				// roundLatLongToNumDP - KEEP
				serializer.startTag("", "roundLatLongToNumDP");
				serializer.text(String.valueOf(this.roundLatLongToNumDP));
				serializer.endTag("", "roundLatLongToNumDP");
				// roundRangeToNumDP - KEEP
				serializer.startTag("", "roundRangeToNumDP");
				serializer.text(String.valueOf(this.roundRangeToNumDP));
				serializer.endTag("", "roundRangeToNumDP");
				// magneticVariation - KEEP
				serializer.startTag("", "magneticVariation");
				serializer.text(String.valueOf(this.magneticVariation));
				serializer.endTag("", "magneticVariation");
				// bestLocationProvider - KEEP
				serializer.startTag("", "bestLocationProvider");
				serializer.text(this.bestLocationProvider);
				serializer.endTag("", "bestLocationProvider");
				// speedConversionFactor - KEEP
				serializer.startTag("", "speedConversionFactor");
				serializer.text(String.valueOf(this.speedConversionFactor));
				serializer.endTag("", "speedConversionFactor");
				// distanceConversionFactor - KEEP
				serializer.startTag("", "distanceConversionFactor");
				serializer.text(String.valueOf(this.distanceConversionFactor));
				serializer.endTag("", "distanceConversionFactor");
				// Vessel Particulars - KEEP
				getVesselParticulars().writeXMLToFile(serializer);
				// bearingUnits
				serializer.startTag("", "bearingUnits");
				serializer.text(this.bearingUnits.toString());
				serializer.endTag("", "bearingUnits");
				// distanceUnits
				serializer.startTag("", "distanceUnits");
				serializer.text(this.distanceUnits.toString());
				serializer.endTag("", "distanceUnits");
				// northUnits
				serializer.startTag("", "northUnits");
				serializer.text(this.northUnits.toString());
				serializer.endTag("", "northUnits");
				// speedUnits
				serializer.startTag("", "speedUnits");
				serializer.text(this.speedUnits.toString());
				serializer.endTag("", "speedUnits");
				//userHasReadSecchiInfo
				serializer.startTag("", "userHasReadSecchiInfo");
				serializer.text(String.valueOf(userHasReadSecchiInfo));
				serializer.endTag("", "userHasReadSecchiInfo");
				//userHasReadWhatisASecchiDisk
				serializer.startTag("", "userHasReadWhatisASecchiDisk");
				serializer.text(String.valueOf(userHasReadWhatisASecchiDisk));
				serializer.endTag("", "userHasReadWhatisASecchiDisk");
				//userHasReadMakingASecchiDisk
				serializer.startTag("", "userHasReadMakingASecchiDisk");
				serializer.text(String.valueOf(userHasReadMakingASecchiDisk));
				serializer.endTag("", "userHasReadMakingASecchiDisk");
				//userHasReadUsingASecchiDisk
				serializer.startTag("", "userHasReadUsingASecchiDisk");
				serializer.text(String.valueOf(userHasReadUsingASecchiDisk));
				serializer.endTag("", "userHasReadUsingASecchiDisk");
				//userHasReadOptionalObservations
				serializer.startTag("", "userHasReadOptionalObservations");
				serializer.text(String.valueOf(userHasReadOptionalObservations));
				serializer.endTag("", "userHasReadOptionalObservations");
				//Use numeric keyboard for number input
				serializer.startTag("", "useKeyboardFornumericInput");
				serializer.text(String.valueOf(useKeyboardForNumericInput));
				serializer.endTag("", "useKeyboardFornumericInput");
				// Secchi reading ID
				serializer.startTag("", "SecchiReadingID");
				serializer.text(String.valueOf(secchiReadingID));
				serializer.endTag("", "SecchiReadingID");
				// Secchi depth
				serializer.startTag("", "secchiDepth");
				serializer.text(String.valueOf(secchiDepth));
				serializer.endTag("", "secchiDepth");
				//Temperature
				serializer.startTag("", "seaTemperature");
				serializer.text(String.valueOf(seaTemperature));
				serializer.endTag("", "seaTemperature");
				// Photo taken.
				serializer.startTag("", "photoTaken");
				serializer.text(Boolean.toString(photoTaken));
				serializer.endTag("", "photoTaken");
				// Show most recent submissions first
				serializer.startTag("", "showMostRecentReadingsFirst");
				serializer.text(Boolean.toString(showMostRecentFirst));
				serializer.endTag("", "showMostRecentReadingsFirst");
				// What's new shown
				serializer.startTag("", "whatsNewShown");
				serializer.text(Boolean.toString(whatsNewShown));
				serializer.endTag("", "whatsNewShown");			
				// Last version; an int.
				serializer.startTag("", "lastVersionCode");
				serializer.text(Integer.toString(lastVersionCode));
				serializer.endTag("", "lastVersionCode");
				// High res photo location
				if (photoHighResLocation != null)
				{
					serializer.startTag("", "photoHighResLocation");
					serializer.text(photoHighResLocation);
					serializer.endTag("", "photoHighResLocation");
				}
				//Notes
				serializer.startTag("", "notes");
				serializer.text(notes);
				serializer.endTag("", "notes");
				// Extra
				serializer.startTag("", "extra");
				serializer.text(extra);
				serializer.endTag("", "extra");
				
				
				// Generate XML for all List objects
				// AnchorAlarmPoint List
				/* Don't need to preserve this
				 * 
				
				if (null != this.listAnchorAlarmPoints && 0 < this.listAnchorAlarmPoints.size()) 
				{
					serializer.startTag("", "AnchorAlarmPointList");
					serializer.attribute("", "NoOfAlarmPoints",
							String.valueOf(this.listAnchorAlarmPoints.size()));
					for (AnchorAlarmPoint objCurrPoint : this.listAnchorAlarmPoints) 
					{
						objCurrPoint.writeXMLToFile(serializer);
					}
					serializer.endTag("", "AnchorAlarmPointList");
				}
				*/

				blnResult = true;
			}
			return blnResult;
		}

		
		
		
		//**********************************************************************//
		// Roy stuff.  Read XML from file (and process it to set attributes).   //
		//**********************************************************************//	
		//ROY CHANGED THIS
		@Override
		public SecchiPackageVariables loadFromXMLFile(XmlPullParser xmlParser)
				throws IOException {
			if (null != xmlParser) 
			{
				try {
					while (xmlParser.getEventType() != XmlPullParser.END_DOCUMENT) 
					{
						if (xmlParser.getEventType() == XmlPullParser.START_TAG) 
						{
							this.processXMLTag(xmlParser);
						}
						xmlParser.next();
					}
				} 
				catch (Exception ex) 
				{
					if (D.debug)
						Log.e(instance.activityName,
								"Error loading GPSPackage Variables from XML file: "
										+ ex.getMessage());
					//Throw exception to load the defaults
					throw new IOException();
				}
			}
			return null;
		}


		//**********************************************************************//
		// Another Roy one.  I wonder what it does.                             //
		// It extracts the next_white tag from the XML, identifies the attribute it 	//
		// represents and uses the XML tag value to set the attribute			//
		//**********************************************************************//
		//ROY CHANGED THIS
		private void processXMLTag(XmlPullParser xmlParser) 
		{
			try 
			{
				//Check we have parser positioned at START TAG
				if (null != xmlParser && xmlParser.getEventType() != XmlPullParser.END_TAG) 
				{
					// Extract Tag Name
					String strTagName = xmlParser.getName();
					
					Log.i(activityName, strTagName);
					
					if (strTagName.equalsIgnoreCase("bearingUnits")) 
					{
						this.bearingUnits = SecchiPackageVariables.BearingUnits
								.parseBearingUnits(xmlParser.nextText());
					}
					if (strTagName.equalsIgnoreCase("bestLocationProvider")) 
					{
						this.bestLocationProvider = xmlParser.nextText();
					}

						if (strTagName.equalsIgnoreCase("distanceUnits")) 
					{
						this.distanceUnits = SecchiPackageVariables.DistanceUnits
								.parseDistanceUnits(xmlParser.nextText());
					}
					if (strTagName.equalsIgnoreCase("fileNameStatus")) 
					{
						this.fileNameStatus = xmlParser.nextText();
					}
					if (strTagName.equalsIgnoreCase("locationLifetimeSecs")) 
					{
						String strData = xmlParser.nextText();
						this.locationLifetimeSecs = Integer.parseInt(strData);
					}
					if (strTagName.equalsIgnoreCase("orientation")) 
					{
						String strData = xmlParser.nextText();
						this.orientation = SecchiPackageVariables.Orientation.parseOrientation(strData);
					}
					if (strTagName.equalsIgnoreCase("magneticVariation")) 
					{
						String strData = xmlParser.nextText();
						this.magneticVariation = Float.parseFloat(strData);
					}
					if (strTagName.equalsIgnoreCase("northUnits")) 
					{
						String strData = xmlParser.nextText();
						this.northUnits = SecchiPackageVariables.North
								.parseNorth(strData);
					}
					if (strTagName.equalsIgnoreCase("roundBearingsToNumDP")) 
					{
						String strData = xmlParser.nextText();
						this.roundBearingsToNumDP = Integer.parseInt(strData);
					}
					if (strTagName.equalsIgnoreCase("roundLatLongToNumDP")) 
					{
						String strData = xmlParser.nextText();
						this.roundLatLongToNumDP = Integer.parseInt(strData);
					}
					if (strTagName.equalsIgnoreCase("roundRangeToNumDP")) 
					{
						String strData = xmlParser.nextText();
						this.roundRangeToNumDP = Integer.parseInt(strData);
					}
					if (strTagName.equalsIgnoreCase("roundSpeedToNumDP")) 
					{
						String strData = xmlParser.nextText();
						this.roundSpeedToNumDP = Integer.parseInt(strData);
					}
					if (strTagName.equalsIgnoreCase("sdCardFolderName")) 
					{
						this.sdCardFolderName = xmlParser.nextText();
					}
					if (strTagName.equalsIgnoreCase("sdCardPrefix")) 
					{
						this.sdCardPrefix = xmlParser.nextText();
					}
					if (strTagName.equalsIgnoreCase("speedConversionFactor")) 
					{
						String strData = xmlParser.nextText();
						this.speedConversionFactor = Float.parseFloat(strData);
					}
					if (strTagName.equalsIgnoreCase("speedUnits")) 
					{
						this.speedUnits = SecchiPackageVariables.SpeedUnits
								.parseSpeedUnits(xmlParser.nextText());
					}

					if (strTagName.equalsIgnoreCase("VesselParticulars")) 
					{
						instance.vesselParticulars = getVesselParticulars().loadFromXMLFile(xmlParser);
					}
					
					
					//**********************************************************************//
					// Status variables for the various sections we must read.				//
					//**********************************************************************//
					if (strTagName.equalsIgnoreCase("userHasReadSecchiInfo")) 
					{
						try 
						{
							String strData = xmlParser.nextText();
							userHasReadSecchiInfo = Boolean.parseBoolean(strData);
						} 
						catch (Exception ex) 
						{
							if (D.debug) 
								Log.e(activityName, "Exception loading userHasReadSecchiInfo: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("userHasReadWhatisASecchiDisk")) 
					{
						try 
						{
							String strData = xmlParser.nextText();
							userHasReadWhatisASecchiDisk = Boolean.parseBoolean(strData);
						} 
						catch (Exception ex) 
						{
							if (D.debug) 
								Log.e(activityName, "Exception loading userHasReadWhatisASecchiDisk: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("userHasReadMakingASecchiDisk")) 
					{
						try 
						{
							String strData = xmlParser.nextText();
							userHasReadMakingASecchiDisk = Boolean.parseBoolean(strData);
						} 
						catch (Exception ex) 
						{
							if (D.debug) 
								Log.e(activityName, "Exception loading userHasReadMakingASecchiDisk: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("userHasReadUsingASecchiDisk")) 
					{
						try 
						{
							String strData = xmlParser.nextText();
							userHasReadUsingASecchiDisk = Boolean.parseBoolean(strData);
						} 
						catch (Exception ex) 
						{
							if (D.debug) 
								Log.e(activityName, "Exception loading userHasReadUsingASecchiDisk: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("userHasReadOptionalObservations")) 
					{
						try 
						{
							String strData = xmlParser.nextText();
							userHasReadOptionalObservations = Boolean.parseBoolean(strData);
						} 
						catch (Exception ex) 
						{
							if (D.debug) 
								Log.e(activityName, "Exception loading userHasReadOptionalObservations: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("useKeyboardFornumericInput")) 
					{
						try 
						{
							String strData = xmlParser.nextText();
							useKeyboardForNumericInput = Boolean.parseBoolean(strData);
						} 
						catch (Exception ex) 
						{
							if (D.debug) 
								Log.e(activityName, "Exception loading useKeyboardFornumericInput: " + ex.getMessage());
						}
					}
					
					if (strTagName.equalsIgnoreCase("Lat"))
					{
						try
						{
							String strData = xmlParser.nextText();
							if (currentLocation == null) currentLocation = new Location(bestLocationProvider);
							currentLocation.setLatitude(Double.valueOf(strData));
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading Lat: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("Lon"))
					{
						try
						{
							String strData = xmlParser.nextText();
							if (currentLocation == null) currentLocation = new Location(bestLocationProvider);
							currentLocation.setLongitude(Double.valueOf(strData));
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading Lat: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("timeOfLocation"))
					{
						try
						{
							String strData = xmlParser.nextText();
							if (currentLocation == null) currentLocation = new Location(bestLocationProvider);
							currentLocation.setTime(Long.valueOf(strData));
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading time of location: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("timeOfDepth"))
					{
						try //uu timeOfDepth  timeOfTemperature
						{
							String strData = xmlParser.nextText();
							timeOfDepth    = Long.parseLong(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading time of depth: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("timeOfTemperature"))
					{
						try 
						{
							String strData    = xmlParser.nextText();
							timeOfTempreature = Long.parseLong(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading time of depth: " + ex.getMessage());
						}
					}

					if (strTagName.equalsIgnoreCase("SecchiReadingID"))
					{
						try
						{
							String strData  = xmlParser.nextText();
							secchiReadingID = Integer.parseInt(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading Secchi reading ID: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("secchiDepth"))
					{
						try
						{
							String strData = xmlParser.nextText();
							secchiDepth    = Float.valueOf(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading Secchi Depth: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("seaTemperature"))
					{
						try
						{
							String strData = xmlParser.nextText();
							seaTemperature = Float.valueOf(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading sea temperature: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("notes"))
					{
						try
						{
							String strData = xmlParser.nextText();
							notes          = strData;
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading notes: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("photoTaken"))
					{
						try
						{
							String strData = xmlParser.nextText();
							photoTaken     = Boolean.parseBoolean(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading notes: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("showMostRecentReadingsFirst"))
					{
						try
						{
							String strData      = xmlParser.nextText();
							showMostRecentFirst = Boolean.valueOf(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading showMostRecentReadingsFirst: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("photoHighResLocation"))
					{
						try
						{
							String strData       = xmlParser.nextText();
							photoHighResLocation = strData;
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading Hi res photo location: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("lastVersionCode"))
					{
						try
						{
							String strData  = xmlParser.nextText();
							lastVersionCode = Integer.valueOf(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading lastVersionCode: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("whatsNewShown"))
					{
						try
						{
							String strData = xmlParser.nextText();
							whatsNewShown  = Boolean.valueOf(strData);
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading whatsNewShown: " + ex.getMessage());
						}
					}
					if (strTagName.equalsIgnoreCase("extra"))
					{
						try
						{
							extra = xmlParser.nextText();
						} 
						catch (Exception ex) 
						{
						if (D.debug) 
							Log.e(activityName, "Exception loading extra: " + ex.getMessage());
						}
					}
				}
			} 
			catch (XmlPullParserException ex) 
			{
				if (D.debug) Log.e(activityName, "Pull Parser Exception: " + ex.getMessage());
			} 
			catch (IOException ex) 
			{
				if (D.debug) Log.e(activityName, "I/O Exception: " + ex.getMessage());
			} 
			catch (NumberFormatException ex) 
			{
				if (D.debug) Log.e(activityName, "Number Format Exception: " + ex.getMessage());
			} 
			catch (Exception doh) 
			{
				if (D.debug)
					Log.e(instance.activityName, "Error loading status file: "
							+ doh.getMessage());
			}
		}


	
	//**************************************************************************//
	// Getters and setters; Mary would approve.									//
    //**************************************************************************//
	
	/**
	 * Return the instance - one and only as this is a singleton class.	
	*/

	public static SecchiPackageVariables getInstance(Context context)
	{
		if (instance != null) return instance;		// Roy look away.
		
		instance  = new SecchiPackageVariables();

		
        instance.setDefaults(context);
        // Will need a load and save status.
        instance.createSdCardFolder(context);
        instance.loadStatus(context);
        instance.loadSecchiReadings(context);
        
		//**********************************************************************//
		// Some stuff we can't put in setDefaults()								//
		//**********************************************************************//
		instance.splashScreenShown = false;
		
		
		return instance;
	}


	

	
	//**********************************************************************//
	// Set defaults; only used if we can't retrieve out status.				//
	//**********************************************************************//
	public void setDefaults(Context context)
	{
		setCurrentLocation(null);
		setNorthUnits(North.trueNorth);
		setMagneticVariation(3);
		setOrientation(Orientation.vertical);
		setSpeedUnits(SpeedUnits.knots);
		setDistanceUnits(DistanceUnits.metre);		
		setBearingUnits(BearingUnits.degreesMinutesDecimal);
		setLocationLifetimeSecs(20*60);		//20 minutes.
		setRoundBearingsToNumDP(1);
		setRoundSpeedToNumDP(2);
		setRoundLatLongToNumDP(2);
		setRoundRangeToNumDP(1);
		setAllSectionReadVarsToFalse();
		setSeaTemperature(-100);
		setSecchiDepth(-100);
		setNotes(context.getString(R.string.noNotes));
		setPhotoTaken(false);
		setUseKeyboardForNumericInput(false);
		setWhatsNewShown(false);
		setShowMostRecentFirst(true);
		setLastVersionCode(0);

		String[] crew = new String[1];
		crew[0]       = context.getString(R.string.noCrewAssigned);
		setVesselParticulars(new VesselParticularsX(
				                 context.getString(R.string.blank),
				                 context.getString(R.string.noSSRAssigned),
				                 crew));
		
		Location l = new Location("trash");
		l.setLatitude(0);
		l.setLongitude(0);
		l.setTime(0);		//Make sure current locaton is out of date.
		
		//*******************************************************************//
		// Some of these have no getters and setters and some of the getters //
		// and setters we do have require a context.						 //
		//*******************************************************************//
		instance.activityName              = "Secchi package variables";
		instance.fileNameStatus	           = "Status.xml";		
		instance.sdCardPrefix	           =  Environment.getExternalStorageDirectory().getPath() 
                							  + "/Android/data";

		instance.sdCardFolderName          = "SecchiApp";		
		instance.firstRun                  = true;
		
		if (D.debug) 
			Log.i(instance.activityName, "Package status variables defaults set.");
	}
	
	
	
	//**********************************************************************//
	// Set all sections read status variables to false.						//
	//**********************************************************************//
	private void setAllSectionReadVarsToFalse()
	{
		userHasReadSecchiInfo           = false;
		userHasReadWhatisASecchiDisk    = false;
		userHasReadMakingASecchiDisk    = false;
		userHasReadUsingASecchiDisk     = false;
		userHasReadOptionalObservations = false;

	}
	

	/**
	 * @return the orientation
	 */
	public synchronized Orientation getOrientation() {
		return orientation;
	}




	/**
	 * @param orientation the orientation to set
	 */
	public synchronized void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}




	/**
	 * @return the bestLocationProvider
	 */
	public String getBestLocationProvider() {
		return bestLocationProvider;
	}




	/**
	 * @param bestLocationProvider the bestLocationProvider to set
	 */
	public void setBestLocationProvider(String bestLocationProvider) {
		this.bestLocationProvider = bestLocationProvider;
	}
	
	
	//**********************************************************************//
	// Get the current location.											//
	//																		//
	// And in addition the setter throws ItemOutOfDateException if the 		//
	// current location is either non-existent or out of date.				//
	// Out of date is determined by the variable locationLifetimeSecs.		//
	//**********************************************************************//
	/**
	 * @return the currentLocation
	 * @throws ItemOutOfDateException 
	 */
	public synchronized Location getCurrentLocation(Context context) 
	                             throws ItemOutOfDateException 
	{
		Location cLoc = currentLocation;
		
	
		
		
		if (cLoc == null)
			throw new ItemOutOfDateException(
					  context.getString(R.string.currentLocationtOutOfDate));
		
		long locationTime = cLoc.getTime();
		long elapsedTime = (System.currentTimeMillis() - locationTime);
		     elapsedTime /= 1000L;
		     
		if (D.debug) Log.i(activityName, "Age of current location is " + elapsedTime);
		
		if (elapsedTime > (long) locationLifetimeSecs)
			throw new ItemOutOfDateException(
					  context.getString(R.string.currentLocationtOutOfDate));
			
		return cLoc;
	}
	
	

	//**********************************************************************//
	// Set the current location.  Also set the location's time to the 		//
	// current time; I have noticed that the time stamp of a Location isn't //
	// always reliable.														//
	//**********************************************************************//
	public synchronized void setCurrentLocation(Location location) 
	{
		if( location != null) location.setTime(System.currentTimeMillis());
		currentLocation = location;
	}




	/**
	 * @return the northUnits
	 */
	public North getNorthUnits() {
		return northUnits;
	}




	/**
	 * @param northUnits the northUnits to set
	 */
	public void setNorthUnits(North northUnits) {
		this.northUnits = northUnits;
	}




	/**
	 * @return the speedUnits
	 */
	public SpeedUnits getSpeedUnits() {
		return speedUnits;
	}




	



	/**
	 * @return the distanceUnits
	 */
	public DistanceUnits getDistanceUnits() {
		return distanceUnits;
	}




	/**
	 * @param distanceUnits the distanceUnits to set
	 */
	//**********************************************************************//
	// Also sets distanceConversionFactor.									//
	//**********************************************************************//
	public void setDistanceUnits(DistanceUnits distanceUnits) 
	{
		instance.distanceUnits            = distanceUnits;
		instance.distanceConversionFactor = Enumerators.convertFomMetresToDistanceUnits(distanceUnits, 1);


		if (D.debug) Log.i(instance.activityName, "Distance units set to " + distanceUnits.toString()
	             + " conversion factor " 
	             + instance.distanceConversionFactor);
	}




	/**
	 * @return the bearingUnits
	 */
	public BearingUnits getBearingUnits() {
		return bearingUnits;
	}




	/**
	 * @param bearingUnits the bearingUnits to set
	 */
	public void setBearingUnits(BearingUnits bearingUnits) {
		this.bearingUnits = bearingUnits;
	}




	/**
	 * @return the roundBearingsToNumDP
	 */
	public int getRoundBearingsToNumDP() {
		return roundBearingsToNumDP;
	}




	/**
	 * @param roundBearingsToNumDP the roundBearingsToNumDP to set
	 */
	public void setRoundBearingsToNumDP(int roundBearingsToNumDP) {
		this.roundBearingsToNumDP = roundBearingsToNumDP;
	}




	/**
	 * @return the roundSpeedToNumDP
	 */
	public int getRoundSpeedToNumDP() {
		return roundSpeedToNumDP;
	}




	/**
	 * @param roundSpeedToNumDP the roundSpeedToNumDP to set
	 */
	public void setRoundSpeedToNumDP(int roundSpeedToNumDP) {
		this.roundSpeedToNumDP = roundSpeedToNumDP;
	}




	/**
	 * @return the roundLatLongToNumDP
	 */
	public int getRoundLatLongToNumDP() {
		return roundLatLongToNumDP;
	}




	/**
	 * @param roundLatLongToNumDP the roundLatLongToNumDP to set
	 */
	public void setRoundLatLongToNumDP(int roundLatLongToNumDP) {
		this.roundLatLongToNumDP = roundLatLongToNumDP;
	}




	/**
	 * @return the roundRangeToNumDP
	 */
	public int getRoundRangeToNumDP() {
		return roundRangeToNumDP;
	}




	/**
	 * @param roundRangeToNumDP the roundRangeToNumDP to set
	 */
	public void setRoundRangeToNumDP(int roundRangeToNumDP) {
		this.roundRangeToNumDP = roundRangeToNumDP;
	}
	
	/**
	 * @return the speedConversionFactor
	 */
	public float getSpeedConversionFactor() {
		return instance.speedConversionFactor;
	}

	/**
	 * @param speedUnits the speedUnits to set
	 */
	//**********************************************************************//
	// Also sets speedConversionFactor.										//
	//**********************************************************************//
	public void setSpeedUnits(SpeedUnits speedUnits) 
	{
		instance.speedUnits = speedUnits;
		switch(speedUnits)
		{
		case MPH:
			 instance.speedConversionFactor = 2.2369362921f;
			 break;
			 
		case KPH:
			 instance.speedConversionFactor = 3.6f;
			 break;
				 
		case knots:
			 instance.speedConversionFactor = 1.9438444925f;
			 break;
		}
	if (D.debug) Log.i(instance.activityName, "Speed units set to " + speedUnits.toString()
			             + " conversion factor " 
			             + speedConversionFactor);
	}




	/**
	 * @return the magneticVariation
	 */
	public float getMagneticVariation() {
		return magneticVariation;
	}




	/**
	 * @param magneticVariation the magneticVariation to set
	 */
	public void setMagneticVariation(float magneticVariation) {
		this.magneticVariation = magneticVariation;
	}

	
	
	/**
	 * @return the locationLifetimeSecs
	 */
	public int getLocationLifetimeSecs() {
		return instance.locationLifetimeSecs;
	}



	/**
	 * @param locationLifetimeSecs the locationLifetimeSecs to set
	 */
	public void setLocationLifetimeSecs(int locationLifetimeSecs) 
	{
		instance.locationLifetimeSecs = locationLifetimeSecs;
		if (D.debug) Log.i(instance.activityName, "Location lifetime set to " 
				            + locationLifetimeSecs);
	}

	
	/**
	 * @return the distanceConversionFactor
	 */
	public float getDistanceConversionFactor() {
		return instance.distanceConversionFactor;
	}
	
	/**
	 * @param vesselParticulars the vesselParticulars to set
	 */
	public void setVesselParticulars(VesselParticularsX vesselParticulars) {
		this.vesselParticulars = vesselParticulars;
	}

	/**
	 * @return the vesselParticulars
	 */
	public VesselParticularsX getVesselParticulars() 
	{
		if (vesselParticulars == null) return new VesselParticularsX();
		return vesselParticulars;
	}




	/**
	 * @return the userHasReadSecchiInfo
	 */
	public boolean isUserHasReadSecchiInfo() {
		return userHasReadSecchiInfo;
	}




	/**
	 * @param userHasReadSecchiInfo the userHasReadSecchiInfo to set
	 */
	public void setUserHasReadSecchiInfo(boolean userHasReadSecchiInfo) {
		this.userHasReadSecchiInfo = userHasReadSecchiInfo;
	}




	/**
	 * @return the userHasReadWhatisASecchiDisk
	 */
	public boolean isUserHasReadWhatisASecchiDisk() {
		return userHasReadWhatisASecchiDisk;
	}




	/**
	 * @param userHasReadWhatisASecchiDisk the userHasReadWhatisASecchiDisk to set
	 */
	public void setUserHasReadWhatisASecchiDisk(boolean userHasReadWhatisASecchiDisk) {
		this.userHasReadWhatisASecchiDisk = userHasReadWhatisASecchiDisk;
	}




	/**
	 * @return the userHasReadMakingASecchiDisk
	 */
	public boolean isUserHasReadMakingASecchiDisk() {
		return userHasReadMakingASecchiDisk;
	}




	/**
	 * @param userHasReadMakingASecchiDisk the userHasReadMakingASecchiDisk to set
	 */
	public void setUserHasReadMakingASecchiDisk(boolean userHasReadMakingASecchiDisk) {
		this.userHasReadMakingASecchiDisk = userHasReadMakingASecchiDisk;
	}




	/**
	 * @return the userHasReadUsingASecchiDisk
	 */
	public boolean isUserHasReadUsingASecchiDisk() {
		return userHasReadUsingASecchiDisk;
	}




	/**
	 * @param userHasReadUsingASecchiDisk the userHasReadUsingASecchiDisk to set
	 */
	public void setUserHasReadUsingASecchiDisk(boolean userHasReadUsingASecchiDisk) {
		this.userHasReadUsingASecchiDisk = userHasReadUsingASecchiDisk;
	}




	/**
	 * @return the userHasReadOptionalObservations
	 */
	public boolean isUserHasReadOptionalObservations() {
		return userHasReadOptionalObservations;
	}




	/**
	 * @param userHasReadOptionalObservations the userHasReadOptionalObservations to set
	 */
	public void setUserHasReadOptionalObservations(
			boolean userHasReadOptionalObservations) {
		this.userHasReadOptionalObservations = userHasReadOptionalObservations;
	}



	/**
	 * @return  that the the User Has Read All Sections
	 */
	public boolean isUserHasReadAllSections() 
	{
		return userHasReadSecchiInfo 
			   && userHasReadWhatisASecchiDisk 
			   && userHasReadMakingASecchiDisk
			   && userHasReadUsingASecchiDisk 
			   && userHasReadOptionalObservations;
	}


	/**
	 * @return  that the the User Has Read All Sections
	 */
	public boolean isUserHasReadAnySections() 
	{
		return userHasReadSecchiInfo
			   || userHasReadWhatisASecchiDisk 
			   || userHasReadMakingASecchiDisk
			   || userHasReadUsingASecchiDisk 
			   || userHasReadOptionalObservations;
	}


	/**
	 * @return the splashScreenShown
	 */
	public boolean isSplashScreenShown() {
		return splashScreenShown;
	}




	/**
	 * @param splashScreenShown the splashScreenShown to set
	 */
	public void setSplashScreenShown(boolean splashScreenShown) {
		this.splashScreenShown = splashScreenShown;
	}




	/**
	 * @return the secchiDepth
	 */
	public float getSecchiDepth() {
		return secchiDepth;
	}




	/**
	 * @param secchiDepth the secchiDepth to set
	 */
	public void setSecchiDepth(float secchiDepth) {
		if (D.debug)
			Log.i(activityName, "Secchi depth set to " + secchiDepth);
		this.secchiDepth = secchiDepth;
	}




	/**
	 * @return the temperature
	 */
	public float getSeaTemperature() {
		return seaTemperature;
	}




	/**
	 * @param temperature the temperature to set
	 */
	public void setSeaTemperature(float temperature) {
		this.seaTemperature = temperature;
	}




	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}




	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}




	/**
	 * @return the photoTaken
	 */
	public boolean isPhotoTaken() 
	{
		return photoTaken;
	}




	/**
	 * @param photoTaken the photoTaken to set
	 */
	public void setPhotoTaken(boolean photoTaken) 
	{
		this.photoTaken = photoTaken;
		if (D.debug)
			Log.i(activityName, "Photo taken set to " 
					             + photoTaken);
	}

	

	/**
	 *  Check that we have a Secchi ID.  This does NOT necessarily mean the 
	 * Secchi ID is valid
	 *  
	 *  At the moment, this is done simply by checking the
	 *  length if the ID we have.
	 *  
	 * @return true if we have a valid ID. 
	 */
	public boolean isHaveSecchiID()
	{
		String id = vesselParticulars.getSSR();
		if (id.length() == LengthSecchiID)
			return true;
		else
			return false;
	}


	/**
	 * @return Returns The Pending Secchi reading with a 
	 *                 particular number
	 * @throws ItemNotFoundException the reading is not found.
	 */
	public SecchiReadingX getPendingSecchiReading(Context context,
			                                      int readingNumber)
	                      throws ItemNotFoundException
	{
		SecchiReadingX reading = null;
		try
		{
			reading = listPendingSecchiReadings.get(readingNumber);
		}
		catch (Exception doh)
		{
			throw new ItemNotFoundException(
					context.getString(
							R.string.readingCouldNotBeRetrievedFromlist));
		}
		return reading;
	}


	
	
	/**
	 * @return Returns T / F depending on whether the pending 
	 * Secchi reading list is empty.
	 */
	public boolean isListPendingSecchiReadingEmpty()
	{
		if (listPendingSecchiReadings == null) return false;
		
		if (listPendingSecchiReadings.isEmpty()) 
			  return true;
		else  return false;
	}

	
	
	
	/**
	 * @return Returns The Submitted Secchi reading with a 
	 *                 particular number
	 * @throws ItemNotFoundException the reading is not found.
	 */
	public SecchiReadingX getSubmittedSecchiReading(Context context,
			                                      int readingNumber)
	                      throws ItemNotFoundException
	{
		SecchiReadingX reading = null;
		try
		{
			reading = listSubmittedSecchiReadings.get(readingNumber);
		}
		catch (Exception doh)
		{
			throw new ItemNotFoundException(
					context.getString(
							R.string.readingCouldNotBeRetrievedFromlist));
		}
		return reading;
	}


	
	
	/**
	 * @return Returns an array of pending Secchi readings
	 * @throws ItemNotFoundException of there are none.
	 */
	
	public ArrayList<SecchiReadingX> getListOfOfPendingSecchiReadings(Context context) 
			                                                throws
	                                                        ItemNotFoundException
	{
		if (listPendingSecchiReadings == null) 
			throw new ItemNotFoundException(
					context.getString(R.string.noPendingreadings));
		if (listPendingSecchiReadings.isEmpty()) 
			throw new ItemNotFoundException(
					context.getString(R.string.noPendingreadings));
		
		return listPendingSecchiReadings;
	}
	
	
	
	
	
	/**
	 * @return Returns an array of submitted Secchi readings
	 * @throws ItemNotFoundException of there are none.
	 */
	
	public ArrayList<SecchiReadingX> getListOfOfSumbittedSecchiReadings(Context context) 
			                                                throws
	                                                        ItemNotFoundException
	{
		if (listSubmittedSecchiReadings == null) 
			throw new ItemNotFoundException(
					context.getString(R.string.noSubmittedReadings));
		if (listSubmittedSecchiReadings.isEmpty()) 
			throw new ItemNotFoundException(
					context.getString(R.string.noSubmittedReadings));
		
		return listSubmittedSecchiReadings;
	}




	/**
	 * @return the extra
	 */
	public String getExtra() {
		return extra;
	}




	/**
	 * @param extra the extra to set
	 */
	public void setExtra(String extra) {
		this.extra = extra;
	}




	/**
	 * @param timeOfDepth the timeOfDepth to set
	 */
	public void setTimeOfDepth(long timeOfDepth) {
		this.timeOfDepth = timeOfDepth;
	}




	/**
	 * @param timeOfTempreature the timeOftempreature to set
	 */
	public void setTimeOfTempreature(long timeOfTempreature) {
		this.timeOfTempreature = timeOfTempreature;
	}




	/**
	 * @return the firstRun
	 */
	public boolean isFirstRun() {
		return firstRun;
	}




	/**
	 * @return the lengthSecchiID
	 */
	public int getLengthSecchiID() {
		return LengthSecchiID;
	}




	/**
	 * @return the useKeyboardForNumericInput
	 */
	public boolean isUseKeyboardForNumericInput() {
		return useKeyboardForNumericInput;
	}




	/**
	 * @param useKeyboardForNumericInput the useKeyboardForNumericInput to set
	 */
	public void setUseKeyboardForNumericInput(boolean useKeyboardForNumericInput) {
		this.useKeyboardForNumericInput = useKeyboardForNumericInput;
	}




	/**
	 * @return the showMostRecentFirst
	 */
	public boolean isShowMostRecentFirst() {
		return showMostRecentFirst;
	}




	/**
	 * @param showMostRecentFirst the showMostRecentFirst to set
	 */
	public void setShowMostRecentFirst(boolean showMostRecentFirst) {
		this.showMostRecentFirst = showMostRecentFirst;
	}




	/**
	 * @return the whatsNewShown
	 */
	public boolean isWhatsNewShown() {
		return whatsNewShown;
	}




	/**
	 * @param whatsNewShown the whatsNewShown to set
	 */
	public void setWhatsNewShown(boolean whatsNewShown) {
		this.whatsNewShown = whatsNewShown;
	}




	/**
	 * @return the lastVersionCode
	 */
	public int getLastVersionCode() {
		return lastVersionCode;
	}




	/**
	 * @param lastVersionCode the lastVersionCode to set
	 */
	public void setLastVersionCode(int lastVersionCode) {
		this.lastVersionCode = lastVersionCode;
	}




	/**
	 * @return the photoHighResLocation
	 */
	public String getPhotoHighResLocation() {
		return photoHighResLocation;
	}




	/**
	 * @param photoHighResLocation the photoHighResLocation to set
	 */
	public void setPhotoHighResLocation(String photoHighResLocation) {
		this.photoHighResLocation = photoHighResLocation;
	}




	/**
	 * @return the versionName
	 */
	public String getVersionName() {
		return versionName;
	}




	/**
	 * @param versionName the versionName to set
	 */
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}
	



}	// End of classy class.
