//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.exceptions;

//**************************************************************************//
// An exception class thrown when inserting an object into a list and that	//
// item is already there.													//
//**************************************************************************//

public class ItemAlreadyExistsException extends Exception
{

	private static final long serialVersionUID = 1L;

	//**********************************************************************//
	// Constructors.														//
	//**********************************************************************//
	
	public ItemAlreadyExistsException(String message)
	{
		super(message);
	}
	
}	// End of exceptional exception.
