package uk.ac.plymouth.matmutt.secchi.store

import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState

/**
 * Created by David Crotty on 22/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class ApplicationStateStore {
    private var applicationState : ApplicationState? = null
    var stateSubject: Subject<ApplicationState>? = null

    init {
        stateSubject = PublishSubject.create()
    }

    fun updateApplicationStateWith(state : ApplicationState) {
        applicationState = state
        stateSubject?.onNext(applicationState)
    }
}