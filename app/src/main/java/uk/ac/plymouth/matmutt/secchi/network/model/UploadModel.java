package uk.ac.plymouth.matmutt.secchi.network.model;

/**
 * Created by David Crotty on 03/09/2017.
 * <p>
 * Copyright © 2017 David Crotty - All Rights Reserved
 *
 * Map to model external API params is expecting so job has single responsibility when uploading
 */
public class UploadModel {

    public static final String INTENT_KEY = "UploadModel";

    public final String boatName;
    public final int gpsDay;
    public final int gpsMonth;
    public final int gpsYear;
    public final int gpsHour;
    public final int gpsMin;
    public final int gpsSec;
    public final float lattitude;
    public final float longitude;
    public final int depthDay;
    public final int depthMonth;
    public final int depthYear;
    public final int depthHour;
    public final int depthMin;
    public final int depthSec;
    public final int depth;
    public final int temperatureDay;
    public final int temperatureMonth;
    public final int temperatureYear;
    public final int temperatureHour;
    public final int temperatureMin;
    public final int temperatureSec;
    public final int temperature;
    public final String notes;
    public final String photoPath;

    public UploadModel(String boatName, int gpsDay, int gpsMonth, int gpsYear, int gpsHour, int gpsMin,
                       int gpsSec, float lattitude, float longitude, int depthDay, int depthMonth, int depthYear,
                       int depthHour, int depthMin, int depthSec, int depth, int temperatureDay,
                       int temperatureMonth, int temperatureYear, int temperatureHour, int temperatureMin,
                       int temperatureSec, int temperature, String notes, String photoPath) {
        this.boatName = boatName;
        this.gpsDay = gpsDay;
        this.gpsMonth = gpsMonth;
        this.gpsYear = gpsYear;
        this.gpsHour = gpsHour;
        this.gpsMin = gpsMin;
        this.gpsSec = gpsSec;
        this.lattitude = lattitude;
        this.longitude = longitude;
        this.depthDay = depthDay;
        this.depthMonth = depthMonth;
        this.depthYear = depthYear;
        this.depthHour = depthHour;
        this.depthMin = depthMin;
        this.depthSec = depthSec;
        this.depth = depth;
        this.temperatureDay = temperatureDay;
        this.temperatureMonth = temperatureMonth;
        this.temperatureYear = temperatureYear;
        this.temperatureHour = temperatureHour;
        this.temperatureMin = temperatureMin;
        this.temperatureSec = temperatureSec;
        this.temperature = temperature;
        this.notes = notes;
        this.photoPath = photoPath;
    }
}
