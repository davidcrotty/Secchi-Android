//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Boat Name Activity.														//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import uk.ac.plymouth.matmutt.secchi.helpers.VesselParticularsX;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;



//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class BoatNameActivity extends Activity implements
											   OnClickListener
{

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName             = "";
	private SecchiPackageVariables secchiPackageVariables   = null;
	private Button                 buttonSave               = null; 
	private Button                 buttonCancel             = null;
	private EditText               editTextBoatName         = null;
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boat_name_activity_layout);
               
        secchiPackageVariables = SecchiPackageVariables.getInstance(this);
        activityName           = getClass().getSimpleName();


        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        buttonSave       = (Button)   findViewById(R.id.buttonSave);
        buttonCancel     = (Button)   findViewById(R.id.buttonCancel);
        editTextBoatName = (EditText) findViewById(R.id.editTextBoatName);

        buttonSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        editTextBoatName.setOnClickListener(this);
    }

   
    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiPackageVariables.release();
	   
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
		VesselParticularsX particulars = secchiPackageVariables.getVesselParticulars();
		editTextBoatName.setText(particulars.getName());
		editTextBoatName.clearFocus();
		
		
		//*********************************************************//
		// Blank the name if visiting for the first time.			//
		//*********************************************************//
		String blank = getString(R.string.blank);
		if (editTextBoatName
				 .getText()
				 .toString()
				 .equalsIgnoreCase(blank))
			 			editTextBoatName.setText("");

		Enumerators.dismissKeyboard(this, R.id.editTextBoatName);
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }


     
     
        
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		Intent intent = null;
 		
 		switch (view.getId())
 		{
 			case R.id.buttonSave:
 				 EditText etBoat = (EditText) findViewById(R.id.editTextBoatName);
 				 VesselParticularsX vesselParticulars = 
 						 	secchiPackageVariables.getVesselParticulars();
 				 vesselParticulars.setName(etBoat.getText().toString());
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.buttonCancel:
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.editTextBoatName:
 				 break;
  		}
 	}



 

}	// End of classy class.
