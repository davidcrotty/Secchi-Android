//**************************************************************************//
// Nigel's attempt at a simple Marine GPS.                                  //
//																			//
// This class is called Enumerators, but I'm not quite sure anymore.  It    //
// started life as a simple enumerator class that converts various ints to  //
// more meaningful Strings.													//
//                                                                          //
// It is now a miscellaneous collection of static methods.                  //
//                                                                          //
// You will see that I am not using the "JavaDoc" style of comments, as this//
// program isn't intended to be self-documenting.  When you write your code,//
// I suggest that you do indeed use the "JavaDoc" type comments.			//
// 																			//																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.helpers;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables.DistanceUnits;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.GpsStatus;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.FloatMath;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;



//**************************************************************************//
// Simple class to enumerate various things.								//
//**************************************************************************//
public class Enumerators 
{

	
	
	//***********************************************************************//
	// Constants.	 														 //
	//***********************************************************************//
	public  static final float degreeLatToMetres = 1853.0f*60.0f ;  //One degree lat as metres.
	
	
	
	
	//***********************************************************************//
	// Simple utility method to turn the GPS status events into a String.	 //
	//***********************************************************************//
	public static String GPSStatusToString(int status)
	{
		String s = "Unknown";
		switch (status)
		{
			case GpsStatus.GPS_EVENT_FIRST_FIX: s = "First position fix";
				 break;
			case GpsStatus.GPS_EVENT_SATELLITE_STATUS: s = "Satelite status event";
				 break;
			case GpsStatus.GPS_EVENT_STARTED: s = "GPS event started";
				 break;
			case GpsStatus.GPS_EVENT_STOPPED: s = "GPS Event stopped";
				 break;
		}
		return s;
	}
	
	
	
	

	//***********************************************************************//
	// Build approximate duration string.  Basically returns hours	 	 	//
	//***********************************************************************//
	public static String buildApproxDurationString(Context context, long duration)
	{
		int durationInMinutes = (int) duration/(1000*60);
		int hours             = durationInMinutes/60;
		int minutes           = durationInMinutes % 60;
		
		String result = Integer.toString(hours)
		                + " "
		                + context.getString(R.string.hours)
		                + " "
		                + Integer.toString(minutes)
						+ " "
						+ context.getString(R.string.minutes);
		return result;
	}

	
	
	
	
	//***********************************************************************//
	// Build a date string from a date - formatted in a sensible way.	 	 //
	//***********************************************************************//
	public static String buildDateString(long date)
	{
		
		//******************************************************************//
		// DateFotmat used is the Android one, not the standard Java one.	//
		//******************************************************************//
		String ds = DateFormat.format("EEEE, MMMM dd, yyyy h:mmaa", 
				    new Date(date)).toString();
		return ds;
	}
	
	
	//***********************************************************************//
	// Build a date string from a date, formatted shorter.	 	 			 //
	//***********************************************************************//
	public static String buildDateString(long date, boolean shortFormat)
	{
		
		if (!shortFormat) return buildDateString(date);
		
		//******************************************************************//
		// DateFotmat used is the Android one, not the standard Java one.	//
		//******************************************************************//
		String ds = DateFormat.format("kk:mm dd MMM", 
				    new Date(date)).toString();
		return ds;
	}

	

	
	//***********************************************************************//
	// Build a date string from a date - specifying a locale.	 	 		//
	//***********************************************************************//
	public static String buildDateStringUTC(long date)
	{
		
		//******************************************************************//
		// DateFotmat used is the Android one, not the standard Java one.	//
		// I think zz is for UTC.											//
		//******************************************************************//
		String ds = DateFormat.format("MMM dd, yyyy h:mmaa zz ", 
				    new Date(date)).toString();
		return ds;
	}

	//***********************************************************************//
	// Build a date string from a date - specifying a locale.	 	 		//
	//***********************************************************************//
	public static String buildDateStringUTC(long date, boolean  shortFormat)
	{
		
		if (!shortFormat) return buildDateStringUTC(date);

		//******************************************************************//
		// DateFotmat used is the Android one, not the standard Java one.	//
		// I think zz is for UTC.											//
		//******************************************************************//
		String ds = DateFormat.format("dd/MM/yy: h:mmaa zz ", 
				    new Date(date)).toString();
		
		return ds;
	}
	

	
	
	
	
	//***********************************************************************//
	// Build a bearing from an angle.	Also validates for out of range.	 //
	//***********************************************************************//
	public static double buildDecimalAngle(Context context,
			                                   double degreesInDecimal) 
	                                           throws NumberFormatException
	{	
		if (degreesInDecimal < 0) 
		   throw new NumberFormatException(
				     context.getString(R.string.valueMustBePositive));
	
		if (degreesInDecimal > 180.0)
			   throw new NumberFormatException(
					     context.getString(R.string.valueGreaterThan180));
				
		return degreesInDecimal;
	}
	
		
	
	
	//***********************************************************************//
	// Build a bearing from a string, where the string represents degrees in //
	// degrees in decimal.													 //
	// Also calls buildDecimalAngle(), where variation is taken into account.//
	//***********************************************************************//
	public static double buildDecimalAngle(Context context,
                         String strDegreesInDecimal) 
                         throws NumberFormatException
    {
		Double d = Double.valueOf(strDegreesInDecimal);
		return buildDecimalAngle(context, d);   
    }

	
	
	
	//***********************************************************************//
	// Build a bearing from a string, where the string represents degrees in //
	// degrees as integer followed by minutes in decimal.					 //
	// Also calls buildDecimalAngle(), where variation is taken into account.//
	//***********************************************************************//
	public static double buildDecimalAngle(Context context,
            String strDegrees, String strMinutesDecimal) 
                               throws NumberFormatException
    {
		double degs        = Double.valueOf(strDegrees);
		double minsDecimal = Double.valueOf(strMinutesDecimal);
		double d           = 0;
		
		if (Math.abs((int)degs - degs) > 0.000000000001)
			throw new NumberFormatException(
					  context.getString(R.string.degreesMustBeInteger));
			
		if (minsDecimal < 0 )
			throw new NumberFormatException(
					  context.getString(R.string.valueMustBePositive));
		
		if (minsDecimal >= 60.0)
			throw new NumberFormatException(
					  context.getString(R.string.valueSecondsGreaterThan60));

		d = degs + minsDecimal / 60.0;
		return buildDecimalAngle(context, d);   
    }
	
	
	
	
	
	//***********************************************************************//
	// Build a latitude string from degrees and minutes (in decimals) 	 	 //
	// format.                                                               //
	// 																		 //
	// This doesn't insert North/South/East/West; it just returns an array of//
	// strings.  Note the lat/long is always returned as positive.			 //	
	//																		 //
	// And finally, the status of the package variables is used to determine //
	// the output.															 //
	//***********************************************************************//
	public static String[] buildLatLongString(Context context, float latLong)
	{
		latLong = Math.abs(latLong);
		String[] result = null;
		SecchiPackageVariables secchiPackageVariables = SecchiPackageVariables.getInstance(context);

		 
		switch(secchiPackageVariables.getBearingUnits())
		{
			case degreesDecimal:
				result = new String[1];
				result[0] = roundFloatToNdp(latLong, 
						secchiPackageVariables.getRoundLatLongToNumDP()); 
				break;
				
			case degreesMinutesDecimal:
				 result     = new String[2];
				 int degs   = (int) latLong;
				
				 float remainder = (float) latLong - degs;
				 float mins      = remainder*60f;
				
				 result[0] = Integer.toString(degs);
				 result[1] = roundFloatToNdp(mins,
						 secchiPackageVariables.getRoundLatLongToNumDP());
				 break;
		
			case degreesMinutesSeconds:	//Not yet implemented
				break;
		}

		return result;
	}
	
	
	
	
	
	//***********************************************************************//
	// Build a speed string.  The package variables are used to determine the//
	// unit of speed, and the string is terminated by the current units that //
	// are in use.															 //
	//***********************************************************************//
	public static String buildSpeedString(Context context, float speed)
	{
		String result = null;
		String units  = null;
		
		SecchiPackageVariables secchiPackageVariables = SecchiPackageVariables.getInstance(context);
		speed *= secchiPackageVariables.getSpeedConversionFactor();
		
		switch (secchiPackageVariables.getSpeedUnits())
		{
			case knots:
				 units = context.getString(R.string.knots);
				 break;
			
			case MPH:
				 units = context.getString(R.string.MPH);
				 break;
			
			case KPH:
				units = context.getString(R.string.KPH);
				break;
		}
		
		result = roundFloatToNdp(speed, secchiPackageVariables.getRoundSpeedToNumDP()) 
		                         + " " + units;
		return result; 
	}

	

	
	//***********************************************************************//
	// Build a bearing string.  The package variables are used to determine  //
	// the unit of speed, and the string is terminated by the current units  //
	// that are in use.														 //
	// A bearing East, of Grenwich is returned.								 //
	// This also factors in magnetic variation.								 //
	//***********************************************************************//
	public static String buildBearingString(Context context, float bearing)
	{
		String result       = null;
		String units        = null;
		float  variation    = 0;
		float  finalBearing = 0;
		
		SecchiPackageVariables secchiPackageVariables = SecchiPackageVariables.getInstance(context);
		switch (secchiPackageVariables.getNorthUnits())
		{
			case magneticNorth:
				 units     = context.getString(R.string.magneticNorth);
				 variation = secchiPackageVariables.getMagneticVariation();
				 break;
			
			case trueNorth:
				 units = context.getString(R.string.trueNorth);
				 break;
		}
		
		finalBearing = bearing;
		finalBearing -= variation;
		
		if (finalBearing >= 360) finalBearing -= 360;
		if (finalBearing < 0)    finalBearing += 360;
		
		
		result = roundFloatToNdp(finalBearing, 
				                 secchiPackageVariables.getRoundBearingsToNumDP())
				                 + (char) 186		// Degrees
				                 + " " 
				                 + units;
		return result; 
	}

	
	
	
	//***********************************************************************//
	// Build a Latitude / Long string from degrees and minutes (in decimals) //
	// format.                                                               //
	// 																		 //
	// This tries to send a correctly formatted string, looking at the sign  //
	// and returning N/S accordingly.  It also uses the package variables to //
	// return the string correct for the current settings.					 //
	//***********************************************************************//
	public static String buildLatString(Context context, 
			                            float latitude)
	{
		String   result = null;
		String   NS	    = null;
		String[] latStr = null;;

		 if (latitude < 0)
			 NS = context.getString(R.string.S);	//South
		 else
			 NS = context.getString(R.string.N);

		SecchiPackageVariables secchiPackageVariables = SecchiPackageVariables.getInstance(context);
		switch (secchiPackageVariables.getBearingUnits())
		{
			case degreesDecimal:
				 result = roundFloatToNdp(Math.abs(latitude), 
						                  secchiPackageVariables.getRoundLatLongToNumDP());
				 break;
				 
			case degreesMinutesDecimal:
				 latStr = Enumerators.buildLatLongString(context, latitude);
                 result = latStr[0] 
                          + (char) 186 				//Degrees
                          + latStr[1] + "'";   
				 break;
				 
			case degreesMinutesSeconds:
				 break;
		}
		return NS + result;
	}
	
	
	//***********************************************************************//
	// Build a Latitude / Long string from degrees and minutes (in decimals) //
	// format.                                                               //
	// 																		 //
	// This tries to send a correctly formatted string, looking at the sign  //
	// and returning N/S accordingly.  It also uses the package variables to //
	// return the string correct for the current settings.					 //
	//***********************************************************************//
	public static String buildLonString(Context context, 
			                            float longitude)
	{
		String   result = null;
		String   EW	    = null;
		String[] latStr = null;;

		 if (longitude > 0)
			 EW = context.getString(R.string.E);	//South
		 else
			 EW = context.getString(R.string.W);
		
		SecchiPackageVariables secchiPackageVariables = SecchiPackageVariables.getInstance(context);

		switch (secchiPackageVariables.getBearingUnits())
		{
			case degreesDecimal:
				 result = roundFloatToNdp(Math.abs(longitude), 
						                  secchiPackageVariables.getRoundLatLongToNumDP());
				 break;
				 
			case degreesMinutesDecimal:
				 latStr = Enumerators.buildLatLongString(context, longitude);
                 result = latStr[0] 
                          + (char) 186 
                          + latStr[1] + "'";   
				 break;
				 
			case degreesMinutesSeconds:
				 break;
		}
		return EW + result;
	}
	
	
	
	//***********************************************************************//
	// Build a range string.                                                 //
	// It also uses the package variables to return the string correct for   //
	// the current settings.					 							 //
	//***********************************************************************//
	public static String buildRangeString(Context context, 
		 	                              float range)
	{
		String   result = null;
		String   units  = null;
		
		SecchiPackageVariables secchiPackageVariables = SecchiPackageVariables.getInstance(context);
		switch (secchiPackageVariables.getDistanceUnits())
		{
			case kilometer:
				 units = context.getString(R.string.kM);
				 break;
				 
			case mile:
				 units = context.getString(R.string.miles);
				 break;
			
			case nauticleMile:
				 units = context.getString(R.string.NM);
				 break;
			case metre:
				 units = context.getString(R.string.metresCondensed);
				 break;
		}
		
		range  *= secchiPackageVariables.getDistanceConversionFactor();
		result = roundFloatToNdp(Float.valueOf(range), 
				  secchiPackageVariables.getRoundRangeToNumDP())
				 + " " + units;
		return result;
	}
	


	//***********************************************************************//
	// Round to N decimal places, and return a String.  I find Java's 		 //
	// numberFormat	incomprehensible.										 //
	//***********************************************************************//
	public static String roundFloatToNdp(float n, int dp)
	{
		String result    = null;
		double  np10      = Math.pow(10, dp);
		double resultNun;
		
		resultNun = n;
		
		resultNun *=  np10;
		resultNun =  Math.round(resultNun);
		resultNun /= np10;
		
		result = Double.toString(resultNun);
		
		return result;
	}
	

	
	//***********************************************************************//
	// Build a temperature string, rounded to 1dp at the moment.			 //
	//***********************************************************************//
	public static String buildTemperatureString(Context context,
			                                    float temperature)
	{
		 String st = roundFloatToNdp(temperature, 1);
		 return st 
				 + " " + (char) 186
				 + context.getString(R.string.centigradeCondensed);
	}
	

	
	
	
	//***********************************************************************//
	// Convert from metres to the distance units that are currently in use.  //
	//***********************************************************************//
	public static float convertFomMetresToDistanceUnits(
			              DistanceUnits distanceUnits, 
			              float distance)
	{
		float multiplier = 0;
		switch (distanceUnits)
		{
			case kilometer:
				 multiplier = 0.001f;
				 break;
			
			case metre:
				 multiplier = 1;
				 break;
				 
			case mile:
				 multiplier = 6.214e-4f;
				 break;
				
			case nauticleMile:
				 multiplier = 5.3995680345572354211663e-4f;
				 break;				
		}
		return multiplier * distance;
	}
	
	
	//******************************************************************//
	// Dismiss the keyboard.	It only works if the keyboard was 		//
	// //attached to a particular viewI, like an ExitText				//
	//******************************************************************//
	public static void dismissKeyboard(Activity activity, int componentID) 
	{
		try
		{
			View view = activity.findViewById(componentID);
			InputMethodManager keyboard = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE); 
			keyboard.hideSoftInputFromWindow(view.getWindowToken(), 0); 
		}
	catch (Exception ee) { }	//Nothing to do.
	}


	
	

	
	//**********************************************************************//
	// Return the cos of he angle between two bearings, which are both 		//
	// specified in degrees.												//
	//                                                                      //
	// This is based on the formula that									//
	//																		//
	// cos (a-b) = cos(a) cos(b) + sin(a) sin(b)							//
	//**********************************************************************//
	public static double cosOfDifference(float bearingInDegreesA,
			                            float bearingInDegreesB)
	{
		float a = (float) Math.toRadians(bearingInDegreesA);
		float b = (float) Math.toRadians(bearingInDegreesB);
	
		
		double  result = Math.cos(a)*Math.cos(b)
				       +Math.sin(a)*Math.sin(b);
		return result;
	}
	
	

	//**********************************************************************//
	// convert a bearing in degrees to +- 180.  							//
	//**********************************************************************//
	public static float convertBearingTo180(float bearingInDegrees)
	{
		if (bearingInDegrees > 360f)  bearingInDegrees %= 360f;
		if (bearingInDegrees < -360f) bearingInDegrees %= 360f;
		if (bearingInDegrees > 180f)  bearingInDegrees -= 360f;  
		
		return bearingInDegrees;
	}
	
	
	
		
		
		
	
    //**********************************************************************//
 	// Show Privacy Policy.													// 
 	//**********************************************************************//
 	public static void showPrivacyPolicy(Context context)
 	{
 		final Dialog dialogTC	= new Dialog(context);
 		dialogTC.setContentView(R.layout.privacy_policy_dialog);
 		Button buttonOK  = (Button) dialogTC.findViewById(R.id.buttonOK);
 
 		//******************************************************************//
 		// Wot; an anonymous inner class?  But I hate those!				//
 		//******************************************************************//
 		buttonOK.setOnClickListener(new OnClickListener() 
 		{
 			@Override
 			public void onClick(View v) 
 			{
 				dialogTC.dismiss();
 			}
 		});
 		
 		 		
 		//******************************************************************//
 		// Wot; an anonymous inner class?  But I hate those!				//
 		//******************************************************************//
 		dialogTC.setOnCancelListener(new OnCancelListener() 
 		{
 			
 			@Override
 			public void onCancel(DialogInterface dialog) 
 			{
 				dialogTC.dismiss();
 			}
 		});
 	
 		dialogTC.setTitle(R.string.privacyPolicy);
 		dialogTC.show();
 	}
 	


 	
 	//**********************************************************************//
 	// Show Disclaimer.														// 
 	//**********************************************************************//
 	public static void showDisclaimer(Context context)
 	{
 		final Dialog dialogTC	= new Dialog(context);
 		dialogTC.setContentView(R.layout.disclaimer_dialog_layout);
 		
 		Button buttonAgree = (Button)dialogTC.findViewById(R.id.buttonAgree);
 		Button buttonCancel= (Button)dialogTC.findViewById(R.id.buttonCancel); 
 		
 		//******************************************************************//
 		// Wot; an anonymous inner class?  But I hate those!				//
 		//																	//
 		// And another thing, this show you must read all is well hidden, 	//
 		// there should be a better state model.							//
 		//******************************************************************//
 		buttonAgree.setOnClickListener(new OnClickListener() 
 		{
			@Override
			public void onClick(View v) 
			{
				dialogTC.dismiss();
				showYouMustReadAllInfo(dialogTC.getContext());
			}
		});
 		

 		
 		//******************************************************************//
 		// Wot; an anonymous inner class?  But I hate those!				//
 		//******************************************************************//
 		buttonCancel.setOnClickListener(new OnClickListener() 
 		{
			@Override
			public void onClick(View v) 
			{
				System.exit(0);
			}
		});

 		
 		//******************************************************************//
 		// Wot; an anonymous inner class?  But I hate those!				//
 		//******************************************************************//
 		dialogTC.setOnCancelListener(new OnCancelListener() 
 		{
 			
 			@Override
 			public void onCancel(DialogInterface dialog) 
 			{
 				System.exit(0);
 			}
 		});
 	
 		WindowManager wm = (WindowManager) 
 				           context.getSystemService(context.WINDOW_SERVICE);
 		int width        = wm.getDefaultDisplay().getWidth();
 		
 		dialogTC.setTitle(R.string.disclaimer);
 		dialogTC.show();
 	}
 	

 	
 	
 	
 	
     
  	
 	//**********************************************************************//
 	// Show "you must read all" dialog.										// 
 	//**********************************************************************//
 	public static void showYouMustReadAllInfo(Context context)
 	{
 		AlertDialog.Builder dialogYouMust = new AlertDialog.Builder(context);
 		dialogYouMust.setTitle(R.string.pleaseNote);
 		dialogYouMust.setMessage(R.string.youMustreadAllChaptersOfSecchiInfo);
 		dialogYouMust.setPositiveButton(R.string.OK, 
 				new android.content.DialogInterface.OnClickListener()
 		{

 	 		//******************************************************************//
 	 		// Wot; an anonymous inner class?  But I hate those!				//
 	 		//******************************************************************//
			@Override
			public void onClick(DialogInterface arg0, int arg1) 
			{ } 
  		});
 				 		
 		dialogYouMust.show();
 	}
 	
        
  
 	
 	//**********************************************************************//
 	// Show what's new.	For the minute, just use the built in PDF viewer.	//											// 
 	//**********************************************************************//
 	public static void showWhatsNew(Context context)
 	{
 		final Dialog dialogWN	= new Dialog(context);
 		dialogWN.setContentView(R.layout.whats_new_dialog_layout);
 	 	dialogWN.setTitle(R.string.whatsNew);
 		
 		Button  buttonCancel= (Button) dialogWN.findViewById(R.id.buttonCancel); 

 	 		
 	 		
		//******************************************************************//
 		// Wot; an anonymous inner class?  But I hate those!				//
 		//******************************************************************//
 		dialogWN.setOnCancelListener(new OnCancelListener() 
 		{ 			
 			@Override
 	 		public void onCancel(DialogInterface dialog) 
 	 		{
 				dialogWN.dismiss();
 			}
 	 	});
 		
 		
		//******************************************************************//
 		// Wot; an anonymous inner class?  But I hate those!				//
 		//******************************************************************//
 		buttonCancel.setOnClickListener(new View.OnClickListener() 
 		{
			
			@Override
			public void onClick(View v) 
			{
				dialogWN.dismiss();
			}
		});	
 	 	
 	 	WindowManager wm = (WindowManager) 
 	 			           context.getSystemService(context.WINDOW_SERVICE);
 	 	int width         = wm.getDefaultDisplay().getWidth();
 	 		
 	 	dialogWN.show();
 	 }
 	 

 	
	
	//**********************************************************************//
	// Share as email.														//
	//																		//
	// This is megga easy except that not all the intent extra this, that   //
	// and the other are intuitively named.									//
	//																		//
	// Intent.ACTION_SEND does not fire up email particularly, it fires up	//
	// any application that knows what to do with the action we specify.	//
	//**********************************************************************//
	public static void shareAsEmail(Context context, 
			                        String[] to,    String subject, 
			                        String content, String fileToShare)
	{
		File fileAttach = null;
		Uri  uriAttach  = null;

		//******************************************************************//
		// Prepare the MIME attachment, which must be presented as a URI.	//
		//******************************************************************//
		if (fileToShare != null)
		{
			fileAttach = new File(fileToShare);
		}
		
		if (fileToShare != null) 
			uriAttach = Uri.parse("file://" + fileAttach); 

		
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
        //emailIntent.setType("application/octet-stream");	//rfc822
		emailIntent.setType("message/rfc822");
        //emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, to);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, content);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
        if (uriAttach != null) 
        	emailIntent.putExtra(Intent.EXTRA_STREAM, uriAttach);
        context.startActivity(Intent.createChooser(emailIntent, 
        		context.getString(R.string.shareByEmail)));
       	}
	
	
	
	
	
	//**********************************************************************//
	// Share as web site.													//
	//																		//
	// Intent.ACTION_VIEW does not fire up a browser particularly, it fires //
	// up any application that knows what to do with the action we specify.	//
	//**********************************************************************//
	public static void shareWithWebSite(Context context, 
			                            String title, 
			                            String url)
	{
		Uri uriAttach = Uri.parse(url); 
		
		try
		{
			Intent webIntent = new Intent();
			webIntent.setAction(Intent.ACTION_VIEW);
			webIntent.setData(uriAttach);
	        context.startActivity(Intent.createChooser(webIntent, title));
		}
		catch (Exception doh)
		{
			String errStr = context.getString(R.string.cantOpenWebPage)
					        + ": "
					        + doh.getMessage();
			Toast.makeText(context, 
					       errStr, 
					       Toast.LENGTH_SHORT).show();
		}
	}
	
	
	
	//**********************************************************************//
	// Share with Google Urf.												//
	//																		//
	// Intent.ACTION_SEND does not fire up email particularly, it fires up	//
	// any application that knows what to do with the action we specify.	//
	//																		//
	// Now Wirking.															//
	//**********************************************************************//
	public static void shareWithGoogleEarth(Context context, 
			                                String  fileToView)
	{
		//******************************************************************//
		// Prepare the attachment, which must be presented as a URI.		//
		//******************************************************************//
		File     fileAttach = new File(fileToView);
		if (!fileAttach.exists())
		{
			if (D.debug) Log.e(context.getApplicationInfo().className, 
					           "The file we are supposed to attach do Google Earth doesn't exist!");
			return;
		}
		Uri uriAttach = Uri.parse("file://" + fileAttach); 

		try
		{
			Intent urfIntent = new Intent();
			urfIntent.setAction(Intent.ACTION_VIEW);
			urfIntent.setType("application/kml");
			urfIntent.setData(uriAttach);
	        context.startActivity(Intent.createChooser(urfIntent, context.getString(R.string.shareByGoogleEarth)));
		}
		catch (Exception noUrf)
		{
			Toast.makeText(context, R.string.googleEarthIsntHere, Toast.LENGTH_SHORT).show();
		}
	}

	
	
	
	//**********************************************************************//
	// Check if there is a camera app present.								//
	//**********************************************************************//
	public static boolean isCameraintentAvailable(Context context)
	{
		 PackageManager packageManager = context.getPackageManager();    
		 Intent intent                  = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		 List<ResolveInfo> list         = packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);    
		 
		 return (list.size() > 0);
	}
	
}	// End of classy class
