package uk.ac.plymouth.matmutt.secchi.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment
import uk.ac.plymouth.matmutt.secchi.view.SecchiFragmentConfig.Companion.DESCRIPTION
import uk.ac.plymouth.matmutt.secchi.view.SecchiFragmentConfig.Companion.NEGATIVE_TEXT
import uk.ac.plymouth.matmutt.secchi.view.SecchiFragmentConfig.Companion.POSITIVE_TEXT
import uk.ac.plymouth.matmutt.secchi.view.SecchiFragmentConfig.Companion.TITLE

/**
 * Created by David Crotty on 28/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class SecchiDialogFragment : AppCompatDialogFragment() {

    private var listener : DialogProvider.SateliteDialogListener? = null

    companion object : SecchiFragmentConfig() {
        val TAG = "SecchiDialog"
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is DialogProvider.SateliteDialogListener) {
            listener = context
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setTitle(arguments.getString(TITLE))
                .setMessage(arguments.getString(DESCRIPTION))
                .setPositiveButton(arguments.getString(POSITIVE_TEXT), { dialog, which ->
                    listener?.showSatelliteStatus()
                    dismiss()
                })
                .setNegativeButton(arguments.getString(NEGATIVE_TEXT), { dialog, which ->
                    listener?.initiateGPSSearch()
                    dismiss()
                })
                .create()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
    }
}