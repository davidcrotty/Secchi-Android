//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Secchi Disk depth Activity.  A dragable disk, really						//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import io.realm.Realm;
import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.view.components.SecchiDepthView;
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.interfaces.ISecchiDepthlistener;
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork;
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class SecchiDepthActivity extends Activity implements
												  ISecchiDepthlistener,
												  OnClickListener,
												  OnLongClickListener,
												  OnKeyListener
{
	public static String DEPTH_INTENT_KEY = "DEPTH_INTENT_KEY";
	public static String TIME_OF_READING_KEY = "TIME_OF_READING_DEPTH_KEY";
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName           = "";
	private SecchiDepthView        secchiDepthView        = null;
	private TextView               textViewSecchiDepth    = null;
	private Button				   buttonPlus             = null;
	private Button                 buttonMinus            = null; 
	private Button                 buttonSave             = null;
	private Button                 buttonCancel           = null;
	private float                  secchiDepth            = 0;
	private IncDecThread           incDecThread           = null;
	private ImageView              imageViewSoftKeyboard  = null;
	private EditText               editTextDialogByKebd	  = null;
	private UnitOfWork unitOfWork = null;
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secchi_depth_activity_layout);

        activityName           = getClass().getSimpleName();


        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        secchiDepthView       = (SecchiDepthView) findViewById(R.id.secchiDepthView);
        textViewSecchiDepth   = (TextView)        findViewById(R.id.editTextSecchiDepth);
        buttonPlus            = (Button)          findViewById(R.id.buttonPlus);
        buttonMinus           = (Button)          findViewById(R.id.buttonMinus);
        buttonSave            = (Button)          findViewById(R.id.buttonSave);
        buttonCancel          = (Button)          findViewById(R.id.buttonCancel);
        imageViewSoftKeyboard = (ImageView)       findViewById(R.id.imageViewSoftKeyboard);
    
        secchiDepthView.setSccchiDepthListener(this);
        buttonPlus.setOnClickListener(this);
        buttonMinus.setOnClickListener(this);
        buttonSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        buttonPlus.setOnLongClickListener(this);
        buttonMinus.setOnLongClickListener(this);
		unitOfWork = new UnitOfWork(new RealmContext(Realm.getDefaultInstance()));
    }

   	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiDepthView.stopWeeAnimation();
	   
	   if (incDecThread != null)
	   {
		   incDecThread.running = false;
		   incDecThread         = null;
	   }
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		ApplicationState applicationState = unitOfWork.getRealmContext().getApplicationState();
		if(applicationState != null) {
			if(applicationState.getInProgressReading() != null) {
				Float depth = applicationState.getInProgressReading().getDepth();
				if(depth != null) {
					secchiDepthView.setSecchiDepth(depth);
				}

				setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

				secchiDepthView.doWeeAnimation();
				imageViewSoftKeyboard.setOnClickListener(null);
				imageViewSoftKeyboard.setVisibility(View.GONE);
			}
		}
		unitOfWork.complete();
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }


     //**********************************************************************//
     // Overriden from ISecchiDepthlistener.								 //
     //**********************************************************************//
     @Override
     public void onDepthChanged(float newDepth, boolean slider) 
     {
    	 //*****************************************************************//
    	 // Ignore once of the last data was from the keyboard.				//
    	 //*****************************************************************//
    	 if (!slider) return;
    	 

    	 
    	 
    	 //*****************************************************************//
    	 // Nick's Formula 													//
    	 //*****************************************************************//
         // newDepth += 1;	//log(0) = 1 
         //newDepth = (float) Math.log(newDepth*20);	//*sort

    	 //newDepth = 
    	 
    	 secchiDepth = newDepth;
    	 
    	 String s = Enumerators.roundFloatToNdp(newDepth, 1)
    			    + " "
    			    + getString(R.string.metresCondensed);
    	 textViewSecchiDepth.setText(s);
     }
   
     
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		switch (view.getId())
 		{
 			case R.id.buttonPlus:
 				 secchiDepthView.incrementSecchiDepth();
 				 break;
 				 
 			case R.id.buttonMinus:
 				 secchiDepthView.decrementSecchiDepth();
 				 break;
 				 
 			case R.id.buttonSave:
				 Intent result = new Intent();
				 result.putExtra(DEPTH_INTENT_KEY, secchiDepth);
				 result.putExtra(TIME_OF_READING_KEY, System.currentTimeMillis());
				 setResult(Activity.RESULT_OK, result);
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.buttonCancel:
				 Intent cancelResult = new Intent();
				 setResult(Activity.RESULT_CANCELED, cancelResult);
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.imageViewSoftKeyboard:
 				 //*********************************************************//
 				 // Build a crude alert dialog and put in an EditText.		//
 				 //*********************************************************//
 				 AlertDialog.Builder dialogDepth = new AlertDialog.Builder(this);

 				 dialogDepth.setTitle(R.string.enterDepth);
 				 dialogDepth.setMessage(R.string.enterDepth0to100);

 				 editTextDialogByKebd = new EditText(getApplicationContext());
 				 editTextDialogByKebd.setInputType(InputType.TYPE_CLASS_NUMBER
 						                         | InputType.TYPE_NUMBER_FLAG_DECIMAL);
 				 dialogDepth.setView(editTextDialogByKebd);

 				 dialogDepth.setPositiveButton(getString(R.string.OK), 
 						    new DialogInterface.OnClickListener() 
 				 {
	 				 public void onClick(DialogInterface dialog, int whichButton) 
	 				 {
	 					 try
	 					 {
	 						 String strAns = editTextDialogByKebd.getText().toString();
	 						 float  ans    = Float.valueOf(strAns);
	 						 ans           = Math.round(ans * 10f)/10f;
	 				
	 						 if ((ans < 0) || (ans > 100))
	 								 throw new NumberFormatException("Out of range");
	 						 
	 						 secchiDepth        = ans;
	 						 textViewSecchiDepth.setText(String.valueOf(ans));
	 						 secchiDepthView.setSecchiDepth(secchiDepth);
	 					 }
	 					 catch (Exception ee)
	 					 {
	 						 Toast.makeText(getApplicationContext(), 
	 								 R.string.pleaseEnterValidDepth, 
	 								 Toast.LENGTH_LONG).show();
	 					 }
	 				  }
	 				});


 				dialogDepth.show();
 				break;
 				
 		}
 	}


 	
  	//***********************************************************************//
  	// Overridden from View.OnLongclickListener.							 //
  	//***********************************************************************//
 	@Override
	public boolean onLongClick(View view) 
 	{
 		boolean down = view.isPressed();
 		if (D.debug)
 			Log.i(activityName, "button long pressed: down = " + down);
 	
 		if (incDecThread == null)
 			incDecThread = new IncDecThread();
 		
 		
 		switch (view.getId())
 		{
 			case R.id.buttonPlus:
 				 if (!incDecThread.running)
 					 incDecThread.increment = true;
 					 incDecThread.execute();
 				 break;
 				 
 			case R.id.buttonMinus:
				 if (!incDecThread.running)
 					 incDecThread.increment = false;
 					 incDecThread.execute();
 				 break;
 		}
 		return true;
	}


 	
  	//***********************************************************************//
  	// Overridden from View.OnKeyListener.									 //
  	//***********************************************************************//
 	@Override
	public boolean onKey(View v, int keyCode, KeyEvent event) 
 	{
		return false;
	}

 	
    //**********************************************************************//
 	// Inner class for a Thread to auto increment / decrement secchi depth.	//
 	//**********************************************************************//
 	private class IncDecThread extends AsyncTask<Void, Void, Void>
 	{

 		//******************************************************************//
 		// Public instance variables; oops.									//
 		//******************************************************************//
 		public volatile boolean running   = false;
 		public volatile boolean increment = false;
 		
 		
    	//******************************************************************//
    	// Executed in a separate thread in the background					//
    	// Weird syntax for "some parameters may follow, but I don't know   //
    	// anything about them".... 										//
    	//******************************************************************//
		@Override
		protected Void doInBackground(Void... params) 
		{
			running = true;
			while (running)
			{	
				try 
				{
					Thread.sleep(20);		// 1/10 second
					publishProgress();  	// Invokes onProgressUpdate()
				} 
				catch (InterruptedException e) 
				{
					Log.i(activityName, "Timer Thread interrupted");
				}
			}
    		return null;
		}
 		

		
		
		//******************************************************************//
    	// Invoked when we execute publishProgress().						//
    	//******************************************************************//
    	@Override
        protected void onProgressUpdate(Void... progress) 
    	{
    		boolean pressed = (buttonPlus.isPressed()
    				        || buttonMinus.isPressed());
    		
    		//**************************************************************//
    		// If nothing pressed, stop the thread and return.  Roy look	//
    		// away.														//
    		//**************************************************************//
    		if (!pressed)
    		{
    			running      = false;
    			incDecThread = null;
    			return;
    		}
    		
    		//******************************************//
    		// Going up or down.....					//
    		//******************************************//
    		if (increment)
    			secchiDepthView.incrementSecchiDepth();
    		else
    			secchiDepthView.decrementSecchiDepth();
    	}    	
    } //End of AsyncTask

}	// End of classy class.
