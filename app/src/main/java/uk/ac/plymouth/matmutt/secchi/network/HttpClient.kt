package uk.ac.plymouth.matmutt.secchi.network

import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

/**
 * Created by David Crotty on 05/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class HttpClient {
    companion object Factory {
        fun createTimeoutInterceptorWith(timeoutMS: Long) : OkHttpClient.Builder {
            val httpClientBuilder = OkHttpClient.Builder()
            httpClientBuilder.connectTimeout(timeoutMS, TimeUnit.SECONDS)
            httpClientBuilder.readTimeout(timeoutMS, TimeUnit.SECONDS)
            httpClientBuilder.writeTimeout(timeoutMS, TimeUnit.SECONDS)

            return httpClientBuilder
        }
    }
}