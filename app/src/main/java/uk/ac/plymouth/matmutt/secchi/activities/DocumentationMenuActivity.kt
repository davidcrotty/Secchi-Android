package uk.ac.plymouth.matmutt.secchi.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_documentation_menu.*
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.domain.Category
import uk.ac.plymouth.matmutt.secchi.domain.DocumentationPage
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import uk.ac.plymouth.matmutt.secchi.view.PushDialogListener
import uk.ac.plymouth.matmutt.secchi.view.components.NotificationCardView

class DocumentationMenuActivity : AppCompatActivity() {

    companion object { //Needed to allow static extension method
        val SECCHI_APP: Int = 0
        val WHAT_IS_DISK: Int = 5
        val MAKING_DISK: Int = 8
        val USING_DISK: Int = 10
        val OPTIONAL_OBS: Int = 18
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_documentation_menu)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = ""

        the_secchi_app.setOnClickListener {
            DocumentActivity.startWith(this, SECCHI_APP)
        }
        what_is_disk.setOnClickListener {
            DocumentActivity.startWith(this, WHAT_IS_DISK)
        }
        making_disk.setOnClickListener {
            DocumentActivity.startWith(this, MAKING_DISK)
        }
        using_disk.setOnClickListener {
            DocumentActivity.startWith(this, USING_DISK)
        }
        optional_observations.setOnClickListener {
            DocumentActivity.startWith(this, OPTIONAL_OBS)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setStateFor(view: NotificationCardView, pages: Int?) {
        if(pages == null) return
        if(pages > 0) {
            view.notificationNumber(pages)
        } else {
            view.showNotificiationTick()
        }
    }

    override fun onResume() {
        super.onResume()
        val repository = RealmContext(Realm.getDefaultInstance())
        val secchiAppPages = repository.findAll<DocumentationPage>({page -> page.category == Category.THE_SECCHI_APP.name && page.read == false })
        setStateFor(the_secchi_app, secchiAppPages?.size)

        val whatIsDiskPages = repository.findAll<DocumentationPage>({page -> page.category == Category.WHAT_IS_DISK.name && page.read == false })
        setStateFor(what_is_disk, whatIsDiskPages?.size)

        val makingDiskPages = repository.findAll<DocumentationPage>({page -> page.category == Category.MAKING_A_DISK.name && page.read == false })
        setStateFor(making_disk, makingDiskPages?.size)

        val usingDiskPages = repository.findAll<DocumentationPage>({page -> page.category == Category.USING_A_DISK.name && page.read == false })
        setStateFor(using_disk, usingDiskPages?.size)

        val optionalObservationPages = repository.findAll<DocumentationPage>({page -> page.category == Category.OPTIONAL_OBSERVATIONS.name && page.read == false })
        setStateFor(optional_observations, optionalObservationPages?.size)

        //Realm API does not expose generic predicate filter for it's list, so filter the results again
        val pagesRead = repository.findAll<DocumentationPage> { page -> page.read == false }
        if(pagesRead?.isEmpty() == true) {
            val unitOfWork = UnitOfWork(repository)
            val state = unitOfWork.getRealmContext().getApplicationState() ?: return
            state.setReadAllDocumentationPages(true)
            unitOfWork.getRealmContext().add(RealmTransaction(state))
        }

        repository.complete()
        PushDialogListener.register(this@DocumentationMenuActivity)
    }

    override fun onPause() {
        super.onPause()
        PushDialogListener.unregister(this@DocumentationMenuActivity)
    }

}

fun DocumentationMenuActivity.Companion.startFrom(activity: AppCompatActivity) {
    val intent = Intent(activity, DocumentationMenuActivity::class.java)
    activity.startActivity(intent)
}
