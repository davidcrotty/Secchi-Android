package uk.ac.plymouth.matmutt.secchi.rx

import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import uk.ac.plymouth.matmutt.secchi.view.IMainMenuView

/**
 * Created by David Crotty on 07/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class MenuWizard(val unitOfWork: UnitOfWork,
                 val view: IMainMenuView,
                 val readTermsSubject: PublishSubject<Boolean>) :  Disposable {

    private var disposable: Disposable? = null

    override fun isDisposed(): Boolean {
        return disposable?.isDisposed ?: true
    }

    override fun dispose() {
        disposable?.dispose()
    }

    fun observeDialogReading() : Observable<ApplicationState> {
        return Observable.create<ApplicationState> { observable ->
            val currentState = unitOfWork.getRealmContext().getApplicationState()

            if(currentState == null) {
                unitOfWork.getRealmContext().add(RealmTransaction(ApplicationState()))
            }

            observable.onNext(currentState)
        }
        .groupBy { model ->
            if(model.termsAndConditionsShown) {
                "true"
            } else {
                "false"
            }
        }
        .flatMap { model ->
            if(model.key == "true") {
                return@flatMap model
            } else {
                view.showTerms()
                model.zipWith(readTermsSubject, BiFunction<ApplicationState, Boolean, ApplicationState> { state, hasReadTerms ->
                    if(state != null) {
                        state.termsAndConditionsShown = hasReadTerms
                        val transaction = RealmTransaction(state)
                        unitOfWork.getRealmContext().add(transaction)
                        unitOfWork.complete()
                    }
                    return@BiFunction state
                })
            }
        }
    }
}

