package uk.ac.plymouth.matmutt.secchi.domain

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import java.util.*

/**
* Created by David Crotty on 01/04/2017.
*
* Copyright © 2017 David Crotty - All Rights Reserved
*
*
*/
open class SecchiReading : RealmObject {

    @PrimaryKey
    var primaryKey: String? = null
    var vesselName : String? = BuildConfig.BOAT_NAME_DEFAULT //Instance of a boat
    var lattiutde: Float? = null
    var longitude: Float? = null
    var depth: Float? = null
    var temperature: Float? = null
    var photoPath: String? = null
    var timeOfLocation: Long? = null
    var timeOfDepth: Long? = null
    var timeOfTemperature: Long? = null
    var deviceInfo: String? = null
    var notes: String? = null
    var extra: String? = null
    var uploadStatus: Int = UploadStatus.PENDING

    constructor()

    constructor(vesselName: String?,
                lattitude: Float?,
                longitude: Float?,
                depth: Float?,
                temperature: Float?,
                photoPath: String?,
                timeOfLocation: Long?,
                timeOfDepth: Long?,
                timeOfTemperature: Long?,
                deviceInfo: String?,
                notes: String?,
                extra: String?,
                isSubmitted: Int) {
        this.vesselName = vesselName
        this.lattiutde = lattitude
        this.longitude = longitude
        this.temperature = temperature
        this.photoPath = photoPath
        this.timeOfLocation = timeOfLocation
        this.timeOfDepth = timeOfDepth
        this.timeOfTemperature = timeOfTemperature
        this.deviceInfo = deviceInfo
        this.notes = notes
        this.depth = depth
        this.extra = extra
        this.uploadStatus = isSubmitted
    }

    init {
        primaryKey = UUID.randomUUID().toString()
    }

    fun isValidModel() : Boolean {
        if(lattiutde == null ||
                longitude == null ||
                depth == null ||
                timeOfLocation == null ||
                timeOfDepth == null ||
                vesselName == null) {
            return false
        }
        return true
    }
}