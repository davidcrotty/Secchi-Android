package uk.ac.plymouth.matmutt.secchi.view.model

/**
 * Created by David Crotty on 23/08/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class Heading(val title: String, val colourResource: Int, val uploadedHeading: Boolean)