//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// A class to contain a single Secchi reading.								//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.helpers;

import java.io.IOException;
import java.io.Serializable;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import uk.ac.plymouth.matmutt.secchi.interfaces.IXMLSerializable;

import android.util.Log;



//**********************************************************************//
// Ihis reading can write itself as XML and also sort itself.			//
//**********************************************************************//
public class SecchiReadingX implements Serializable,
	                                   IXMLSerializable<SecchiReadingX>,
									   Comparable<SecchiReadingX>
{
	private static final long serialVersionUID = 1;

	
	//**********************************************************************//
	// Instance variables.                                                  //
	//**********************************************************************//
	private String  activityName        = "Secchi Reading";
	private int     id                  = 0;
	private String  vesselName			= "";
	private float   latitude            = 0;
	private float   longitude           = 0;
	private long    timeOfLocation      = 0;
	private long    timeOfDepth			= 0;
	private long    timeOfTemperature   = 0;
	private float   depth               = 0;
	private float   temperature         = 0;
	private String  notes               = "";
	private String  photoLocation       = "null";
	private String  extra               = "";
	private final float minDepth 		= 0f;
	private final float maxDepth        = 100f;
	private boolean mostRecentFirst     = true;		// Used to sort, need not
													// be preserved.

	
	
	//**********************************************************************//
	// Constructors.                                                        //
	//**********************************************************************//
	public SecchiReadingX() 
	{
	}


	//**********************************************************************//
	// This is the constructor that is usually used.  The ID is normally	//
	// allocated by the package variables when we submit the reading.		//
	//**********************************************************************//

	public SecchiReadingX(String vesselName, 
			              float latitude, float longitude, long timeoflocation,
            			  float depth, long timeofDepth, 
            			  float temperature, long timeOfTemperature,
            			  String notes, String photoLocation, String extra) 
	{
		super();
		this.latitude       = latitude;       this.longitude         = longitude;
		this.timeOfLocation = timeoflocation; this.temperature       = temperature;
		this.notes          = notes;          this.photoLocation     = photoLocation;
		this.depth          = depth;	      this.extra             = extra;
		this.timeOfDepth    = timeofDepth;    this.timeOfTemperature = timeOfTemperature;
		this.vesselName     = vesselName; 
	}

	
	//**********************************************************************//
	// As above, but with an extra parameter, an ID.						//
	//**********************************************************************//

	public SecchiReadingX(int id, String vesselName,
			             float latitude, float longitude, long timeoflocation,
			             float depth, long timeofDepth, 
			             float temperature, long timeOfTemperature,
			             String notes, String photoLocation,
			             String extra)  
	{
		super();
		this.latitude       = latitude;       this.longitude         = longitude;
		this.timeOfLocation = timeoflocation; this.temperature       = temperature;
		this.notes          = notes;          this.photoLocation     = photoLocation;
		this.depth          = depth;	      this.extra             = extra;
		this.timeOfDepth    = timeofDepth;    this.timeOfTemperature = timeOfTemperature;
		this.id             = id;	          this.vesselName        = vesselName;
	}

	

	

	
	//**********************************************************************//
	// Roy stuff.															//
	//**********************************************************************//
	@Override
	public boolean writeXMLToFile(XmlSerializer serializer) throws IOException 
	{

		boolean blnWritten = false;
		if (null != serializer) 
		{
			serializer.startTag("", "SecchiReading");
			// Id
			serializer.startTag("", "ID");
			serializer.text(String.valueOf(this.id));
			serializer.endTag("", "ID");
			// Boat name
			serializer.startTag("", "vesselName");
			serializer.text(vesselName);
			serializer.endTag("", "vesselName");
			// Latitude
			serializer.startTag("", "latitude");
			serializer.text(String.valueOf(this.latitude));
			serializer.endTag("", "latitude");
			// Longitude
			serializer.startTag("", "longitude");
			serializer.text(String.valueOf(this.longitude));
			serializer.endTag("", "longitude");
			// Secchi Depth
			serializer.startTag("", "depth");
			serializer.text(String.valueOf(this.depth));
			serializer.endTag("", "depth");
			// Temperature
			serializer.startTag("", "temperature");
			serializer.text(String.valueOf(temperature));
			serializer.endTag("", "temperature");
			// Notes
			serializer.startTag("", "notes");
			serializer.text(notes);
			serializer.endTag("", "notes");
			// Path to photo
			serializer.startTag("", "photoLocation");
			serializer.text(photoLocation);
			serializer.endTag("", "photoLocation");
			// Date date of location
			serializer.startTag("", "timeOfLocation");
			serializer.text(String.valueOf(timeOfLocation));
			serializer.endTag("", "timeOfLocation");
			// Date date of Secchi depth
			serializer.startTag("", "timeOfDepth");
			serializer.text(String.valueOf(timeOfDepth));
			serializer.endTag("", "timeOfDepth");
			// Date date of temperature
			serializer.startTag("", "timeOfTemperature");
			serializer.text(String.valueOf(timeOfTemperature));
			serializer.endTag("", "timeOfTemperature");
			// Extra
			serializer.startTag("", "extra");
			serializer.text(extra);
			serializer.endTag("", "extra");
	
			serializer.endTag("", "SecchiReading");
			blnWritten = true;
		}
		return blnWritten;
	}

	
	
	
	
	//**********************************************************************//
	// Roy stuff.	Poggled by Nigel.										//
	//**********************************************************************//
	@Override
	public SecchiReadingX loadFromXMLFile(XmlPullParser xmlParser)
			throws IOException 
	{
		SecchiReadingX objResult = null;
		try
		{
		
			//*************************************************************//
			// Set everything to a sensible default, in case no tag exists.//
			//*************************************************************//
			latitude            = 0;
			longitude           = 0;
			id                  = 0;
			vesselName			= "";
			timeOfLocation      = 0;
			timeOfDepth			= 0;
			timeOfTemperature   = 0;
			depth               = 0;
			temperature         = 0;
			notes               = "";
			photoLocation       = "null";
			extra               = "";
	
			if(null != xmlParser)
			{
				if (xmlParser.getName().equalsIgnoreCase("SecchiReading"))
				{
					if (xmlParser.getEventType() != XmlPullParser.START_TAG)
						throw new Exception ("no start Secchi Reading tag");
					
					xmlParser.next();
					
					boolean endTag = false;;
					boolean entry  = false;
					while (true)			//exit with a break;
					{
						endTag = (xmlParser.getEventType() == XmlPullParser.END_TAG);
						entry  = (xmlParser.getName().equalsIgnoreCase("SecchiReading"));
						
						
						//**********************************************//
						// Some pretty fiendish logic.  I think this is	//
						// only necessary because of the status 	    //
						// variables being evaluated in the loop to make//
						// debugging easier.							//
						//**********************************************//
						if (!(!entry && !endTag)) break;
				
						if (xmlParser.getEventType() == XmlPullParser.START_TAG) 
						{
							this.processXMLTag(xmlParser);
						}
						xmlParser.next();
					}
					
					xmlParser.next();			//Point past closing tag
				}
			}
		}
		catch (Exception ee)
		{
			if (D.debug) 
				Log.e(activityName, "Error reading XML "
						+ ee.getMessage());
		}
					
		objResult = new SecchiReadingX(id, vesselName,
				           			   latitude, longitude, timeOfLocation,
				           			   depth, timeOfDepth, 
				           			   temperature,  timeOfTemperature,
				           			   notes, photoLocation, 
				           			   extra);
		return objResult;
	}

	
	
	
	
	//**********************************************************************//
	// Process an XML tag.  These can now arrive in any order.				//
	//**********************************************************************//
	private void processXMLTag(XmlPullParser xmlParser) 
	{
		try 
		{
			
			//**************************************************************//
			// Check we have parser positioned at START TAG.				//
			//**************************************************************//
			if (null != xmlParser && xmlParser.getEventType() != XmlPullParser.END_TAG) 
			{
				// Extract Tag Name
				String strTagName = xmlParser.getName();
				
				if (D.debug) Log.i(activityName, "XML tag " + strTagName);

				
				if (strTagName.equalsIgnoreCase("latitude")) 
				{
					String strData = xmlParser.nextText();
					latitude       = Float.parseFloat(strData);
				}
				if (strTagName.equalsIgnoreCase("longitude")) 
				{
					String strData = xmlParser.nextText();
					longitude      = Float.parseFloat(strData);
				}
				if (strTagName.equalsIgnoreCase("ID")) 
				{
					String strData = xmlParser.nextText();
					id           = Integer.parseInt(strData);
				}
				if (strTagName.equalsIgnoreCase("depth")) 
				{
					String strData = xmlParser.nextText();
					depth          = Float.parseFloat(strData);
				}
				if (strTagName.equalsIgnoreCase("temperature")) 
				{
					String strData = xmlParser.nextText();
					temperature    = Float.parseFloat(strData);
				}
				if (strTagName.equalsIgnoreCase("notes")) 
				{
					String strData = xmlParser.nextText();
					notes          = strData;
				}
				if (strTagName.equalsIgnoreCase("photoLocation")) 
				{
					String strData = xmlParser.nextText();
					photoLocation  = strData;
				}
				if (strTagName.equalsIgnoreCase("timeOfLocation")) 
				{
					String strData = xmlParser.nextText();
					timeOfLocation = Long.parseLong(strData);
				}
				if (strTagName.equalsIgnoreCase("timeOfDepth")) 
				{
					String strData = xmlParser.nextText();
					timeOfDepth    = Long.parseLong(strData);
				}
				if (strTagName.equalsIgnoreCase("timeOfTemperature")) 
				{
					String strData    = xmlParser.nextText();
					timeOfTemperature = Long.parseLong(strData);
				}
				if (strTagName.equalsIgnoreCase("extra")) 
				{
					String strData = xmlParser.nextText();
					extra          = strData;
				}
				if (strTagName.equalsIgnoreCase("vesselName")) 
				{
					String strData = xmlParser.nextText();
					vesselName     = strData;
				}
			}
		} 
		catch (XmlPullParserException ex) 
		{
			if (D.debug) Log.e(activityName, "Pull Parser Exception: " + ex.getMessage());
		} 
		catch (IOException ex) 
		{
			if (D.debug) Log.e(activityName, "I/O Exception: " + ex.getMessage());
		} 
		catch (NumberFormatException ex) 
		{
			if (D.debug) Log.e(activityName, "Number Format Exception: " + ex.getMessage());
		} 
		catch (Exception doh) 
		{
			if (D.debug)
				Log.e(activityName, "Error loading Secchi ehtry: "
						+ doh.getMessage());
		}
	}
		
	
	
	
	//**************************************************************************//
	// Implementation of interface CompareTo.  Compare by date of location.		//
	//**************************************************************************//
	@Override
	public int compareTo(SecchiReadingX another) 
	{
		long dateDifference = timeOfLocation - another.timeOfLocation;
		int  result = 0;
		if (dateDifference > 0) result = 1;
		if (dateDifference < 0) result = -1;
		if (mostRecentFirst) result = -result;  
		
		return result;
	}


	
	//**************************************************************************//
	// Getters and setters; Mary would approve.                                 //
	//**************************************************************************//


	/**
	 * @return the latitude
	 */
	public float getLatitude() {
		return latitude;
	}


	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}


	/**
	 * @return the longitude
	 */
	public float getLongitude() {
		return longitude;
	}


	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}


	


	/**
	 * @return the depth.  Do a tiny bit of cleaning
	 */
	public float getDepth() 
	{
		float result = depth;
		if (result < minDepth) result = minDepth;
		if (result > maxDepth) result = maxDepth;
		return result;
	}


	/**
	 * @param depth the depth to set. Do a tiny bit of cleaning
	 */
	public void setDepth(float depth) 
	{
		if (depth < minDepth) depth = minDepth;
		if (depth > maxDepth) depth = maxDepth;
		this.depth = depth;
	}


	/**
	 * @return the temperature
	 */
	public float getTemperature() {
		return temperature;
	}


	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}


	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}


	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}


	/**
	 * @return the photoLocation
	 */
	public String getPhotoLocation() {
		return photoLocation;
	}


	/**
	 * @param photoLocation the photoLocation to set
	 */
	public void setPhotoLocation(String photoLocation) {
		this.photoLocation = photoLocation;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the timeOfLocation
	 */
	public long getTimeOfLocation() {
		return timeOfLocation;
	}


	/**
	 * @param timeOfLocation the timeOfLocation to set
	 */
	public void setTimeOfLocation(long timeOfLocation) {
		this.timeOfLocation = timeOfLocation;
	}


	/**
	 * @return the timeOfDepth
	 */
	public long getTimeOfDepth() {
		return timeOfDepth;
	}


	/**
	 * @param timeOfDepth the timeOfDepth to set
	 */
	public void setTimeOfDepth(long timeOfDepth) {
		this.timeOfDepth = timeOfDepth;
	}


	/**
	 * @return the timeOfTemperature
	 */
	public long getTimeOfTemperature() {
		return timeOfTemperature;
	}


	/**
	 * @param timeOfTemperature the timeOfTemperature to set
	 */
	public void setTimeOfTemperature(long timeOfTemperature) {
		this.timeOfTemperature = timeOfTemperature;
	}


	/**
	 * @return the vessellName
	 */
	public String getVessellName() {
		return vesselName;
	}


	/**
	 * @param vessellName the vessellName to set
	 */
	public void setVessellName(String vessellName) {
		this.vesselName = vessellName;
	}


	/**
	 * @return the extra
	 */
	public String getExtra() {
		return extra;
	}


	/**
	 * @param extra the extra to set
	 */
	public void setExtra(String extra) {
		this.extra = extra;
	}


	/**
	 * @return the mostRecentFirst
	 */
	public boolean isMostRecentFirst() {
		return mostRecentFirst;
	}


	/**
	 * @param mostRecentFirst the mostRecentFirst to set
	 */
	public void setMostRecentFirst(boolean mostRecentFirst) {
		this.mostRecentFirst = mostRecentFirst;
	}


} // End of classy class.
