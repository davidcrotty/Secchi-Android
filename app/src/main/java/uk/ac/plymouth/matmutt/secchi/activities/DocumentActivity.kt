package uk.ac.plymouth.matmutt.secchi.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.RelativeLayout
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_document.*
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.domain.DocumentationPage
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import uk.ac.plymouth.matmutt.secchi.view.PushDialogListener
import uk.ac.plymouth.matmutt.secchi.view.components.DocumentPager
import uk.ac.plymouth.matmutt.secchi.view.components.VerticalPageTransformer
import uk.ac.plymouth.matmutt.secchi.view.components.VerticalViewPager

/**
 * Created by David Crotty on 18/06/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class DocumentActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    companion object {
        val PAGE_KEY: String = "PAGE_KEY"
    }

    private var databaseDisposable: Disposable? = null
    private lateinit var pager: ViewPager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document)
        supportActionBar?.title = ""
        supportActionBar?.setHomeAsUpIndicator(R.drawable.home_icon)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //TODO this can be dynamic based on API level
        pager = VerticalViewPager(this, VerticalPageTransformer())
        pager.id = R.id.pager_view
        root.addView(pager, 0, getLayoutParams())

        val dataContext = RealmContext(Realm.getDefaultInstance())

        val repository = UnitOfWork(dataContext)
        val pages = repository.getRealmContext().findAll<DocumentationPage>().toList()
        if(pages.isEmpty()) {
            //TODO mark as pages read, ask to contact support
        } else {
            pager.adapter = DocumentPager(supportFragmentManager,
                    pages)
            pager.addOnPageChangeListener(this)
        }
        repository.complete()

        next.setOnClickListener {
            advancePager()
        }
        processPriorPageNumber()
    }

    override fun onResume() {
        super.onResume()
        PushDialogListener.register(this@DocumentActivity)
    }

    override fun onPause() {
        super.onPause()
        PushDialogListener.unregister(this@DocumentActivity)
    }

    private fun getLayoutParams() : RelativeLayout.LayoutParams {
        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        return params
    }

    private fun processPriorPageNumber() {
        when(intent.getIntExtra(PAGE_KEY, -1)) {
            DocumentationMenuActivity.WHAT_IS_DISK -> {
                markPageIndexAsReadFor(DocumentationMenuActivity.WHAT_IS_DISK)
                pager.setCurrentItem(DocumentationMenuActivity.WHAT_IS_DISK, true)
            }
            DocumentationMenuActivity.MAKING_DISK -> {
                markPageIndexAsReadFor(DocumentationMenuActivity.MAKING_DISK)
                pager.setCurrentItem(DocumentationMenuActivity.MAKING_DISK, true)
            }
            DocumentationMenuActivity.USING_DISK -> {
                markPageIndexAsReadFor(DocumentationMenuActivity.USING_DISK)
                pager.setCurrentItem(DocumentationMenuActivity.USING_DISK, true)
            }
            DocumentationMenuActivity.OPTIONAL_OBS -> {
                markPageIndexAsReadFor(DocumentationMenuActivity.OPTIONAL_OBS)
                pager.setCurrentItem(DocumentationMenuActivity.OPTIONAL_OBS, true)
            }
            else -> {
                markPageIndexAsReadFor(DocumentationMenuActivity.SECCHI_APP)
                pager.setCurrentItem(DocumentationMenuActivity.SECCHI_APP, true)
            }
        }
    }

    override fun finish() {
        super.finish()
        databaseDisposable?.dispose()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        reversePager()
    }

    override fun onPageScrollStateChanged(state: Int) {
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        if(position >= 1) {
            next.setImageResource(R.drawable.next_blue)
        } else {
            next.setImageResource(R.drawable.next_white)
        }
    }

    override fun onPageSelected(position: Int) {
       markPageIndexAsReadFor(position)
    }

    private fun markPageIndexAsReadFor(pageIndex: Int) {
        databaseDisposable = Single.just(Any()).subscribeOn(Schedulers.io()).map {
            val unitOfWork = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
            val page = unitOfWork.getRealmContext().find<DocumentationPage> { page -> page.id == pageIndex} ?: return@map
            page.read = true
            unitOfWork.getRealmContext().add(RealmTransaction(page))
            unitOfWork.complete()
        }.subscribe()
    }

    private fun advancePager() {
        val currentItem = pager.currentItem

        if(currentItem == BuildConfig.NUMBER_OF_DOC_PAGES - 1) {
            finish()
        } else {
            pager.setCurrentItem(currentItem + 1, true)
        }
    }

    private fun reversePager() {
        val currentItem = pager.currentItem
        if(currentItem == 0) {
            finish()
        } else {
            pager.setCurrentItem(currentItem - 1, true)
        }
    }
}

fun DocumentActivity.Companion.startWith(activity: AppCompatActivity,
                                         pageNumber: Int) {
    val intent = Intent(activity, DocumentActivity::class.java)
    intent.putExtra(PAGE_KEY, pageNumber)
    activity.startActivity(intent)
}