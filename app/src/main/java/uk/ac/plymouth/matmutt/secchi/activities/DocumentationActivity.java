//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.activities;
import java.util.Date;
import java.util.List;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.R.drawable;
import uk.ac.plymouth.matmutt.secchi.R.id;
import uk.ac.plymouth.matmutt.secchi.R.layout;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.location.Criteria;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.Toast;


//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
//We can see from the line below that this Activity listens for events.		//
//**************************************************************************//
public class DocumentationActivity extends Activity implements 
                                                    android.view.View.OnClickListener
{

	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private SecchiPackageVariables secchiPackageVariables = null;
	private String                 activityName           = "";

	
	//**********************************************************************//
	// UI components.														//
	//**********************************************************************//
	private TableRow               tableRowTheSecchiApp           = null;
	private TableRow               tableRowWhatIsASecchiDisk	  = null;
	private TableRow               tableRowMakingASecchiDisk	  = null;
	private TableRow               tableRowUsingASecchiDisk       = null;
	private TableRow               tableRowOptionalObservations   = null;
	private CheckBox               checkBoxTheSecchiApp           = null;
	private CheckBox               checkBoxWhatIsASecchiDisk      = null;
	private CheckBox               checkBoxMakingASecchiDisk      = null;
	private CheckBox               checkBoxUsingASecchiDisk       = null;
	private CheckBox               checkBoxOptionalObservations   = null;
	
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.documentation_activity_layout);
        
     secchiPackageVariables = SecchiPackageVariables.getInstance(this);
     activityName           = getClass().getSimpleName();

     
     //*****************************************************************//
   	 // Get the UI components.         									//
   	 //*****************************************************************//
   	 tableRowTheSecchiApp         = (TableRow) findViewById(R.id.tableRowTheSecchiApp);
   	 tableRowWhatIsASecchiDisk    = (TableRow) findViewById(R.id.tableRowWhatIsASecchiDisk);
   	 tableRowMakingASecchiDisk    = (TableRow) findViewById(R.id.tableRowMakingASecchiDisk);
   	 tableRowUsingASecchiDisk     = (TableRow) findViewById(R.id.tableRowUsingASecchiDisk);
   	 tableRowOptionalObservations = (TableRow) findViewById(R.id.tableRowOptionalObservations);
   	 checkBoxTheSecchiApp         = (CheckBox) findViewById(R.id.checkBoxTheSecchiApp);
   	 checkBoxWhatIsASecchiDisk    = (CheckBox) findViewById(R.id.checkBoxWhatIsASecchiDisk);
   	 checkBoxMakingASecchiDisk    = (CheckBox) findViewById(R.id.checkBoxMakingASecchiDisk);
   	 checkBoxUsingASecchiDisk     = (CheckBox) findViewById(R.id.checkBoxUsingASecchiDisk);
   	 checkBoxOptionalObservations = (CheckBox) findViewById(R.id.checkBoxOptionalObservations);
   	 
   	 
   	 //*****************************************************************//
   	 // Set the sizes (heights).										//
   	 //*****************************************************************//
   	 LayoutParams layoutParams = new LayoutParams();
   	 layoutParams.height = 200;
   	 
   	 //tableRowInfo.setLayoutParams(layoutParams);
 
     //*****************************************************************//
   	 // Add listeners; all this activity.								//
   	 //*****************************************************************//
   	 tableRowTheSecchiApp.setOnClickListener(this);
   	 tableRowWhatIsASecchiDisk.setOnClickListener(this);
   	 tableRowMakingASecchiDisk.setOnClickListener(this);
   	 tableRowUsingASecchiDisk.setOnClickListener(this);
   	 tableRowOptionalObservations.setOnClickListener(this);
    }

    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
	//**********************************************************************//
	@Override
	public void onPause()
   {
	   super.onPause();
	   secchiPackageVariables.release();
	   if (D.debug) Log.i(activityName, "Activity paused");
   }

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();

		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
		populateUIComponents();
		
		if (D.debug) Log.i(activityName, "Activity resumed");   		
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	// If we are to avoid flattening the battery, we must remove the status	 //
	// listeners.  It is possible that we might want to keep logging position//
	// at a reduced rate, though.											 //
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			 //	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }
   
     
     
     
     //**********************************************************************//
     // Populate the UI components.											 //
     //**********************************************************************//
     private void populateUIComponents()
     {
    	//******************************************************************//
    	// Check where appropriate.										   	//
    	//******************************************************************//
    	checkBoxTheSecchiApp.setChecked(
    			 secchiPackageVariables.isUserHasReadSecchiInfo());
    	checkBoxWhatIsASecchiDisk.setChecked(
    			 secchiPackageVariables.isUserHasReadWhatisASecchiDisk());
    	checkBoxMakingASecchiDisk.setChecked(
    			 secchiPackageVariables.isUserHasReadMakingASecchiDisk());
    	checkBoxUsingASecchiDisk.setChecked(
    			 secchiPackageVariables.isUserHasReadUsingASecchiDisk());
    	checkBoxOptionalObservations.setChecked(
    			secchiPackageVariables.isUserHasReadOptionalObservations());
     }
     

     
     //**********************************************************************//
     // Overridden method for an Activity.  Here we get the results of the 	 //
     // Activity we invoked.  The requestCode tells us what the Activity 	 //
     // was.       															 //
     //**********************************************************************//
     @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data) 
     {
    	 if (D.debug)
    		 Log.i(activityName, "Activity result received, code " + resultCode);
    	 switch (requestCode)
    	{
	    	//**************************************************************//
	 	 	// Secchi app info.												//
	 	 	//**************************************************************//
	 	 	case 1:	
	 	 		 if (resultCode == 0) 
	 	 		 {
	 	 			 if (!secchiPackageVariables.isUserHasReadSecchiInfo())
	 	 				 checkBoxTheSecchiApp.setChecked(false);
	 	 		 }
	 	 		 else
	 	 		 {
	 	 			 checkBoxTheSecchiApp.setChecked(true);
	 	 			 secchiPackageVariables.setUserHasReadSecchiInfo(true);
	 	 		 }
	 	 		 break;
	 	 		 
	 	 		 
	   	 	//**************************************************************//
	   	 	// What is a Secchi disk?										//
	   	 	//**************************************************************//
	   	 	case 2:	
	   	 		 if (resultCode == 0) 
	   	 		 {
	   	 			 if (!secchiPackageVariables.isUserHasReadWhatisASecchiDisk())
	   	 				 checkBoxWhatIsASecchiDisk.setChecked(false);
	   	 		 }
	   	 		 else
	   	 		 {
	   	 			checkBoxWhatIsASecchiDisk.setChecked(true);
	   	 			secchiPackageVariables.setUserHasReadWhatisASecchiDisk(true);
	   	 		 }
	   	 		 break;
	 
	     	 		 
   	   	 	//**************************************************************//
      	 	// Making a Secchi disk?										//
       	 	//**************************************************************//
       	 	case 3:	
       	 		 if (resultCode == 0) 
       	 		 {
    	 			 if (!secchiPackageVariables.isUserHasReadMakingASecchiDisk())
          	 				 checkBoxMakingASecchiDisk.setChecked(false);
       	 		 }
       	 		 else
       	 		 {
       	 			checkBoxMakingASecchiDisk.setChecked(true);
       	 			secchiPackageVariables.setUserHasReadMakingASecchiDisk(true);
       	 		 }
       	 		 break;

          	 		 
			//**************************************************************//
			// Using a Secchi Disk.											//
			//**************************************************************//
       	 	case 4:	
       	 		 if (resultCode == 0) 
       	 		 {
       	 			 if (!secchiPackageVariables.isUserHasReadUsingASecchiDisk())
           	 				 checkBoxUsingASecchiDisk.setChecked(false);
       	 		 }
       	 		 else
       	 		 {
       	 			checkBoxUsingASecchiDisk.setChecked(true);
       	 			secchiPackageVariables.setUserHasReadUsingASecchiDisk(true);
       	 		 }
       	 		 break;

       	 		 
  			//**************************************************************//
  			// Optional Observations.										//
   			//**************************************************************//
            case 5:	
            	 if (resultCode == 0) 
            	 {
            		 if (!secchiPackageVariables.isUserHasReadOptionalObservations())
             				 checkBoxOptionalObservations.setChecked(false);
            	 }
            	 else
            	 {
            		 checkBoxOptionalObservations.setChecked(true);
            		secchiPackageVariables.setUserHasReadOptionalObservations(true);
            	 }
            	 break;
    	 }
    	 secchiPackageVariables.saveStatus(this);
     }
     
     
     
 	//***********************************************************************//
 	// Overridden from View.OnclickListener.								 //
 	//***********************************************************************//
	@Override
	public void onClick(View view) 
	{
		Intent intent = null;
		
		//******************************************************************//
		// For every section, we assemble an array of strings of images 	//
		// that make up the section.  We invoke an activity and ask for a 	//
		// result back.  We also get the requestCode back, which is how we	//
		// know the activity that generated the result.						//
		//******************************************************************//
		int[] iDImageNames = null;
		
		switch (view.getId())
		{
			//**************************************************************//
			// The Secchi Disk App info.									//
			//**************************************************************//
			case R.id.tableRowTheSecchiApp:
				 intent = new Intent(this, DocumentSectionViewActivity.class);
				 iDImageNames    = new int[5];
				 iDImageNames[0] = R.drawable.documentation_001; 
				 iDImageNames[1] = R.drawable.documentation_002; 
				 iDImageNames[2] = R.drawable.documentation_003; 
				 iDImageNames[3] = R.drawable.documentation_004;
				 iDImageNames[4] = R.drawable.documentation_005;
				 intent.putExtra("image_names", iDImageNames);
				 startActivityForResult(intent, 1);					// Return code 1, 2, 3
				 break;
				 
				 
			//**************************************************************//
			// What is a Secchi Disk.										//
			//**************************************************************//
			case R.id.tableRowWhatIsASecchiDisk:
				 intent = new Intent(this, DocumentSectionViewActivity.class);
				 iDImageNames    = new int[3];
				 iDImageNames[0] = R.drawable.documentation_006;
				 iDImageNames[1] = R.drawable.documentation_007;
				 iDImageNames[2] = R.drawable.documentation_008;
				 intent.putExtra("image_names", iDImageNames);
				 startActivityForResult(intent, 2);
				 break;
				 
			//**************************************************************//
			// Making a Secchi Disk.										//
			//**************************************************************//
			case R.id.tableRowMakingASecchiDisk:
				 intent = new Intent(this, DocumentSectionViewActivity.class);
				 iDImageNames    = new int[2];
				 iDImageNames[0] = R.drawable.documentation_009;
				 iDImageNames[1] = R.drawable.documentation_010;
				 intent.putExtra("image_names", iDImageNames);
				 startActivityForResult(intent, 3);
				 break;
						 
			//**************************************************************//
			// Using a Secchi Disk.											//
			//**************************************************************//
			case R.id.tableRowUsingASecchiDisk:
				 intent = new Intent(this, DocumentSectionViewActivity.class);
				 iDImageNames    = new int[8];
				 iDImageNames[0] = R.drawable.documentation_011;
				 iDImageNames[1] = R.drawable.documentation_012;
				 iDImageNames[2] = R.drawable.documentation_013;
				 iDImageNames[3] = R.drawable.documentation_014;
				 iDImageNames[4] = R.drawable.documentation_015;
				 iDImageNames[5] = R.drawable.documentation_016;
				 iDImageNames[6] = R.drawable.documentation_017;
				 iDImageNames[7] = R.drawable.documentation_018;
				 intent.putExtra("image_names", iDImageNames);
				 startActivityForResult(intent, 4);
				 break;
										 
					 
			//**************************************************************//
			// Optional Observations.										//
			//**************************************************************//
			case R.id.tableRowOptionalObservations:
				 intent = new Intent(this, DocumentSectionViewActivity.class);
				 iDImageNames    = new int[5];
				 iDImageNames[0] = R.drawable.documentation_019;
				 iDImageNames[1] = R.drawable.documentation_020;
				 iDImageNames[2] = R.drawable.documentation_021;
				 iDImageNames[3] = R.drawable.documentation_022;
				 iDImageNames[4] = R.drawable.documentation_023;
				 intent.putExtra("image_names", iDImageNames);
				 startActivityForResult(intent, 5);
				 break;
		}
	}

 
 
     
}	// End of classy class.
