package uk.ac.plymouth.matmutt.secchi.network

import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import timber.log.Timber
import uk.ac.plymouth.matmutt.secchi.App
import uk.ac.plymouth.matmutt.secchi.network.model.PushModel
import uk.ac.plymouth.matmutt.secchi.view.PushDialogListener


/**
 * Created by David Crotty on 10/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class PushNotificationService : FirebaseMessagingService() {

    companion object {
        val PUSH_MODEL_KEY = "PUSH_MODEL_KEY"
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        /* no-op, we are using firebase console Notifications, use case currently does
           not use 'data' messages (only accessible as of writing from firebase web API) */
        Timber.d("Message received")
        //sanitize
        val map = message?.data ?: return
        val app = application as App
        val elements = app.gson.toJsonTree(map)
        val pushModel = app.gson.fromJson(elements, PushModel::class.java)
        if(pushModel.isValid() == false) return
        val intent = Intent(PushDialogListener.SHOW_DIALOG_ACTION)
        intent.putExtra(PUSH_MODEL_KEY, pushModel)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }
}