package uk.ac.plymouth.matmutt.secchi.presenter

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.LocationManager
import android.net.Uri
import android.os.Environment
import android.os.Handler
import android.os.HandlerThread
import io.reactivex.Emitter
import io.reactivex.Observable
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.internal.operators.observable.ObservableJust
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.realm.Realm
import org.joda.time.DateTime
import org.joda.time.Duration
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.activities.MainMenuActivity
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState
import uk.ac.plymouth.matmutt.secchi.domain.SecchiReading
import uk.ac.plymouth.matmutt.secchi.domain.User
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import uk.ac.plymouth.matmutt.secchi.service.GPSService
import uk.ac.plymouth.matmutt.secchi.store.ApplicationStateStore
import uk.ac.plymouth.matmutt.secchi.view.DialogProvider
import uk.ac.plymouth.matmutt.secchi.view.IMainMenuView
import java.io.File
import java.io.InvalidObjectException
import java.util.*

/**
 * Created by David Crotty on 17/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class MainMenuActivityPresenter(private val view: IMainMenuView,
                                private val store: ApplicationStateStore,
                                private val unitOfWork: UnitOfWork) {

    private val TAG = "MainMenuPresenter"
    var dataStoreSubscription: Disposable? = null
    private var handlerThread: HandlerThread? = null
    private var uiUpdateHandler: Handler? = null
    private var currentImagePath: Uri? = null
    private val COMPRESSION_PERCENTAGE = 80
    private val SCALED_WIDTH = 640
    private val SCALED_HEIGHT = 480
    private var gpsUpdateDisposable: Disposable? = null

    fun observeGPSChanges(gpsSubject: BehaviorSubject<Intent>) {
        if(gpsUpdateDisposable?.isDisposed == true || gpsUpdateDisposable == null) {
            gpsUpdateDisposable = gpsSubject.subscribe { intent ->
                if(intent.action == GPSService.GPS_BROADCAST) {
                    view.hideGPSSpinner()
                    checkCoordinatesFromDB()
                }
            }
        }
    }

    fun fetchPreferedBoatName() : Observable<String> {
        return Observable.create(ObservableOnSubscribe<String> { e ->
            val user = Realm.getDefaultInstance().where(User::class.java).findFirst()
            if(user == null) {
                e.onNext(BuildConfig.BOAT_NAME_DEFAULT)
                e.onComplete()
            } else {
                e.onNext(user.boatNameDefault)
                Realm.getDefaultInstance().close()
                e.onComplete()
            }
        })
        .subscribeOn(AndroidSchedulers.mainThread())
    }

    fun updateUIFromPreviousSession() {
        view.blockSubmission()

        var currentState = unitOfWork.getRealmContext().getApplicationState()
        if(currentState == null) {
            currentState = ApplicationState()
            val transaction = RealmTransaction(currentState)
            unitOfWork.getRealmContext().add(transaction)
            unitOfWork.complete()
        }

        if(currentState.inProgressReading == null) {
            currentState.inProgressReading = SecchiReading()
            val transaction = RealmTransaction(currentState)
            unitOfWork.getRealmContext().add(transaction)
            unitOfWork.complete()
        }

        store.updateApplicationStateWith(currentState)
    }

    fun newReading() {
        val currentReading = unitOfWork.getRealmContext().getApplicationState()?.inProgressReading ?: return
        unitOfWork.getRealmContext().add(RealmTransaction(currentReading))
        unitOfWork.complete()

        val state = unitOfWork.getRealmContext().getApplicationState() ?: return
        state.inProgressReading = SecchiReading()
        unitOfWork.getRealmContext().add(RealmTransaction(state))
        unitOfWork.complete()
        store.updateApplicationStateWith(state)
    }

    fun generateUriForImage() : File {
        currentImagePath = null
        val fileName = UUID.randomUUID().toString()
        val storageDIR = view.getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val file = File.createTempFile(fileName,
                ".jpg",
                storageDIR)
        val path = Uri.parse(file.absolutePath)
        currentImagePath = path
        return file
    }

    fun onActivityPause() {
        gpsUpdateDisposable?.dispose()
        dataStoreSubscription?.dispose()
        shutdownHandlerThread()
    }

    private fun shutdownHandlerThread() {
        handlerThread?.quit()
        uiUpdateHandler?.removeCallbacksAndMessages(null)
    }

    fun processCoordinatesFor(reading: SecchiReading?) {
        if(reading?.lattiutde == null || reading.longitude == null || reading.timeOfLocation == null) {
            view.getActivity().runOnUiThread {
                view.updateCoordinatesWith(view.getActivity().getString(R.string.blank_reading),
                        view.getActivity().getString(R.string.blank_reading),
                        view.getActivity().getString(R.string.not_acquired_gps))
                view.disableDepth()
                view.disableOptionalAndSubmission() //TODO DRY
            }
        } else {
            val expiryLimit = DateTime(reading.timeOfLocation?.plus(BuildConfig.GPS_EXPIRY_TIME_MS))
            val remainingDuration = Duration(System.currentTimeMillis(), expiryLimit.millis)
            var remainingMinutes = remainingDuration.standardMinutes
            if(remainingMinutes <= 0) {
                remainingMinutes = 0
                reading.depth = null
                val unitOfWork = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
                val state = unitOfWork.getRealmContext().getApplicationState()!!
                state.inProgressReading = reading
                unitOfWork.getRealmContext().add(RealmTransaction(state))
                unitOfWork.complete()
                view.getActivity().runOnUiThread {
                    view.updateCoordinatesWith("%.2f".format(reading.lattiutde),
                            "%.2f".format(reading.longitude),
                            "0 minutes remaining")
                    view.clearDepth()
                    view.disableDepth()
                    view.disableOptionalAndSubmission() //TODO DRY
                }
            } else {
                //how do we know coords are 'new' ? Look at time acquired, if != then hide spinner
                view.getActivity().runOnUiThread {
                    view.updateCoordinatesWith("%.2f".format(reading.lattiutde),
                            "%.2f".format(reading.longitude),
                            "$remainingMinutes  ${view.getActivity().getString(R.string.gps_minutes_remaining)}")
                    view.enableDepth()
                    processDepthEnabledFor(reading.depth)
                }
            }
        }
    }

    private fun processDepthEnabledFor(depth: Float?) {
        if(depth != null) {
            view.enableOptionalAndSubmission()
        }
    }

    fun checkCoordinatesFromDB() {
        val state = UnitOfWork(RealmContext(Realm.getDefaultInstance())).getRealmContext().getApplicationState() ?: return
        processCoordinatesFor(state.inProgressReading)
    }

    fun periodicUIUpdate() {
        dataStoreSubscription = store.stateSubject?.subscribeOn(Schedulers.newThread())
                ?.subscribe { state ->
                    val depth = state?.inProgressReading?.depth
                    if(depth == null) {
                        view.getActivity().runOnUiThread {
                            view.clearDepth()
                            view.disableOptionalAndSubmission()
                        }
                    } else {
                        view.getActivity().runOnUiThread {
                            view.updateDepthWith(depth)
                            view.allowSubmission()
                        }
                    }

                    checkCoordinatesFromDB()

                    val temperature = state?.inProgressReading?.temperature
                    if(temperature == null) {
                        view.getActivity().runOnUiThread {
                            view.clearTemperature()
                        }
                    } else {
                        view.getActivity().runOnUiThread {
                            view.updateTemperatureWith(temperature)
                        }
                    }

                    val boatName = state?.inProgressReading?.vesselName
                    if(boatName == null) {
                        view.getActivity().runOnUiThread {
                            view.resetBoatName()
                        }
                    } else {
                        view.getActivity().runOnUiThread {
                            view.updateBoatNameWith(boatName)
                        }
                    }

                    val photoPath = state?.inProgressReading?.photoPath
                    if(photoPath.isNullOrBlank()) {
                        view.getActivity().runOnUiThread {
                            view.showPhotoIncomplete()
                        }
                    } else {
                        view.getActivity().runOnUiThread {
                            view.showPhotoComplete()
                        }
                    }

                    enableReadingComponentsWith(state)
                }

        shutdownHandlerThread()
        handlerThread = HandlerThread("Secchi:GPSUpdate")
        handlerThread?.start()
        handlerThread?.let { thread ->
            uiUpdateHandler = Handler(thread.looper)
            updateStore()
        }
    }

    fun onActivityResume() {
        periodicUIUpdate()
    }

    fun checkIfCanStartSearchingGPSWith(manager: LocationManager?) {
        if(BuildConfig.FLAVOR.equals(BuildConfig.DEV_FLAVOUR)) {
            view.beginGPSSearch(mockReading = true)
            return
        }

        manager ?: return

        //Don't dispose on pause, will need across lifecycle
        ObservableJust(Any()).map {
            if(manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                LocationManager.GPS_PROVIDER
            } else if(manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                LocationManager.NETWORK_PROVIDER
            } else {
                throw InvalidObjectException("No location provider")
            }
        }.onErrorResumeNext { ex: Throwable ->
            return@onErrorResumeNext Observable.empty<String>()
        }.map { providerType ->
            val location = manager.getLastKnownLocation(providerType)
            if(location == null) {
                return@map true
            } else {
                return@map System.currentTimeMillis() >= location.time + BuildConfig.LAST_LOCATION_READING_INTERVAL_MS
            }
        }.subscribe({ shouldShowSatelliteDialog ->
            if(shouldShowSatelliteDialog) {
                view.showSatelliteDialog()
            } else {
                if(view is DialogProvider.SateliteDialogListener) {
                    view.initiateGPSSearch()
                }
            }

        })
    }

    fun updateDepthWith(depth: Float, timeOfDepthRecording: Long) {
        val currentState = unitOfWork.getRealmContext().getApplicationState() ?: return
        currentState.inProgressReading?.depth = depth
        currentState.inProgressReading?.timeOfDepth = timeOfDepthRecording
        val transaction = RealmTransaction(currentState)
        unitOfWork.getRealmContext().add(transaction)
        unitOfWork.complete()
        store.updateApplicationStateWith(currentState)
    }

    fun updateTemperatureWith(temperature: Float, timeOfTemperatureRecording: Long) {
        val currentState = unitOfWork.getRealmContext().getApplicationState() ?: return
        currentState.inProgressReading?.temperature = temperature
        currentState.inProgressReading?.timeOfTemperature = timeOfTemperatureRecording
        unitOfWork.getRealmContext().add(RealmTransaction(currentState))
        unitOfWork.complete()
        store.updateApplicationStateWith(currentState)
    }

    fun processImage(path: String) : String {
        val bitmap = scaleBitmapFrom(path)

        val updatedImage = File(path)
        if(updatedImage.exists()) {
            updatedImage.delete()
        }
        updatedImage.createNewFile()

        bitmap.compress(Bitmap.CompressFormat.JPEG,
                COMPRESSION_PERCENTAGE,
                updatedImage.outputStream())

        return updatedImage.absolutePath
    }

    private fun scaleBitmapFrom(path: String) : Bitmap {
        val bitmap = BitmapFactory.decodeFile(path)
        val aspectRatio = bitmap.width / bitmap.height
        val pair = if(aspectRatio > 1) {
            Pair(SCALED_WIDTH, SCALED_HEIGHT)
        } else {
            Pair(SCALED_HEIGHT, SCALED_WIDTH)
        }
        return Bitmap.createScaledBitmap(bitmap,
                pair.first,
                pair.second,
                false)
    }

    fun updatePhotoPath(didTake: Boolean) {
        if(didTake) {
            currentImagePath?.let { path ->
                val downscaledImage = processImage(path.toString())

                val currentState = unitOfWork.getRealmContext().getApplicationState() ?: return
                currentState.inProgressReading?.photoPath = downscaledImage
                unitOfWork.getRealmContext().add(RealmTransaction(currentState))
                unitOfWork.complete()
                store.updateApplicationStateWith(currentState)
            }
        }
        currentImagePath = null
    }

    fun updateNotesWith(notes: String) {
        val currentState = unitOfWork.getRealmContext().getApplicationState() ?: return
        currentState.inProgressReading?.notes = notes
        unitOfWork.getRealmContext().add(RealmTransaction(currentState))
        unitOfWork.complete()
        store.updateApplicationStateWith(currentState)
    }

    fun updateBoatNameWith(name : String) {
        updateUserProfileWith(name)
        val currentState = unitOfWork.getRealmContext().getApplicationState() ?: return
        currentState.inProgressReading?.vesselName = name
        unitOfWork.getRealmContext().add(RealmTransaction(currentState))
        unitOfWork.complete()
        store.updateApplicationStateWith(currentState)
    }

    fun enableReadingComponentsWith(state: ApplicationState) {
        if(state.hasReadAllDocumentationPages()) {
            view.getActivity().runOnUiThread {
                view.enableGPS()
            }
        }
    }

    private fun updateUserProfileWith(name: String) {
        val user = Realm.getDefaultInstance().where(User::class.java).findFirst()
        Realm.getDefaultInstance().beginTransaction()
        user?.boatNameDefault = name
        Realm.getDefaultInstance().commitTransaction()
        Realm.getDefaultInstance().close()
    }

    private fun updateStore() {
        uiUpdateHandler?.postDelayed({
            val unitOfWork = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
            val state = unitOfWork.getRealmContext().getApplicationState()
            if(state != null) {
                store.updateApplicationStateWith(state)
            }
            updateStore()
        }, BuildConfig.UI_CHECK_INTERVAL)
    }
}
