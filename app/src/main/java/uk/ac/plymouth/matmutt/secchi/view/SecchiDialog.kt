package uk.ac.plymouth.matmutt.secchi.view

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatDialogFragment

/**
 * Created by David Crotty on 17/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class SecchiDialog : AppCompatDialogFragment() {

    companion object {
        val TAG = "SecchiDialog"
        val TITLE = "title"
        val DESCRIPTION = "description"

        fun newInstance(title: String, description: String) : SecchiDialog {
            val dialog = SecchiDialog()
            val bundle = Bundle()
            bundle.putString(TITLE, title)
            bundle.putString(DESCRIPTION, description)
            dialog.arguments = bundle
            return dialog
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return AlertDialog.Builder(activity)
                .setTitle(arguments.getString(TITLE))
                .setMessage(arguments.getString(DESCRIPTION))
                .setPositiveButton("Confirm", { dialog, which ->
                    dismiss()
                })
                .create()
    }
}