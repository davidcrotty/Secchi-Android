package uk.ac.plymouth.matmutt.secchi.view

import uk.ac.plymouth.matmutt.secchi.view.model.Experiment

/**
 * Created by David Crotty on 24/10/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
interface IUploadView {
    fun renderRoamingPrompt(experiment: Experiment, isEditable: Boolean)
}