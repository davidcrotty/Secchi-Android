package uk.ac.plymouth.matmutt.secchi.view

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.support.design.widget.BottomSheetDialogFragment
import android.view.View
import android.widget.Button
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.R
import uk.ac.plymouth.matmutt.secchi.activities.WhoWeAreActivity

/**
* Created by David Crotty on 19/03/2017.
*
* Copyright © 2017 David Crotty - All Rights Reserved
*/
class InformationFragment : BottomSheetDialogFragment() {

    val MAIL_MIME_TYPE = "message/rfc822"

    override fun setupDialog(dialog: Dialog?, style: Int) {
        super.setupDialog(dialog, style)
        val view = View.inflate(activity, R.layout.information_view, null)
        dialog?.setContentView(view)

        val webSiteButton = view.findViewById(R.id.website_button) as Button
        webSiteButton.setOnClickListener {
            dismiss()
            launchWebSiteIntentWith(BuildConfig.PROJECT_WEBSITE_URL)
        }

        val charityWebSiteButton = view.findViewById(R.id.charity_website_button) as Button
        charityWebSiteButton.setOnClickListener {
            dismiss()
            launchWebSiteIntentWith(BuildConfig.CHARITY_WEBSITE_URL)
        }

        val whoWeAreButton = view.findViewById(R.id.who_we_are_button) as Button
        whoWeAreButton.setOnClickListener {
            dismiss()
            activity.startActivity(Intent(activity, WhoWeAreActivity::class.java))
        }

        val emailButton = view.findViewById(R.id.email_button) as Button
        emailButton.setOnClickListener {
            dismiss()
            launchEmailIntent()
        }

        val facebookButton = view.findViewById(R.id.facebook_button) as Button
        facebookButton.setOnClickListener {
            dismiss()
            launchFacebookIntent()
        }
    }

    private fun launchFacebookIntent() {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(BuildConfig.PROJECT_FACEBOOK_URL)
        startActivity(intent)
    }

    private fun launchWebSiteIntentWith(url: String) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse(url)
        startActivity(intent)
    }

    private fun launchEmailIntent() {
        val emailIntent = Intent(Intent.ACTION_SEND)
        emailIntent.type = MAIL_MIME_TYPE
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(BuildConfig.PROJECT_EMAIL_ADDRESS))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
        emailIntent.putExtra(Intent.EXTRA_TEXT, "")
        startActivity(Intent.createChooser(emailIntent, resources.getString(R.string.shareByEmail)))
    }
}