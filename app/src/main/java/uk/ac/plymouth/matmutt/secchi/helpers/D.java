//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// A very simple status class, currently initially for the debugging state,	//
// but now used for such things as whether this is the free version.		//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.helpers;

public class D 
{
	public static final boolean debug               = false;
	public static final boolean useFakeGPS          = false;
	public static final boolean useSDCardFolder     = false;
	public static final boolean photosOnSDCard      = false;
	public static final boolean allowExportToSDCard = true;
}	// End of classy class.
