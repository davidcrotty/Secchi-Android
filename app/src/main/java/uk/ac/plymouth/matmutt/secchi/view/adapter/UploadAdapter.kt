package uk.ac.plymouth.matmutt.secchi.view.adapter

import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.view.LayoutInflater
import android.view.ViewGroup
import uk.ac.plymouth.matmutt.secchi.activities.UploadActivity
import uk.ac.plymouth.matmutt.secchi.databinding.ExperimentViewBinding
import uk.ac.plymouth.matmutt.secchi.databinding.HeadingViewBinding
import uk.ac.plymouth.matmutt.secchi.presenter.UploadActivityPresenter
import uk.ac.plymouth.matmutt.secchi.view.model.Experiment
import uk.ac.plymouth.matmutt.secchi.view.model.Heading
import uk.ac.plymouth.matmutt.secchi.view.model.UploadItem


/**
 * Created by David Crotty on 20/08/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class UploadAdapter(val data: ArrayList<IAdaptable>,
                    val activity: UploadActivity,
                    val presenter: UploadActivityPresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        val EXPERIMENT = 0
        val HEADING = 1
    }

    var canEdit: Boolean = false

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        when(holder) {
            is HeadingViewHolder -> {
                val uploadItem = data[position] as UploadItem
                val heading = uploadItem.heading ?: return
                holder.bind(heading)
            }
            is ExperimentViewHolder -> {
                val uploadItem = data[position] as UploadItem
                val experiment = uploadItem.experiment ?: return
                holder.bind(experiment)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        when(viewType) {
            HEADING -> {
                return HeadingViewHolder(HeadingViewBinding.inflate(layoutInflater, parent, false))
            }
            else -> {
                return ExperimentViewHolder(ExperimentViewBinding.inflate(layoutInflater, parent, false))
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return data[position].getViewType()
    }

    fun getPositionFor(uuid: String) : Int {
        for(i in 0..data.size - 1) {
            val item = data[i]
            if(item is UploadItem) {
                if(item.experiment?.id == uuid) {
                    return i
                }
            }
        }

        return -1
    }

    fun remove(position: Int) {
        data.removeAt(position)
    }

    fun toggleEdit() {
        canEdit = !canEdit
        notifyDataSetChanged()
    }

    inner class HeadingViewHolder(private val binding: HeadingViewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(heading: Heading) {
            binding.heading = heading
            binding.executePendingBindings()
        }
    }

    inner class ExperimentViewHolder(private val binding: ExperimentViewBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(experiment: Experiment) {
            binding.experiment = experiment
            binding.activity = activity
            binding.isEditable = canEdit
            binding.presenter = presenter
            binding.executePendingBindings()
        }
    }
}