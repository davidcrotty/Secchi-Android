package uk.ac.plymouth.matmutt.secchi.domain

/**
 * Created by David Crotty on 18/06/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 *
 * Category of documentation
 */
enum class Category {
    THE_SECCHI_APP,
    WHAT_IS_DISK,
    MAKING_A_DISK,
    USING_A_DISK,
    OPTIONAL_OBSERVATIONS
}