package uk.ac.plymouth.matmutt.secchi.activities

import android.Manifest
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.xmlpull.v1.XmlPullParserFactory
import uk.ac.plymouth.matmutt.secchi.BuildConfig
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiReadingX
import uk.ac.plymouth.matmutt.secchi.service.LegacyXmlParserService
import java.io.File
import java.io.IOException
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import io.realm.Realm
import io.realm.RealmList
import net.hockeyapp.android.CrashManager
import net.hockeyapp.android.CrashManagerListener
import net.hockeyapp.android.UpdateManager
import net.hockeyapp.android.UpdateManagerListener
import net.hockeyapp.android.metrics.MetricsManager
import uk.ac.plymouth.matmutt.secchi.domain.*
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmTransaction
import java.io.InputStream
import java.lang.ref.WeakReference
import java.util.*


class SplashActivity : AppCompatActivity() {

    private val INSTALLER_ADB = "adb"
    private var parserService: LegacyXmlParserService? = null
    private val WRITE_PERMISSION_REQUEST_CODE = 1
    private val FIREBASE_KEY = "url"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(playServicesCheck() == false) return
        if(BuildConfig.DEBUG == false) {
            MetricsManager.register(application, BuildConfig.HOCKEYAPP_APP_ID)
        }
        if(pushNotificationCheck()) return
        configureHockeySDK()
        //comment line below for test adding prior data
//        addLegacyXML()
        //comment line below for dumping database
//        dumpRealmDatabase()
    }

    /**
     * Use in both onresume and oncreate as these are both possible entry points
     * from the user tapping a notification.
     */
    private fun pushNotificationCheck() : Boolean {
        var didResolve = false
        if(intent.hasExtra(FIREBASE_KEY)) {
            val value = intent.getStringExtra(FIREBASE_KEY)
            if(value.isNullOrEmpty() == false) {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(value)
                startActivity(intent)
                didResolve = true
                finish()
            }
        }

        return didResolve
    }

    override fun onResume() {
        super.onResume()
        if(pushNotificationCheck()) return
        playServicesCheck()
    }

    private fun playServicesCheck() : Boolean {
        val playServiceAPI = GoogleApiAvailability.getInstance()
        val availabilityCode = playServiceAPI.isGooglePlayServicesAvailable(this)
        if(availabilityCode == ConnectionResult.SUCCESS) {
            return true
        } else {
            if(playServiceAPI.isUserResolvableError(availabilityCode)) {
                playServiceAPI.getErrorDialog(this, availabilityCode,
                        0).show()
            } else {
                Toast.makeText(this, "Play services is required to run this application", Toast.LENGTH_SHORT).show()
            }
            return false
        }
    }

    private fun configureHockeySDK() {
        if(installedFromMarket(WeakReference(this))) {
            processCrashesThenStartMigration()
        } else {
            UpdateManager.register(this, BuildConfig.HOCKEYAPP_APP_ID, object : UpdateManagerListener() {
                override fun onCancel() {
                    processCrashesThenStartMigration()
                }

                override fun onNoUpdateAvailable() {
                    processCrashesThenStartMigration()
                }
            })
        }
    }

    private fun installedFromMarket(weakContext: WeakReference<out Context>): Boolean {
        var result = false

        val context = weakContext.get()
        if (context != null) {
            try {
                val installer = context.packageManager.getInstallerPackageName(context.packageName)
                result = !TextUtils.isEmpty(installer) || installer != null && !TextUtils.equals(installer, INSTALLER_ADB)
            } catch (e: Throwable) {
            }

        }

        return result
    }

    private fun processCrashesThenStartMigration() {
        if(BuildConfig.DEBUG == false) {
            CrashManager.register(applicationContext, BuildConfig.HOCKEYAPP_APP_ID, object : CrashManagerListener() {
                override fun shouldAutoUploadCrashes(): Boolean {
                    return true
                }
            })
        }
        migrateLegacyDataToRealm()
    }

    override fun onStop() {
        super.onStop()
        UpdateManager.unregister()
    }

    private fun dumpRealmDatabase() {
        val writePermissionCheck = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        if(writePermissionCheck == PackageManager.PERMISSION_GRANTED) {
            //TODO insert new realm repo dump method here, see: https://gitlab.com/davidcrotty/Secchi-Android/issues/44
        } else {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        WRITE_PERMISSION_REQUEST_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            WRITE_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty()
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dumpRealmDatabase()
                }
            }
        }
    }

    /**
     * For testing purposes only. Adds old XML
     */
    private fun addLegacyXML() {
        val packageVariables = SecchiPackageVariables.getInstance(this)
        val submittedReading = SecchiReadingX("name",
                0f, 0f, 0L, 0f, 0L, 0f, 0L, "foo", "bar", "baz")

        packageVariables.addSecchiReadingtoSuccessfulSubmissions(submittedReading)
        packageVariables.addSecchiReadingtoSuccessfulSubmissions(submittedReading)
        packageVariables.addSecchiReadingtoSuccessfulSubmissions(submittedReading)

       packageVariables.saveSecchiReadings(this)
    }

    private fun migrateLegacyDataToRealm() {
        //TODO make this observable so IO operation does not lock UI thread
        if(applicationStateDoesNotExist() || legacyFilesExist()) {
            val applicationState = createApplicationState()
            createUserFor(applicationState)
            startMigrationUsing(applicationState)
        } else {
            checkIfUserOnboarded()
        }
    }

    private fun createApplicationState() : ApplicationState {
        val state = ApplicationState()
        state.documentationStateList = generateDocumentation()
        return state
    }

    private fun generateDocumentation() : RealmList<DocumentationPage> {
        val documentList = RealmList<DocumentationPage>()
        /** Magic strings, ew
         *  Looks like the only option unless significant time is invested refactoring how the
         *  documentation is laid out via XML but will involve too much time refactoring the legacy
         *  code that the time quoted allows.
         */
        val templateUnderTen = "documentation_00"
        val templateAboveTen = "documentation_0"

        for(i in 0..BuildConfig.NUMBER_OF_DOC_PAGES) {
            val document = DocumentationPage()
            document.id = i
            document.read = false
            if(i in 0..4) {
                document.category = Category.THE_SECCHI_APP.name
            } else if(i in 5..7) {
                document.category = Category.WHAT_IS_DISK.name
            } else if(i in 8..9) {
                document.category = Category.MAKING_A_DISK.name
            } else if(i in 10..17) {
                document.category = Category.USING_A_DISK.name
            } else if(i in 18..22) {
                document.category = Category.OPTIONAL_OBSERVATIONS.name
            } else {
                continue
            }

            document.resourceID = if((i + 1) <= 9) {
                resources.getIdentifier("$templateUnderTen${i + 1}", "drawable", packageName)
            } else {
                resources.getIdentifier("$templateAboveTen${i + 1}", "drawable", packageName)
            }
            documentList.add(document)
        }

        return documentList
    }

    private fun legacyFilesExist() : Boolean {
        val pendingFile = getFileStreamPath(BuildConfig.PENDING_UPLOADS_FILENAME)
        val submittedFile = getFileStreamPath(BuildConfig.SUBMITTED_UPLOADS_FILENAME)
        val statusFile = getFileStreamPath(BuildConfig.STATUS_FILENAME)

        return pendingFile.exists() || submittedFile.exists() || statusFile.exists()
    }

    private fun getInputStreamFromPath(path: String) : InputStream {
        return openFileInput(path)
    }

    private fun createUserFor(applicationState: ApplicationState) {
        val user = User().apply {
            primaryKey = UUID.randomUUID().toString()
        }
        val work = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        applicationState.user = user
        work.getRealmContext().add(RealmTransaction(applicationState))
        work.complete()
    }

    private fun startMigrationUsing(applicationState: ApplicationState) {
        parserService = LegacyXmlParserService(XmlPullParserFactory.newInstance().newPullParser())
        var submittedDataModels: List<SecchiReading>? = null
        var pendingDataModels: List<SecchiReading>? = null
        var updatedState: ApplicationState? = null
        /* Bespoke user management as we are tied to unis auth ids which
         rely on the app being online to retrieve, create our own UUID and store. */

        try {
            submittedDataModels = parserService?.retrieveDataModelsWith(getInputStreamFromPath(BuildConfig.SUBMITTED_UPLOADS_FILENAME))
            submittedDataModels?.map { item ->
                item.uploadStatus = UploadStatus.UPLOADED
            }
        } catch (ex: Exception) {
            //Data is corrupt or not found, not much we can do
        }

        try {
            pendingDataModels = parserService?.retrieveDataModelsWith(getInputStreamFromPath(BuildConfig.PENDING_UPLOADS_FILENAME))
        } catch (ex: IOException) {
            //Data is corrupt or not found, not much we can do
        }

        try {
            updatedState = parserService?.retrieveCurrentApplicationStateWith(getInputStreamFromPath(BuildConfig.STATUS_FILENAME),
                    applicationState)
        } catch (ex: IOException) {
            //Data is corrupt or not found, not much we can do
            ex.printStackTrace()
            updatedState = applicationState
        }

        updateDatabaseWith(submittedDataModels,
                pendingDataModels,
                updatedState,
                {
                    checkIfUserOnboarded()
                })
    }

    private fun updateDatabaseWith(submittedDataModels: List<SecchiReading>?,
                                   pendingDataModels: List<SecchiReading>?,
                                   applicationState: ApplicationState?,
                                   completedCommitHandler: () -> Unit) {

        submittedDataModels?.let { submitted ->
            val repository = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
            val transaction = RealmTransaction(submitted)
            repository.getRealmContext().add(transaction)
            repository.complete()
        }

        if(pendingDataModels != null) {
            val repository = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
            val transaction = RealmTransaction(pendingDataModels)
            repository.getRealmContext().add(transaction)
            repository.complete()
        }

        applicationState?.let {
            val repository = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
            val transaction = RealmTransaction(applicationState)
            repository.getRealmContext().add(transaction)
            repository.complete()
        }


        renameLegacyFiles()

        completedCommitHandler.invoke()
    }

    private fun renameLegacyFiles() {
        val pendingUploads = getFileStreamPath(BuildConfig.PENDING_UPLOADS_FILENAME)
        if(pendingUploads.exists()) {
            pendingUploads.renameTo(File("${BuildConfig.PENDING_UPLOADS_FILENAME}.old"))
            pendingUploads.delete()
        }

        val submittedUploads = getFileStreamPath(BuildConfig.SUBMITTED_UPLOADS_FILENAME)
        if(submittedUploads.exists()) {
            submittedUploads.renameTo(File("${BuildConfig.SUBMITTED_UPLOADS_FILENAME}.old"))
            submittedUploads.delete()
        }

        val status = getFileStreamPath(BuildConfig.STATUS_FILENAME)
        if(status.exists()) {
            status.renameTo(File("${BuildConfig.STATUS_FILENAME}.old"))
            status.delete()
        }
    }

    private fun checkIfUserOnboarded() {
        val user = Realm.getDefaultInstance().where(User::class.java).findFirst() ?: return

        if(user.readWhatsNew) {
            startActivity(Intent(this, MainMenuActivity::class.java))
        } else {
            startActivity(Intent(this, OnBoardingActivity::class.java))
        }

        Realm.getDefaultInstance().close()
    }

    private fun applicationStateDoesNotExist() : Boolean {
        val unitOfWork = UnitOfWork(RealmContext(Realm.getDefaultInstance()))
        try {
            val state = unitOfWork.getRealmContext().getApplicationState()
            return state == null
        } finally {
            unitOfWork.complete()
        }
    }
}
