package uk.ac.plymouth.matmutt.secchi.domain

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by David Crotty on 18/06/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 *
 * Use pattern described in https://github.com/realm/realm-java/issues/776#issuecomment-190147079
 * for enum support
 */
open class DocumentationPage() : RealmObject() {

    @PrimaryKey
    var id: Int? = null
    var resourceID: Int? = null
    var read: Boolean = false
    var category: String? = null

    constructor(id: Int,
                resourceID: Int,
                read: Boolean,
                category: String) : this() {
        this.id = id
        this.resourceID = resourceID
        this.read = read
        this.category = category
    }
}