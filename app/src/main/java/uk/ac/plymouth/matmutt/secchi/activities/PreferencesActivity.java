
//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Basic preferences activity.												//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2013.											//
//**************************************************************************//

package uk.ac.plymouth.matmutt.secchi.activities;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables.BearingUnits;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables.Orientation;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.widget.Toast;



//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
//We can see from the line below that this Activity listens for events.		//
//**************************************************************************//
public class PreferencesActivity extends PreferenceActivity 
                                            implements OnPreferenceChangeListener,
                                                       OnPreferenceClickListener
{
	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//

	private String                 activityName                  = "";
	private ListPreference         listPreferenceOrientation     = null;
	private ListPreference         listPreferenceBearingUnits    = null;
	private CheckBoxPreference     checkBoxPreferenceUseKeyboard = null;
	private SecchiPackageVariables secchiPackageVariables        = null;
	

	//**********************************************************************//
	//As the name suggests; called when the activity is first created. 		//
	//**********************************************************************//
	@Override
    public void onCreate(Bundle savedInstanceState) 
	{
         super.onCreate(savedInstanceState);
         activityName           = getClass().getSimpleName();

         addPreferencesFromResource(R.xml.preferences_layout);
        
		 listPreferenceOrientation    = (ListPreference)
				                           	findPreference("listPreferenceOrientation");
		 listPreferenceBearingUnits   = (ListPreference)
		 							 		findPreference("listPreferenceBearingUnitsUnits");
		 checkBoxPreferenceUseKeyboard    = (CheckBoxPreference)
				 							findPreference("checkBoxPreferenceUseKeyboardForNumericInput");
		 
		 //listPreferenceOrientation.setOnPreferenceChangeListener(this);
		 listPreferenceBearingUnits.setOnPreferenceChangeListener(this);
		 checkBoxPreferenceUseKeyboard.setOnPreferenceChangeListener(this);
	}

	

	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
	// If we are to avoid flattening the battery, we must remove the status	 //
	// listeners.  It is possible that we might want to keep logging position//
	// at a reduced rate, though.											 //
	//***********************************************************************//
	@Override
	public void onPause()
   {
	   super.onPause();
	   secchiPackageVariables.release();
	   if (D.debug) Log.i(activityName, "Activity paused");
   }

	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	// If we are to avoid flattening the battery, we must remove the status	 //
	// listeners.  It is possible that we might want to keep logging position//
	// at a reduced rate, though.											 //
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}


	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//																		//
	// Populate the UI components from the saved defaults.					// 
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		populateUIComponents();
		secchiPackageVariables.retain();
		
  	    //Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }

		if (D.debug) Log.i(activityName, "Activity resumed");   
	}
	

	
	
	
	//***********************************************************************//
	// Back button pressed.  Just quit (finish) the activity.  We don't want //
	// to leave it knocking about in memory.								 //
	//***********************************************************************//
	@Override
	public void onBackPressed() 
	{
		super.onBackPressed();
		finish();
	}
	
	




	//**********************************************************************//
	// Overridden from OnPreferenceChangeListener.							//
	//**********************************************************************//
	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) 
	{
		String  key    = preference.getKey();
		boolean result = true;
		
		
		
		if (key.equals("listPreferenceOrientation"))
		{
			String val   = (String) newValue;
			Orientation orientation = Orientation.vertical;
			if (val.equals("V")) orientation = Orientation.vertical;
			if (val.equals("H")) orientation = Orientation.horizontal;
			secchiPackageVariables.setOrientation(orientation);

			switch (secchiPackageVariables.getOrientation())
	        {
	        	case vertical:
	        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	        		 break;
	       	  
	        	case horizontal:
	        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	        		 break;
	        }

			
		}
		
	
		if (key.equals("listPreferenceBearingUnitsUnits"))
		{
			BearingUnits bu = BearingUnits.degreesMinutesDecimal;
			String val      = (String) newValue;
			if (val.equals("degreesMinutesDecimal")) bu = BearingUnits.degreesMinutesDecimal;
			if (val.equals("degreesDecimal"))        bu = BearingUnits.degreesDecimal;
			secchiPackageVariables.setBearingUnits(bu);
		}
		
		
		if (key.equals("checkBoxPreferenceUseKeyboardForNumericInput"))
		{
			Boolean val = (Boolean) newValue;
			secchiPackageVariables.setUseKeyboardForNumericInput(val.booleanValue());
		}

		
		return result;
	}


	
	
	
	//**********************************************************************//
	// Preferences click; not exactly sure why we need it.					//
	//**********************************************************************//
	@Override
	public boolean onPreferenceClick(Preference preference) 
	{
		return false;
	}
	

	
	//**********************************************************************//
	// Populate the UI components from the stored preferences.				//
	//**********************************************************************//

	private void populateUIComponents()
	{

		//******************************************************************//
		// Get the value we should use for the default.  A bit of a pain.	//
		// First do the position update time.								//
		//******************************************************************//
		int position = 0;


		//******************************************************************//
		// Orientation.                                                     //
		//******************************************************************//
		/*
		 * switch (secchiPackageVariables.getOrientation())
		
		{
			case vertical:
				 position = 0;
				 break;
			case horizontal:
				 position = 1;
				 break;
		}
		listPreferenceOrientation.setValueIndex(position);
		 */

		
		
		

		//******************************************************************//
		// Now bearing units.												//
		//******************************************************************//
		switch (secchiPackageVariables.getBearingUnits())
		{
			case degreesMinutesDecimal:
				 position = 0;
				 break;
			case degreesDecimal:
				 position = 1;
				 break;
		}
		listPreferenceBearingUnits.setValueIndex(position);
		
		
		checkBoxPreferenceUseKeyboard.setChecked(
				secchiPackageVariables.isUseKeyboardForNumericInput());

	}

	
	
	
	
	
	//*******************************************************************************//
	// Inner class to handle the contacts dialog that I make.			 			 //
	//*******************************************************************************//
	private class ContactsDialogHandler implements 
	                                    DialogInterface.OnClickListener
	{
		//**************************************************************************//
		// Instance variable.														//
		//**************************************************************************//

		 
        //**************************************************************************// 
		// Constructor.																//
		//**************************************************************************//
		public ContactsDialogHandler(Context context)
		{
		}
         
		//**************************************************************************//
		// Overriden from DialogInterface.OnClickListener.  The onclick() for the  	//
		// "really delete?" alert.													//
		//**************************************************************************//
		@Override
		public void onClick(DialogInterface dialog, int whichButton) 
		{
			switch (whichButton)
			{
				case DialogInterface.BUTTON1:	//Yes
					 Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
					 intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
					 startActivityForResult(intent, 1);	//Expect 1 back.
					 break;
			}		 
		}
	}	//End of inner class.





	
}	//End of classy class
