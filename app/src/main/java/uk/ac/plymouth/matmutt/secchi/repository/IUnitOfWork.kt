package uk.ac.plymouth.matmutt.secchi.repository

/**
 * Created by David Crotty on 14/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
interface IUnitOfWork {
    fun complete()
}