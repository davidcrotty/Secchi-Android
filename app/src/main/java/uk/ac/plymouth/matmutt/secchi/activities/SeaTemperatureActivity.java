//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Sea temperature Activity.												//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import io.realm.Realm;
import io.realm.RealmQuery;
import kotlin.jvm.functions.Function1;
import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.repository.UnitOfWork;
import uk.ac.plymouth.matmutt.secchi.repository.realm.RealmContext;


//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class SeaTemperatureActivity extends Activity implements
												     OnClickListener,
												     OnLongClickListener,
												     OnSeekBarChangeListener
{

	public static String TEMPERATURE_INTENT_KEY = "TEMPERATURE_INTENT_KEY";
	public static String TIME_OF_READING_KEY = "TIME_OF_READING_TEMPERATURE_KEY";
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName           = "";
//	private SecchiPackageVariables secchiPackageVariables = null;
	private Button                 buttonSave             = null; 
	private Button                 buttonCancel           = null;
	private Button				   buttonPlus             = null;
	private Button                 buttonMinus            = null; 
	private TextView               textViewTemperature    = null;
	private SeekBar                seekBarTemperature     = null;
	private float                  seaTemperature		  = 0;
	private float                  minSeaTemperature      = -4;
	private float                  maxSeaTemperature      = 40;
    private IncDecThread           incDecThread           = null;
	private EditText               editTextDialogByKebd	  = null;
	private volatile boolean       sliderScrolled         = false;
	private UnitOfWork unitOfWork = null;
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sea_temperature);
		unitOfWork = new UnitOfWork(new RealmContext(Realm.getDefaultInstance()));
        activityName           = getClass().getSimpleName();


        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        buttonSave            = (Button)    findViewById(R.id.buttonSave);
        buttonCancel          = (Button)    findViewById(R.id.buttonCancel);
        buttonPlus            = (Button)    findViewById(R.id.buttonPlus);
        buttonMinus           = (Button)    findViewById(R.id.buttonMinus);
        textViewTemperature   = (TextView)  findViewById(R.id.textViewSeaTemperature);
        seekBarTemperature    = (SeekBar)   findViewById(R.id.seekBarTemperature);
        
        
        buttonPlus.setOnClickListener(this);
        buttonMinus.setOnClickListener(this);    
        buttonSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        buttonPlus.setOnLongClickListener(this);
        buttonMinus.setOnLongClickListener(this);
        
        
        //******************************************************************//
        // There seems to be no minimum; we must calculate this??			//
        //******************************************************************//
        seekBarTemperature.setMax((int) (maxSeaTemperature - minSeaTemperature));
        seekBarTemperature.setOnSeekBarChangeListener(this);
    }

   
    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   
	   if (incDecThread != null)
	   {
		   incDecThread.running = false;
		   incDecThread         = null;
	   }
   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		ApplicationState applicationState = unitOfWork.getRealmContext().getApplicationState();
		if(applicationState != null) {
			if (applicationState.getInProgressReading() != null) {
				Float temperature = applicationState.getInProgressReading().getTemperature();
				if (temperature != null) {
					seaTemperature = temperature;
				}
			}
		}
		unitOfWork.complete();

		if (seaTemperature < minSeaTemperature)
			seaTemperature = 10;					//Constrain and sensible default.
		if (seaTemperature > maxSeaTemperature)
			seaTemperature = maxSeaTemperature;
		
		seekBarTemperature.setProgress((int)(seaTemperature - minSeaTemperature));
		textViewTemperature.setText(
				Enumerators.buildTemperatureString(this, seaTemperature));

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }


   
     
     //**********************************************************************//
     // Update Temperature.  Also round to 0.1.								 //
     //**********************************************************************//
     private void updateTemperature()
     {
		 seaTemperature *= 10;							//Round to 0.1 and
		 seaTemperature = Math.round(seaTemperature);	
		 seaTemperature /= 10;
 
		if (seaTemperature < minSeaTemperature)
			seaTemperature = minSeaTemperature;					//Constrain.
		if (seaTemperature > maxSeaTemperature)
			seaTemperature = maxSeaTemperature;

		textViewTemperature.setText(
				Enumerators.buildTemperatureString(this, seaTemperature));
		
		if (!sliderScrolled)
			seekBarTemperature.setProgress((int)(seaTemperature - minSeaTemperature));
		sliderScrolled = false;
     }
     
    
     
     
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		switch (view.getId())
 		{
 			case R.id.buttonPlus:
				 seaTemperature += 0.1;
				 updateTemperature();
 				 break;
 				 
 			case R.id.buttonMinus:
				 seaTemperature -= 0.1;
				 updateTemperature();
 				 break;
 				 
 			case R.id.buttonSave:
				 Intent result = new Intent();
				 result.putExtra(TEMPERATURE_INTENT_KEY, seaTemperature);
				 result.putExtra(TIME_OF_READING_KEY, System.currentTimeMillis());
				 setResult(Activity.RESULT_OK, result);
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.buttonCancel:
				 Intent cancelResult = new Intent();
				 cancelResult.putExtra(TEMPERATURE_INTENT_KEY, seaTemperature);
				 cancelResult.putExtra(TIME_OF_READING_KEY, System.currentTimeMillis());
				 setResult(Activity.RESULT_CANCELED, cancelResult);
 				 finish();
 				 break;		// Unreachable.
 				 
 				 
 			case R.id.imageViewSoftKeyboard:
				 //*********************************************************//
				 // Build a crude alert dialog and put in an EditText.		//
				 //*********************************************************//
				 AlertDialog.Builder dialogDepth = new AlertDialog.Builder(this);

				 dialogDepth.setTitle(R.string.enterTempreatuteminus4to40);
				 dialogDepth.setMessage(R.string.pleaseEnterValidTemperature);

				 editTextDialogByKebd = new EditText(getApplicationContext());
				 editTextDialogByKebd.setInputType(InputType.TYPE_CLASS_NUMBER
						                         | InputType.TYPE_NUMBER_FLAG_DECIMAL);
				 dialogDepth.setView(editTextDialogByKebd);

				 dialogDepth.setPositiveButton(getString(R.string.OK), 
						    new DialogInterface.OnClickListener() 
				 {
	 				 public void onClick(DialogInterface dialog, int whichButton) 
	 				 {
	 					 try
	 					 {
	 						 String strAns = editTextDialogByKebd.getText().toString();
	 						 float  ans    = Float.valueOf(strAns);
	 						 ans           = Math.round(ans * 10f)/10f;
	 				
	 						 if ((ans < minSeaTemperature) || (ans > maxSeaTemperature))
	 								 throw new NumberFormatException("Out of range");
	 						 
	 						seaTemperature  = ans;
	 						sliderScrolled  = false;
	 						updateTemperature();
	 					 }
	 					 catch (Exception ee)
	 					 {
	 						 Toast.makeText(getApplicationContext(), 
	 								 R.string.pleaseEnterValidTemperature, 
	 								 Toast.LENGTH_LONG).show();
	 					 }
	 				  }
	 				});


				dialogDepth.show();
				break;
			}
 	}

 	
 	

  	//***********************************************************************//
  	// Overridden from View.OnLongclickListener.							 //
  	//***********************************************************************//
 	@Override
	public boolean onLongClick(View view) 
 	{
 		boolean down = view.isPressed();
 		if (D.debug)
 			Log.i(activityName, "button long pressed: down = " + down);
 	
 		if (incDecThread == null)
 			incDecThread = new IncDecThread();
 		
 		
 		switch (view.getId())
 		{
 			case R.id.buttonPlus:
 				 if (!incDecThread.running)
 					 incDecThread.increment = true;
 					 incDecThread.execute();
 				 break;
 				 
 			case R.id.buttonMinus:
				 if (!incDecThread.running)
 					 incDecThread.increment = false;
 					 incDecThread.execute();
 				 break;
 		}
 		return true;
	}


 	

 	
 		


 	//**********************************************************************//
 	// Overridden from OnSeekBarChangeListener.  We have only one seek bar, //
 	// but we still have a switch anyway.									//
 	//**********************************************************************//
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) 
	{
		seaTemperature = progress + minSeaTemperature;
		sliderScrolled = true;
		updateTemperature();
	}
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) 
	{	
	}
	@Override
	public void onStopTrackingTouch(SeekBar seekBar) 
	{
	}

	
	
	
    //**********************************************************************//
 	// Inner class for a Thread to auto increment / decrement secchi depth.	//
 	//**********************************************************************//
 	private class IncDecThread extends AsyncTask<Void, Void, Void>
 	{

 		//******************************************************************//
 		// Public instance variables; oops.									//
 		//******************************************************************//
 		public volatile boolean running   = false;
 		public volatile boolean increment = false;
 		
 		
    	//******************************************************************//
    	// Executed in a separate thread in the background					//
    	// Weird syntax for "some parameters may follow, but I don't know   //
    	// anything about them".... 										//
    	//******************************************************************//
		@Override
		protected Void doInBackground(Void... params) 
		{
			running = true;
			while (running)
			{	
				try 
				{
					Thread.sleep(100);		// 1/10 second
					publishProgress();  	// Invokes onProgressUpdate()
				} 
				catch (InterruptedException e) 
				{
					Log.i(activityName, "Timer Thread interrupted");
				}
			}
    		return null;
		}
 		

		
		
		//******************************************************************//
    	// Invoked when we execute publishProgress().						//
    	//******************************************************************//
    	@Override
        protected void onProgressUpdate(Void... progress) 
    	{
    		boolean pressed = (buttonPlus.isPressed()
    				        || buttonMinus.isPressed());
    		
    		//**************************************************************//
    		// If nothing pressed, stop the thread and return.  Roy look	//
    		// away.														//
    		//**************************************************************//
    		if (!pressed)
    		{
    			running      = false;
    			incDecThread = null;
    			return;
    		}
    		
    		sliderScrolled = false;
    		//******************************************//
    		// Going up or down.....					//
    		//******************************************//
    		if (increment)
    		{
				seaTemperature += 0.1;
				updateTemperature();
    		}
    		else
    		{
    			seaTemperature -= 0.1;
				updateTemperature();
    		}
    		sliderScrolled = true;
    	}    	
    } //End of AsyncTask


}	// End of classy class.
