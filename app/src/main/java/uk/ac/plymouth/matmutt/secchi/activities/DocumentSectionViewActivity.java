//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// This Activity allows you to viewI n documents (images only at present),   //
// with a swipe between them.												//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.R.id;
import uk.ac.plymouth.matmutt.secchi.R.layout;
import uk.ac.plymouth.matmutt.secchi.R.string;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.IInterface;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.GestureDetector.OnGestureListener;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//																		 	//
// Here we return a 0 / 1 result, 1 if all images have been viewed.  	 	//
//**************************************************************************//
public class DocumentSectionViewActivity extends Activity  implements
												 OnTouchListener,
												 OnGestureListener,
												 OnClickListener
{

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName           = "";
	private GestureDetector        gestureDetector        = null;
	private SecchiPackageVariables secchiPackageVariables = null;
	private ImageView              imageViewSectionImage  = null;
	private TextView               textViewSectionNumber  = null;
	private int[]				   iDImageNames           = null;
	private boolean[]              imageViewed            = null;
	private int                    imageNumber            = 0;
	private int                    numImages              = 0;
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.documentation_section_layout);
        gestureDetector = new GestureDetector(getApplicationContext(), this);
                
        secchiPackageVariables = SecchiPackageVariables.getInstance(this);
        activityName           = getClass().getSimpleName();

        //**********************************************************************//
        // Show a toast for the first section only.								//
        //**********************************************************************//
        if (!secchiPackageVariables.isUserHasReadSecchiInfo())
        	Toast.makeText(this, R.string.swipeToViewNextPage, 
        			       Toast.LENGTH_SHORT).show();
        
        //**********************************************************************//
        // Get image names.														//
        //**********************************************************************//
        try
        {
        	iDImageNames = getIntent().getIntArrayExtra("image_names");
        }
        catch (Exception doh)
        {
        	if (D.debug)
        		Log.e(activityName, "Couldn't get image data");
        	finish();
        }

        //*****************************************************************//
   	 	// Get the UI components.         									//
   	 	//*****************************************************************//
   	 	imageViewSectionImage = (ImageView) findViewById(R.id.imageViewSectionImage);
   	 	textViewSectionNumber = (TextView)  findViewById(R.id.textViewSectionNumber);
   	 	imageViewSectionImage.setOnTouchListener(this);	 
   
   	 	numImages   = iDImageNames.length;
   	 	imageViewed = new boolean[numImages];
    }

    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiPackageVariables.release();
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();

		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
		showImage();	     	
		if (D.debug) Log.i(activityName, "Activity resumed");   		
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }
   
     
     
     
 

 	//***********************************************************************//
 	// Overridden from View.OnclickListener.								 //
 	//***********************************************************************//
	@Override
	public void onClick(View view) 
	{
		switch (view.getId())
		{
		}
	
	}

 	
	
	//**********************************************************************//
	// Show the image and record that we have done so.						//
	//																		//
	// Here we return a 0 / 1 result, 1 if all images have been viewed.  	//
    // I use onPause as the result is returned much more immediately 		//
	//**********************************************************************//
	private void showImage()
	{
		imageViewSectionImage.setImageResource(iDImageNames[imageNumber]);
		imageViewed[imageNumber] = true;

		// Update the section number.
		String sn = Integer.toString(imageNumber + 1) 
				  + "/" 
				  + Integer.toString(iDImageNames.length);
		textViewSectionNumber.setText(sn);
		
		
		boolean viewedAll  = true;
		int     resultCode = 0;
		   
		for(boolean viewed : imageViewed)
		{
		   if (!viewed) viewedAll = false;
		   if (!viewed) break;
		}

		   
		if (viewedAll) resultCode = 1;
		else           resultCode = 0; 
		  
		setResult(resultCode);  //Return the result.
}
	
	
	
	//***********************************************************************//
	// Call-backs overridden from OnTouchListener.							 //
	//***********************************************************************//
	@Override
	public boolean onTouch(View v, MotionEvent event) 
	{
		gestureDetector.onTouchEvent(event);
		return true;
	}
	@Override
	public boolean onDown(MotionEvent e) 
	{
		return false;
	}


	
	
	
	//***********************************************************************//
	// Call-backs overridden from GestureListener.							 //
	//***********************************************************************//
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) 
	{
		if (velocityX < 100)
		{
			imageNumber++;
			
			//**************************************************************//
			// Finish the activity if we have seen all the images.			//
			//**************************************************************//
			if (imageNumber >= numImages) 
			{
				imageNumber = numImages-1;
				finish();
			}
			
			if (D.debug)
				Log.i(activityName, "fling right, velocity : " + velocityX);
		}
		if (velocityX > 100)
		{
			imageNumber--;
			if (imageNumber < 0) imageNumber = 0;
			
			if (D.debug)
				Log.i(activityName, "fling left, velocity : " + velocityX);
		}		
		showImage();
		return true;
	}
	
	@Override
	public void onLongPress(MotionEvent e) 
	{
	}
	
	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2,
			float distanceX, float distanceY) 
	{

		return true;
	}
	@Override
	public void onShowPress(MotionEvent e) 
	{
		
	}
	@Override
	public boolean onSingleTapUp(MotionEvent e) 
	{
		return false;
	}
	

}	// End of classy class.
