//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.	 Actually now   //
// closer to 4 than to 2 now, Feb 2013.										//
//																			//
// Class  to do the web stuff.												//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2013.											//
//**************************************************************************//
package uk.ac.plymouth.matmutt.secchi.helpers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.format.DateFormat;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.security.MessageDigest;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import uk.ac.plymouth.matmutt.secchi.interfaces.IHTTPResultStringListener;

public class WebStuff 
{
	//http://developer.android.com/reference/java/security/MessageDigest.html

	//**********************************************************************//
	// Instance variables for constant stuff.								//
	//**********************************************************************//
	/* Old URLs, should probably be deleted.
	private final String    UrlGetNewID        = "http://user42691.vs.easily.co.uk/phpsql_userform.php";
	private final String    UrlServerBase      = "user42691.vs.easily.co.uk";
	private final String    UrlSubmit		   = "http://user42691.vs.easily.co.uk/phpsql_secchidataform.php";
	public static String    UrlSubmit		   = "http://141.163.187.34/phpsql_secchidataform.php";
	public static String    UrlSubmit		   = "http://192.168.1.75/phpsql_secchidataform.php";
	*/
	
	// Real URLs
	private final String    UrlGetNewID        = "https://www.playingwithdata.com/phpsql_userform.php";
	private final String    UrlSubmit		   = "https://www.playingwithdata.com/phpsql_secchidataform.php";
	private final String    UrlServerBase      = "https://www.playingwithdata.com";
	
	// Test URLs
//	private final String    UrlGetNewID        = "https://www.playingwithdata.com/phpsql_userform_test.php";
//	private final String    UrlSubmit		   = "https://www.playingwithdata.com/phpsql_secchidataform_test.php";
//	private final String    UrlServerBase      = "https://www.playingwithdata_test.com";

	
	// Own server for testing
//	private final String    UrlSubmit		   = "http://141.163.187.185/phpsql_secchidataform.php";
	
	// Insecure test URLs
//	private final String    UrlGetNewID        = "http://www.playingwithdata.com/phpsql_userform_test.php";
//	private final String    UrlSubmit		   = "http://www.playingwithdata.com/phpsql_secchidataform_test.php";
//	private final String    UrlServerBase      = "http://www.playingwithdata_test.com";
	
	
	private final int       ConnectTimeoutSecs = 15 * 1000; 
	

	//**********************************************************************//
	// Crypto stuff.   There are several strings here, just in case anybody	//
	// tries to reverse engineer this.										//
	//**********************************************************************//
	private final int       ReadTimeoutSecs      = 10 * 1000; 
	private final String    cryptoHash           = "sha256";			//sha256
	private final String    nonk                 = "udfhakhjk&343534[2Hgsg&^g7*gbvvs";
	private final String    nink                 = "oierjfv3rw90uejfiosdnwefdsvnfesd";
	private final String    pink                 = "ieowfjfjsoidlclsdiojvsdiovjdsvvv";
	private final String    rink                 = "rioehfd;ovh;sdoifvdfh;dfihdfassas";
	private final boolean   trustAllCerts        = false;
	private TrustManager[]  trustManagerAllCerts = null;
	

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                    activityName             = "Web Stuff";
	private IHTTPResultStringListener httpResultStringListener = null;
    private String                    twoHyphens               = "--";
    private String                    boundary                 = null;
    private String 					  newLine                  = "\r\n";
	private String                    contentType              = "Content-Type";
	private String contentDisposition = "Content-Disposition: form-data; name=";
	private int                       maxBufferSize            = 400*1024;
	
	
	
	//**********************************************************************//
	// Constructor.															//
	//**********************************************************************//
	public WebStuff()
	{
		if (trustAllCerts) createTrustManagerAllCerts();
	}
	
	

	//**********************************************************************//
	// Is the server reachable?  A but squiffy at present.					//
	//**********************************************************************//
	public boolean isServerReachable()
	{
		InetAddress server = null;
		try 
		{
			server = InetAddress.getByName(UrlServerBase);
			if (server.isReachable(1000))					//Timeout
				return true;
			else
				return false;
		} 
		catch (Exception e) 
		{
			if (D.debug)
				Log.e(activityName, "Can't reach server " 
						            + e.getMessage());
			return false;
		}
	}
	
	
	
	
	
	
	
	//**********************************************************************//
	// Get a new user ID.  Fires off a Thread, and fires off a callback 	//
	// through a handler.													//
	//**********************************************************************//
	public void getNewID()
	{ 
		GetIdThread getIdThread = new GetIdThread();
		getIdThread.start();
	}


	
	
	//**********************************************************************//
	// Submit the reading.  Fires off a Thread, and fires off a callback 	//
	// through a handler.													//
	//**********************************************************************//
	public void submitReading(SecchiReadingX reading, String appUser)
	{ 
		SubmitDataThread submitDataThread = new SubmitDataThread(reading, appUser);
		submitDataThread.start();
	}

	
	
	
	
	//**********************************************************************//
	// Build date strings.													//
	//**********************************************************************//
	private DateAsStrings buildDateStrings(long date)
	{
		String   raw   = DateFormat.format("MM dd yyyy kk mm ss zz ", date).toString();
		String[] items = raw.split(" ");
		
		DateAsStrings ds = new DateAsStrings();
		
		try
		{
			ds.strMonth = items[0];
			ds.strDay   = items[1];
			ds.strYear  = items[2];
			ds.strHour  = items[3];
			ds.strMin   = items[4];
			ds.strSec   = items[5];
			
			//**************************************************************//
			// Make sure these are two character, they need to be for 		//
			// calculating the magic ident									//
			//**************************************************************//
			if (ds.strHour.length() == 1) ds.strHour = "0" + ds.strHour;
			if (ds.strMin.length()  == 1) ds.strMin  = "0" + ds.strMin;
			if (ds.strSec.length()  == 1) ds.strSec  = "0" + ds.strSec;
		}
		catch (Exception doh)
		{
			if (D.debug)
				Log.e(activityName, "Error building date strings "
						            +  doh.getMessage());
		}
		
		return ds;
	}
	
	
	//***********************************************************************//
	// Handler so that the Thread messages can come back in the user Thread. //
	//***********************************************************************//
	Handler messageHandler = new Handler()
	{
		
		//*******************************************************************//
		// We must override this one, for pretty obvious reasons.			 //
		//*******************************************************************//
		@Override
		public void handleMessage(Message message) 
		{
			String result = "null";
			try
			{
				result = message.getData().getString("newID");
				if (result != null)
				{
					httpResultStringListener.onStringReceived(result);
					return;						//Roy look away.
				}
				
				result = message.getData().getString("Submit");
				if (result != null)
				{
					httpResultStringListener.onStringReceived(result);
					return;						//Roy look away.					
				}
			}
			catch (Exception e)
			{
				if (D.debug)
					Log.e(activityName, "Error in handler receiving web stuff "
							            + e.getMessage());
			}	
		}
	};	//End of handler
	


	
	
	
	

	
	
	//***********************************************************************//
	// Add a text element to our multipart form.  Throws Exception.			 //
	//***********************************************************************//
	private void addTextElementToForm(DataOutputStream out, 
			                          String           name, 
			                          String value) throws Exception
	{
		out.writeBytes(twoHyphens+ boundary);
		out.writeBytes(newLine);
		out.writeBytes(contentDisposition 
				+ "\""
				+ name
				+ "\""
				+ newLine
				+ newLine
				+ value
				+ newLine);
	}
	
	
	
	//***********************************************************************//
	// Add a an image to our multipart form.  The image is specified by a 	 //
	// path name.															 //
	//	Throws Exception.			 										 //
	//***********************************************************************//
	private void addImageElementToForm(DataOutputStream out, 
			                          String            pathName, 
			                          String value)     
			                        		            throws Exception
	{
		String[] parts = pathName.split("/");		// Extract file name from
		String   fileName = parts[parts.length-1];	// path name.
				
				
		out.writeBytes(twoHyphens + boundary + newLine);
		out.writeBytes(contentDisposition
				       + "\""
				       + value
				       + "\"; filename=\""
				       + fileName
				       +"\""
				       + newLine);
		
		out.writeBytes("Content-Type: image/jpeg" + newLine);
		out.writeBytes("Content-Transfer-Encoding: binary" + newLine);
		out.writeBytes(newLine);
        
		FileInputStream     fi = new FileInputStream(pathName);
		BufferedInputStream in = new BufferedInputStream(fi);
		
        int bytesAvailable = in.available();
        int bufferSize     = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer      = new byte[bufferSize];
        
        int  bytesRead     = in.read(buffer, 0, bufferSize);
        if (bytesRead != bytesAvailable)
        	throw new Exception("Error reading image file");
        
        out.write(buffer, 0, bufferSize);
        out.writeBytes(newLine);
        out.writeBytes(newLine);

	}
	
	
	
	
	//***********************************************************************//
	//***********************************************************************//
	// Inner class for a Thread to generate the time of last fix.  It uses a //
	// message handler so that the Thread's messages come back in the UI 	 //
	// Thread, which is Google's Threading rule #2.							 //
	//																		 //
	// Error codes that can be returned:									 // 
	// OK (successful) 													 	 //
	// UID_INVALID 		(appuser doesn't pre-exist)							 //
	// SECCHI_INVALID 	(checks both GPS & value field. Returned when year   //
	//					less than 2012 or depth negative)           		 //
	// GPS_INVALID 		(returned when year less than 2012)					 //
	// UPLOAD_FAILED  	(could not upload data to database / verify it 		 //
	// 					uploaded correctly)  								 //
	// DATABASE_ERROR 	(could not communicate with database)				 // 
	// IMG_INVALID      (image upload failed or size > 350Kb).				 //
	// CONNECTION_ERR   (general exception, probably no network connection). //
	//***********************************************************************//
	private class SubmitDataThread extends Thread implements Runnable
	{

		//*******************************************************************//
		// Constructor.														 //
		//*******************************************************************//
		public SubmitDataThread(SecchiReadingX reading, String appUser)
		{
			super();
			this.reading = reading;
			strAppUser   = appUser;
		}
		
		
		
		//*******************************************************************//
		// Instance variables.  											 //
		//*******************************************************************//
		private SecchiReadingX reading    = null;
		private String         strAppUser = null;
		
		
		//*******************************************************************//
		// Thread's run() method.											 //
		//*******************************************************************//
		@Override
		public void run()
		{
			if (reading == null)
			{	if (D.debug)
					Log.e(activityName, "Submit data Thread; no data");
				return;
			}
			
			
			//**************************************************************//
			// Multi-part form boundary.									//
			//**************************************************************//
			boundary = "*******************" 
			           + Long.toHexString(System.currentTimeMillis())
			           + "**";
			
			
			//**************************************************************//
			// First build up strings representing all the variables we 	//
			// will need.													//
			//																//
			// I have named them to be consistent with the names on Sam's	//
			// form.														//
			//**************************************************************//
			String strDevice   = reading.getExtra();
			String strBoatName = reading.getVessellName();
				
						
			DateAsStrings locationDateAsStrings 
            			  = buildDateStrings(reading.getTimeOfLocation());
			String strLat = String.valueOf(reading.getLatitude());
			String strLon = String.valueOf(reading.getLongitude());

			String strDepth = String.valueOf(reading.getDepth());
			DateAsStrings depthDateAsStrings 
                          = buildDateStrings(reading.getTimeOfDepth());
			
			
			String strTemperature = String.valueOf(reading.getTemperature());
			DateAsStrings temperatureDateAsStrings 
                          = buildDateStrings(reading.getTimeOfTemperature());
			
			String  strPhotoPath  = reading.getPhotoLocation();
	        String  strNotes      = reading.getNotes();
			Bundle  bundle        = new Bundle();
			Message message       = messageHandler.obtainMessage();

			
			
			//**************************************************************//
			// iNick's magic ident.											//
			//**************************************************************//
			//locationDateAsStrings.strHour ="22";
			//locationDateAsStrings.strMin = "20";
			//locationDateAsStrings.strSec = "18";
			//strAppUser = "61L8M3JUFIG27UUV5JB0";
			
			String strIdent = cryptoHash + " not supported";
			try 
			{
				MessageDigest digester = MessageDigest.getInstance(cryptoHash);
				String strMagicString  = strAppUser
						               + locationDateAsStrings.strHour
						               + locationDateAsStrings.strMin
						               + locationDateAsStrings.strSec;
				
				digester.reset();
				//digester.update(saltString.getBytes("UTF8"));
				//digester.update(strMagicString.getBytes("UTF8"));

				strMagicString = nonk + strMagicString;
				

				//digester.reset();
				//digester.update("ponk".getBytes("UTF8")); //salt
				//digester.update("pink".getBytes("UTF8")); 
				
				byte[] ident = digester.digest(strMagicString.getBytes("UTF8"));
				strIdent     = new String();

				
/*
  				for (byte b : ident) 
 
				{
					int i = (int) (0xFF & b);			//What a pile of shit, all because
					String bs = Integer.toHexString(i);	//Java bytes are always signed.
					if (bs.length() == 1) 
						bs = "0" + bs;
					strIdent += bs;
				}
*/
				
		     
		     StringBuffer sb = new StringBuffer();
		      
		     for (int i = 0; i < ident.length; i++) 
		     {
		         sb.append(Integer.toString((ident[i] & 0xff) + 0x100, 16).substring(1));
	         }

		     strIdent = sb.toString();
		     
		     
				
			 if (D.debug)
				Log.i(activityName, strIdent);
			} 
			catch (Exception doh2) 
			{
				if (D.debug)
					Log.e(activityName, "Can't make crypto hash " 
			                         	+ cryptoHash
			                         	+ " reason: "
			                         	+ doh2.getMessage());
			}  
			
//			byte[] bytes = new byte[8192];  
//			int byteCount;  
//			while ((byteCount = in.read(bytes)) > 0) {    digester.update(bytes, 0, byteCount);  }  byte[] digest = digester.digest(); 
//			
			
			//**************************************************************//
			// Network stuff.												//
			//**************************************************************//
	   		String             responseStr = "null";
	   		HttpsURLConnection con     = null;
	        DataOutputStream   out     = null;
	        BufferedReader     in      = null;

	   		try
	   		{
	   			URL url = new URL(UrlSubmit);

	   			con = (HttpsURLConnection) url.openConnection();
		        con.setReadTimeout(ReadTimeoutSecs);
		        con.setConnectTimeout(ConnectTimeoutSecs);		//Timeout in milliseconds.
		        
		        
		        con.setDoInput(true);
		        con.setDoOutput(true);
		        con.setUseCaches(false);
                
		        con.setRequestMethod("POST");
		        con.setRequestProperty("Connection", "Keep-Alive");
//		        con.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.5");
		        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
		        con.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		        con.setRequestProperty("Accept-Encoding", "gzip,deflate");
		        con.setRequestProperty("Accept-Language", "en-GB,en-US;q=0.8,en;q=0.6");
		        con.setRequestProperty("Cache-Control", "max-age=0");
		        con.setRequestProperty("Origin", "null");
		        con.setRequestProperty(contentType, "multipart/form-data; boundary="+boundary);
		        
		        
//		        URLConnection connection = new URL("https://www.google.com/search?q=" + query).openConnection();
//		        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
//		        connection.connect();

		        //**************************************************************//
		        // Install the all-trusting trust manager						//
		        //**************************************************************//
			     if (trustAllCerts) try 
			        {
			            SSLContext sc = SSLContext.getInstance("SSL");
			            sc.init(null, trustManagerAllCerts, new java.security.SecureRandom());
			            con.setSSLSocketFactory(sc.getSocketFactory());
			        } 
			        catch (Exception e) 
			        {
			        	if (D.debug)
			        		Log.e(activityName, "can't install TM");
			        }

		        //con.connect();
		        
                out = new DataOutputStream(con.getOutputStream());
                
                //out.writeBytes(newLine);
                //out.writeBytes(newLine);
                              
               
    			//**************************************************************//
                // Header stuff.	User, boat name, device, and ident.  		//
                // Whatever	an ident is.	IDENT not moved.					//
    			//**************************************************************//
                addTextElementToForm(out, "appuser", strAppUser);
                addTextElementToForm(out, "deviceinfo", strDevice);
                addTextElementToForm(out, "boatname", strBoatName);
                addTextElementToForm(out, "ident", strIdent);
                
    			//**************************************************************//
                // Location date.												//
    			//**************************************************************//
                addTextElementToForm(out, "gps_day",  locationDateAsStrings.strDay);
                addTextElementToForm(out, "gps_mnth", locationDateAsStrings.strMonth);
                addTextElementToForm(out, "gps_yr",   locationDateAsStrings.strYear);
                addTextElementToForm(out, "gps_hr",   locationDateAsStrings.strHour);
                addTextElementToForm(out, "gps_min",  locationDateAsStrings.strMin);
                addTextElementToForm(out, "gps_sec",  locationDateAsStrings.strSec);
                
    			//**************************************************************//
                // Lat / lon.													//
    			//**************************************************************//
                addTextElementToForm(out, "lat", strLat);
                addTextElementToForm(out, "lon", strLon);
              
                
       			//**************************************************************//
                // Secchi depth date.											//
       			//**************************************************************//
                addTextElementToForm(out, "sec_day",  depthDateAsStrings.strDay);
                addTextElementToForm(out, "sec_mnth", depthDateAsStrings.strMonth);
                addTextElementToForm(out, "sec_yr",   depthDateAsStrings.strYear);
                addTextElementToForm(out, "sec_hr",   depthDateAsStrings.strHour);
                addTextElementToForm(out, "sec_min",  depthDateAsStrings.strMin);
                addTextElementToForm(out, "sec_sec",  depthDateAsStrings.strSec);
                               
      			//**************************************************************//
                // Secchi depth.												//
      			//**************************************************************//
                addTextElementToForm(out, "secchidepth",  strDepth);
             
                
                
      			//**************************************************************//
                // Temperature date.											//
                // if there is a temperature, addAll it.							//
      			//**************************************************************//
                if (!(reading.getTemperature() < -10))
                {
	                addTextElementToForm(out, "sst_day",  temperatureDateAsStrings.strDay);
	                addTextElementToForm(out, "sst_mnth", temperatureDateAsStrings.strMonth);
	                addTextElementToForm(out, "sst_yr",   temperatureDateAsStrings.strYear);
	                addTextElementToForm(out, "sst_hr",   temperatureDateAsStrings.strHour);
	                addTextElementToForm(out, "sst_min",  temperatureDateAsStrings.strMin);
	                addTextElementToForm(out, "sst_sec",  temperatureDateAsStrings.strSec);
	                            
	                //**********************************************************//
	                // Sea temperature.											//
	                //**********************************************************//
	                addTextElementToForm(out, "sst",  strTemperature);
                }
                
                
      			//**************************************************************//
                // Bloody image.												//
      			//**************************************************************//
                if (!strPhotoPath.equalsIgnoreCase("null"))
                	addImageElementToForm(out, strPhotoPath, "file");
                
                
      			//**************************************************************//
                // Notes.														//
      			//**************************************************************//
                addTextElementToForm(out, "notes",  strNotes);
                
                
      			//**************************************************************//
                // Submit button.												//
      			//**************************************************************//
                addTextElementToForm(out, "submit",  "Submit");
             
                
      			//**************************************************************//
                // Fix.												            //
      			//**************************************************************//
                out.writeBytes("--" + boundary + "--" + newLine);

                //out.flush();
                //sleep(100);
      
                con.connect();
                
                //**************************************************************//
                // Did the server return 20something?  e.g. 200?  Error if not.	//
                //**************************************************************//
                int status = con.getResponseCode();
                status /= 100;
                
                if (status != 2)
                {
                   if (D.debug)
                	   Log.e(activityName, "HTTP Commection returned error " 
                			               + status);
                   
      		       InputStream is = con.getErrorStream(); 
     		       in             = new BufferedReader(
    			                       new InputStreamReader(is, "UTF-8"));
    		       String line    = "";
    		       
     		       try
     		       {   do
	     		       {
	     		    	   line = in.readLine();
	     		    	   if (D.debug)
	     		    		   Log.e(activityName, line);
	     		       }
	     		       while (line != null);
     		       }
     		       catch (Exception ee)
     		       {
     		    	   
     		       }
                	
                	throw new Exception("Error other than 200");
                }
                
               //**************************************************************//
		       // Now read the response.								  	   //
      		   //**************************************************************//
 		       InputStream is = con.getInputStream(); 
 		       in             = new BufferedReader(
			                       new InputStreamReader(is, "UTF-8"));
		        		       
		       responseStr = in.readLine();
		       responseStr =  responseStr.replaceAll("\"", "");		// Remove "
		       in.close();
               out.close();
		       con.disconnect();		       
	   		}
		    catch (Exception e ) 
			{       
			       try 
			       {
			    	   in.close();
		               out.close();
				       //con.disconnect();
		               responseStr = "CONNECTION_ERR";
			       } 
			       catch (Exception e1) 
			       {  
		               responseStr = "CONNECTION_ERR";
			       }


		    	if (D.debug)
					Log.e(activityName, "error in getID Thread " 
				                        + e.getMessage());
			}
			

			bundle.putString("Submit", responseStr);
			message.setData(bundle);
			messageHandler.sendMessage(message);
		}
	} // End of inner class



	
	

	//***********************************************************************//
	//***********************************************************************//
	// Inner class for a Thread to generate the time of last fix.  It uses a //
	// message handler so that the Thread's messages come back in the UI 	 //
	// Thread, which is Google's Threading rule #2.							 //
	//***********************************************************************//
	private class GetIdThread extends Thread implements Runnable
	{

		//*******************************************************************//
		// Instance variable.												 //
		//*******************************************************************//
		HttpsURLConnection con     = null;
        BufferedReader     in      = null;
		
		//*******************************************************************//
		// Thread's run() method.											 //
		//*******************************************************************//
		@Override
		public void run()
		{
			Bundle    bundle   = new Bundle();
			Message   message  = messageHandler.obtainMessage();
   			SecchiHNV verifier = new SecchiHNV(UrlGetNewID);
	
			
	   		String            responseStr = "No luck";

	   		try
	   		{
	   			URL url = new URL(UrlGetNewID);
	   			
	   			con = (HttpsURLConnection) url.openConnection();
		        con.setReadTimeout(ReadTimeoutSecs);
		        con.setConnectTimeout(ConnectTimeoutSecs);		//Timeout in milliseconds.
		        con.setRequestMethod("POST");		//Use HTTP GET
		        con.setChunkedStreamingMode(0);	    //Default
		        con.setDoInput(true);				//So that we can read response back
		        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
		        con.setRequestProperty("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
		        con.setRequestProperty("Accept-Encoding", "gzip,deflate");
		        con.setRequestProperty("Accept-Language", "en-GB,en-US;q=0.8,en;q=0.6");
		        con.setRequestProperty("Cache-Control", "max-age=0");
		        con.setRequestProperty("Origin", "null");

		        
		        //**************************************************************//
		        // Install the all-trusting trust manager						//
		        //**************************************************************//
			     if (trustAllCerts) try 
			        {
			            SSLContext sc = SSLContext.getInstance("SSL");
			            sc.init(null, trustManagerAllCerts, new java.security.SecureRandom());
			            con.setSSLSocketFactory(sc.getSocketFactory());
			        } 
			        catch (Exception e) 
			        {
			        	if (D.debug)
			        		Log.e(activityName, "can't install TM");
			        }
		        
		        con.connect();
		        
		        
	            int status = con.getResponseCode();  
                if (status != 200)
                {
                   if (D.debug)
                	   Log.e(activityName, "HTTP Commection returned error " 
                			               + status);
                   
      		       InputStream is = con.getErrorStream(); 
     		       in             = new BufferedReader(
    			                       new InputStreamReader(is, "UTF-8"));
    		       String line    = "";
    		       
     		       try
     		       {   do
	     		       {
	     		    	   line = in.readLine();
	     		    	   if (D.debug)
	     		    		   Log.e(activityName, line);
	     		       }
	     		       while (line != null);
     		       }
     		       catch (Exception ee)
     		       {
     		    	   
     		       }
                	
                	throw new Exception("Error other than 200");
                }

		        //********************************************************//
		        // Now read the response.								  //
		        //********************************************************//
		        BufferedReader in = new BufferedReader(
		              new InputStreamReader(con.getInputStream(), "UTF-8"));
		   
		        		       
		       responseStr = in.readLine();
		       in.close();
		       con.disconnect();
		       
		       responseStr = responseStr.replaceAll("\"", "");
	   		}
	   		catch (SSLHandshakeException hand)
	   		{
		    	responseStr = "CONNECTION_ERR";
	   			if (D.debug)
	   				Log.i(activityName, "SSLHandshake exception "
	   						            + hand.getMessage());
	   		}
		    catch (Exception e) 
			{       
		    	responseStr = "CONNECTION_ERR";
				if (D.debug)
					Log.e(activityName, "error in getID Thread " 
				                        + e.getMessage());
				
			}
			

			bundle.putString("newID", responseStr);
			message.setData(bundle);
			messageHandler.sendMessage(message);
		}
	} // End of inner class



	
	
	//***********************************************************************//
	// Create a trust manager that does not validate certificate chains.	 //
	//***********************************************************************//
	private void createTrustManagerAllCerts()
	{
		trustManagerAllCerts = new TrustManager[]
				{
		    new X509TrustManager() 
		    {
		        public java.security.cert.X509Certificate[] getAcceptedIssuers() 
		        {
		            return null;
		        }
		        public void checkClientTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) 
		        {
		        }
		        public void checkServerTrusted(
		            java.security.cert.X509Certificate[] certs, String authType) 
		        {
		        }
		    }
		};
	}
	
	
	
	//***********************************************************************//
	// Create an HostnameVerifier that hardwires the expected hostname.		 //
	// Note that is different than the URL's hostname.						 //
	//***********************************************************************//
	private class SecchiHNV implements HostnameVerifier 
	{
		//*******************************************************************//
		// Constructor.                                                      //
		//*******************************************************************//
		public SecchiHNV(String urlHost)
		{
			this.urlHost = urlHost;
		}
		
		//*******************************************************************//
		// Instance variables.					                             //
		//*******************************************************************//
		private String urlHost = "null";
		
		//*******************************************************************//
		// Implemented method.												 //
		//*******************************************************************//
	    @Override
	    public boolean verify(String hostname, SSLSession session) 
	    {
	        HostnameVerifier hv =
	            HttpsURLConnection.getDefaultHostnameVerifier();
	        //return hv.verify(urlHost, session);
	        
	        return true;
	    }
	}

	
	//***********************************************************************//
	//***********************************************************************//
	// Very small inner class to represent a date and time as strings.		 //
	//***********************************************************************//
	private class DateAsStrings
	{
		
		//******************************************************************//
		// Instance variables.  All public; I am using this as a struct		//
		// rather than a class.												//
		//																	//
		// All seem to be a number represented by a String.					//
		//******************************************************************//
		public String strDay   = null;
		public String strMonth = null;
		public String strYear  = null;
		public String strHour  = null;
		public String strMin   = null;
		public String strSec   = null;
	
	}	// End of classy class
	
	

	//***********************************************************************//
	// Getters and setters.  Mary, Roy, Martin will approve.				 //
	//***********************************************************************//
	

	/**
	 * @return the httpResultStringListener
	 */
	public IHTTPResultStringListener getHttpResultStringListener() {
		return httpResultStringListener;
	}




	/**
	 * @param httpResultStringListener the httpResultStringListener to set
	 */
	public void setHttpResultStringListener(
			IHTTPResultStringListener httpResultStringListener) {
		this.httpResultStringListener = httpResultStringListener;
	}
	

	
}	// End of classy class.
