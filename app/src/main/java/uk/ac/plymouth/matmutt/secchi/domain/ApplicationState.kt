package uk.ac.plymouth.matmutt.secchi.domain

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by David Crotty on 10/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 *
 * Represents state of application, ie: has user read doc, did they quit app mid experiment
 */
open class ApplicationState : RealmObject() {

    companion object {
        val PRIMARY_KEY = "Single"
    }

    @PrimaryKey
    var primaryKey: String = PRIMARY_KEY //only one instance needed

    var inProgressReading: SecchiReading? = null
    var documentationStateList: RealmList<DocumentationPage>? = null
    var whatsNewShown: Boolean = false //TODO could be used to display a newly designed whats new page
    var termsAndConditionsShown: Boolean = false //new condition
    var user: User? = null

    fun hasReadAllDocumentationPages() : Boolean {
        val pages = documentationStateList ?: return false
        for(item in pages) {
            if(item.read == false) {
                return false
            }
        }

        return true
    }

    fun setReadAllDocumentationPages(hasRead: Boolean) {
        val pages = documentationStateList ?: return
        for(item in pages) {
            item.read = hasRead
        }
    }
}
