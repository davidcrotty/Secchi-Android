package uk.ac.plymouth.matmutt.secchi.network.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by David Crotty on 14/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
class PushModel : Serializable {
    @SerializedName("alertTitle")
    var alertTitle: String? = null
    @SerializedName("alertBody")
    var alertBody: String? = null
    @SerializedName("url")
    var URL: String? = null
    @SerializedName("buttonTitle")
    var buttonTitle: String? = null

    fun isValid() : Boolean {
        return alertTitle != null &&
                alertBody != null
    }
}