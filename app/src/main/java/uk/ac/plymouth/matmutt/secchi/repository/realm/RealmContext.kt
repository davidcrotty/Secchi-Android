package uk.ac.plymouth.matmutt.secchi.repository.realm

import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.RealmResults
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState
import uk.ac.plymouth.matmutt.secchi.repository.IUnitOfWork

/**
 * Created by David Crotty on 14/05/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 *
 * Realm specific implementation. Commits happen in unit of work.
 */
class RealmContext(var realm: Realm) : IUnitOfWork {

    val TAG = "RealmContext"

    inline fun <reified T : RealmObject> findAll(): List<T> {
        validateRealmOpen()
        return realm.copyFromRealm(realm.where(T::class.java).findAll())
    }

    inline fun <reified T: RealmObject> findAll(predicate: (T) -> Boolean) : List<T>? {
        validateRealmOpen()
        val list = realm.where(T::class.java).findAll()
        var filteredResult: List<T>? = null
        if(list == null) {
            return null
        } else {
            filteredResult = list.filter(predicate)
            return filteredResult
        }
    }

    fun <T : RealmObject>add(transaction: RealmTransaction<T>) {
        validateRealmOpen()
        realm.beginTransaction()
        for(entity in transaction.holder) {
            realm.copyToRealmOrUpdate(entity)
        }
    }

    fun <T : RealmObject>delete(transaction: RealmTransaction<T>) {
        validateRealmOpen()
        realm.beginTransaction()
        for(entity in transaction.holder) {
            val realmEntity = realm.copyToRealmOrUpdate(entity)
            realmEntity.deleteFromRealm()
        }
    }

    inline fun <reified T : RealmObject>find(predicate: (T) -> Boolean) : T? {
        validateRealmOpen()
        val list = realm.where(T::class.java).findAll()
        var filteredResult: List<T>? = null
        if(list == null) {
            return null
        } else {
            filteredResult = list.filter(predicate)
            if(filteredResult.isNotEmpty()) {
                return realm.copyFromRealm(filteredResult[0])
            }
        }

        return null
    }

    fun getApplicationState() : ApplicationState? {
        validateRealmOpen()
        val applicationState = realm.where(ApplicationState::class.java).findFirst() ?: return null
        return realm.copyFromRealm(applicationState)
    }

    fun validateRealmOpen() {
        if(realm.isClosed) {
            realm = Realm.getDefaultInstance()
        }
    }

    override fun complete() {
        if(realm.isInTransaction) {
            realm.commitTransaction()
        }
        realm.close()
    }
}