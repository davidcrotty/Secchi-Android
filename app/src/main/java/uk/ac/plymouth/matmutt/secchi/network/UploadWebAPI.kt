package uk.ac.plymouth.matmutt.secchi.network

import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.ResponseBody
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

/**
 * Created by David Crotty on 05/09/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
interface UploadWebAPI {
    @POST("phpsql_userform.php") //Should be a GET but external API does not conform to REST whatsoever
    fun getUserID() : Single<ResponseBody>
    @Multipart
    @POST("phpsql_secchidataform.php") //No solution allows us to not use part params with gson, only alternative is hashmap which requires populating still
    fun submit(@Part("appuser") appUser: String, @Part("deviceinfo") deviceInfo: String, @Part("boatname") boatName: String?,
               @Part("ident") ident: String, @Part("gps_day") gpsDay: Int?, @Part("gps_mnth") gpsMonth: Int?,
               @Part("gps_yr") gpsYear: Int?, @Part("gps_hr") gpsHour: Int, @Part("gps_min") gpsMin: Int?,
               @Part("gps_sec") gpsSec: Int?, @Part("lat") lattitude: Float?, @Part("lon") longitude: Float?,
               @Part("sec_day") depthDay: Int?, @Part("sec_mnth") depthMonth: Int?, @Part("sec_yr") depthYear: Int?,
               @Part("sec_hr") depthHour: Int?, @Part("sec_min") depthMin: Int?, @Part("sec_sec") depthSec: Int?,
               @Part("secchidepth") depth: Int?, @Part("sst_day") temperatureDay: Int?, @Part("sst_month") temperatureMonth: Int?,
               @Part("sst_yr") temperatureYear: Int?, @Part("sst_hr") temperatureHour: Int?, @Part("sst_min") temperatureMin: Int?,
               @Part("sst_sec") temperatureSec: Int?, @Part("notes") notes: String?, @Part("sst") temperature: Int?,
               @Part imageFile: MultipartBody.Part?, @Part("wbody") wbody: String = "wbody", @Part("submit") submit: String = "submit") : Single<ResponseBody>
}