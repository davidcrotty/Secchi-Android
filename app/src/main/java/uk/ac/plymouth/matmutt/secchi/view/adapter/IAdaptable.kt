package uk.ac.plymouth.matmutt.secchi.view.adapter

/**
 * Created by David Crotty on 27/08/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
interface IAdaptable {
    fun getViewType() : Int
}