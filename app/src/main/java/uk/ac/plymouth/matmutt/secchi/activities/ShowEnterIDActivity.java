//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Show the user ID or enter a new ID Activity.								//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.Enumerators;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import uk.ac.plymouth.matmutt.secchi.helpers.VesselParticularsX;
import uk.ac.plymouth.matmutt.secchi.helpers.WebStuff;
import uk.ac.plymouth.matmutt.secchi.interfaces.IHTTPResultStringListener;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.content.pm.ActivityInfo;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethod;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;




//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class ShowEnterIDActivity extends Activity implements
										 OnClickListener,
										 OnCheckedChangeListener
{

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName             = "";
	private SecchiPackageVariables secchiPackageVariables   = null;
	private Button                 buttonDone               = null; 
	private Button                 buttonCancel             = null;
	private ProgressBar            progressBarWaiting       = null;
	private EditText               editTextID1              = null;
	private EditText               editTextID2              = null;
	private EditText               editTextID3              = null;
	private EditText               editTextID4              = null;
	private ToggleButton           toggleButtonShowID       = null;
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_enter_id_layout);
               
        secchiPackageVariables = SecchiPackageVariables.getInstance(this);
        activityName           = getClass().getSimpleName();


        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        buttonDone         = (Button)       findViewById(R.id.buttonDone);
        buttonCancel       = (Button)       findViewById(R.id.buttonCancel);
        toggleButtonShowID = (ToggleButton) findViewById(R.id.toggleButtonShowID);
        progressBarWaiting = (ProgressBar)  findViewById(R.id.progressBarGetID);
        editTextID1        = (EditText)     findViewById(R.id.editTextID1);
        editTextID2        = (EditText)     findViewById(R.id.editTextID2);
        editTextID3        = (EditText)     findViewById(R.id.editTextID3);
        editTextID4        = (EditText)     findViewById(R.id.editTextID4);
        
        buttonDone.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        toggleButtonShowID.setOnCheckedChangeListener(this);
    }

   
    
    
   
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiPackageVariables.release();
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
		
		populateEditBoxes();
		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }



        
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		
 		switch (view.getId())
 		{
 			case R.id.buttonCancel:
 				 finish();
 				 break;		// Unreachable.
 				 
 				 
 			case R.id.buttonDone:
 				 String id = editTextID1.getText().toString()
 				           + editTextID2.getText().toString()
 				           + editTextID3.getText().toString()
 				           + editTextID4.getText().toString();
 			     VesselParticularsX vp = secchiPackageVariables.getVesselParticulars();
 			     vp.setSSR(id);
 			     secchiPackageVariables.saveStatus(this);
 			     finish();
				 break;		// Unreachable.			     
  			}
 	}

 	
  	//***********************************************************************//	
 	// Overriden from OnCheckedChangeListener.								 //
 	//***********************************************************************//
	@Override
	public void onCheckedChanged(CompoundButton button, boolean isChecked) 
	{
		switch (button.getId())
		{
			case R.id.toggleButtonShowID:
				 populateEditBoxes();
				 break;
		}
	}

 	
 	
 	
 	//**********************************************************************//
 	// Populate edit boxes.													//
 	//**********************************************************************//
 	private void populateEditBoxes()
 	{
		//******************************************************************//
		// Is there a valid Secchi address?  If so, we can't get a new		//
		// one.																//
		//******************************************************************//
 		boolean validID = secchiPackageVariables.isHaveSecchiID();
		if (validID)
		{
			String id = secchiPackageVariables
					    .getVesselParticulars()
					    .getSSR();
			String string1 = id.substring(0, 5);	//Offset past the last character
			String string2 = id.substring(5, 10);
			String string3 = id.substring(10, 15);
			String string4 = id.substring(15, 20);

			editTextID1.setText(string1);
			editTextID2.setText(string2);
			editTextID3.setText(string3);
			editTextID4.setText(string4);
			
			if (toggleButtonShowID.isChecked())
			{
				editTextID1.setInputType(InputType.TYPE_CLASS_TEXT
						   | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
				editTextID2.setInputType(InputType.TYPE_CLASS_TEXT
						   | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
				editTextID3.setInputType(InputType.TYPE_CLASS_TEXT
						   | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
				editTextID4.setInputType(InputType.TYPE_CLASS_TEXT
						   | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
			}
			else
			{
				editTextID1.setInputType(InputType.TYPE_CLASS_TEXT 
		                  | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				editTextID2.setInputType(InputType.TYPE_CLASS_TEXT 
		                  | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				editTextID3.setInputType(InputType.TYPE_CLASS_TEXT 
		                  | InputType.TYPE_TEXT_VARIATION_PASSWORD);
				editTextID4.setInputType(InputType.TYPE_CLASS_TEXT 
		                  | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		}
		}
 	}

 	
}	// End of classy class.
