package uk.ac.plymouth.matmutt.secchi.view

import android.support.v7.app.AppCompatActivity
import io.reactivex.subjects.PublishSubject
import uk.ac.plymouth.matmutt.secchi.domain.ApplicationState

/**
 * Created by David Crotty on 17/04/2017.
 *
 * Copyright © 2017 David Crotty - All Rights Reserved
 */
interface IMainMenuView {
    fun promptUserToReadDocumentation()
    fun enableDepth()
    fun disableDepth()
    fun enableOptionalAndSubmission()
    fun disableOptionalAndSubmission()
    fun enableGPS()
    fun disableGPS()
    fun enableAll()
    fun disableAll()
    fun clearDepth()
    fun clearTemperature()
    fun getActivity() : AppCompatActivity
    fun updateCoordinatesWith(lattitude: String, longitude: String, timeOfReading: String)
    fun updateDepthWith(depth: Float)
    fun updateTemperatureWith(temperature: Float)
    fun blockSubmission()
    fun allowSubmission()
    fun updateBoatNameWith(name : String)
    fun resetBoatName()
    fun showTerms()
    fun hideGPSSpinner()
    fun showGPSSpinner()
    fun beginGPSSearch(mockReading: Boolean)
    fun showSatelliteDialog()
    fun startSatelliteActivity()
    fun showPhotoComplete()
    fun showPhotoIncomplete()
}