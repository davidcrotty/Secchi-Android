//**************************************************************************//
//- Secchi Disk Project.   Android version, starting life in	 			//
// Layouts for Android 2.2.  Android 4 version will follow.					//
//																			//
// Take a photo Activity.													//
//																			//
// You will see that I am not using a mix of "JavaDoc" style of comments, as//
// well as the "Nigel style comments for methods.							//
// 																			//
//   Nigel@soc.plymouth.ac.uk 2012.											//
//**************************************************************************//


package uk.ac.plymouth.matmutt.secchi.activities;

import java.io.File;
import java.io.FileOutputStream;

import uk.ac.plymouth.matmutt.secchi.R;
import uk.ac.plymouth.matmutt.secchi.helpers.D;
import uk.ac.plymouth.matmutt.secchi.helpers.SecchiPackageVariables;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;



//**************************************************************************//
//An Activity is the basic container for an Android application.			//	
//																			//
// We can see from the line below that this Activity listens for events.	//	
//**************************************************************************//
public class TakePhotoActivity extends Activity implements
											    OnClickListener
{

	
	//**********************************************************************//
	// Instance variables.													//
	//**********************************************************************//
	private String                 activityName             = "";
	private SecchiPackageVariables secchiPackageVariables   = null;
	private Button                 buttonSave               = null; 
	private Button                 buttonCancel             = null;
	private ImageButton			   imageButtonTakePhoto	    = null;	  
	private ImageView              imageViewPhotoOnView	  	= null;
	private Bitmap                 bitmapPhoto 				= null;
	private String                 highResFileName          = "hi_res_image.jpeg";
	private int					   imageResX		        = 640;
	private int                    imageResY                = 480;
	
	
	//**************************************************************************//
	// An Activity is the basic container for an Android application.			//	
	//																			//
	// We can see from the line below that this Activity listens for events.	//
	//**************************************************************************//
   @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.take_photo_activity_layout);
               
        secchiPackageVariables = SecchiPackageVariables.getInstance(this);
        activityName           = getClass().getSimpleName();


        //******************************************************************//
   	 	// Get the UI components.         									//
   	 	//******************************************************************//
        buttonSave           = (Button)      findViewById(R.id.buttonSave);
        buttonCancel         = (Button)      findViewById(R.id.buttonCancel);
        imageButtonTakePhoto = (ImageButton) findViewById(R.id.imageButtonTakePhoto);
        imageViewPhotoOnView = (ImageView)   findViewById(R.id.imageViewPhoto);
        
        
        buttonSave.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        imageButtonTakePhoto.setOnClickListener(this);
    }

   
    
    
	//**********************************************************************//
	// Call-backs for activity.												//
	//**********************************************************************//
	// On Pause call-back.  Paused != stopped.							   	//
   	//**********************************************************************//
	@Override
	public void onPause()
	{
	   super.onPause();
	   secchiPackageVariables.release();
	   
	   
	   if (D.debug) Log.i(activityName, "Activity paused");
	}

	
	
	//**********************************************************************//
	// On Resume call-back.  Start the location service listeners.  You must//
	// understand the state diagram of an Activity at this point.			//
	//**********************************************************************//
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		secchiPackageVariables = SecchiPackageVariables.getInstance(this);
		secchiPackageVariables.retain();
		
		//Stop the screen re-orienting itself.
		switch (secchiPackageVariables.getOrientation())
        {
        	case vertical:
        		 setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        		 break;
       	  
        	case horizontal:
       		     setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        		 break;
        }
	}
	


	
	
	//**********************************************************************//
	// On Restart call-back.  Pairs with OnStopped.   Paused != stopped.	//	
	//**********************************************************************//
	@Override
	protected void onRestart()
	{
	   super.onRestart();
	   if (D.debug) Log.i(activityName, "Activity restarted");   
	}
	   
	   
	//***********************************************************************//
	// On Stop call-back.  Pairs with On resume   Paused != stopped.		 //	   
	// 																		 //
	// This is the only Activity in the suite that doesn't destroy the 		 //
	// package variables, as the camera doesn't retain the package 			 //
	// variables.															 //
	//***********************************************************************//
	@Override
	protected void onStop()
	{
	   super.onStop();	   
	   if (D.debug) Log.i(activityName, "Activity stopped");
	}
	   
	   
	//***********************************************************************//
	// On Destroy call-back.  We are never usually terminated by Android; we //
	// usually just go into a stopped state until we are resumed.			//	
	//***********************************************************************//
	@Override
	protected void onDestroy()
	{
	   super.onDestroy();
	   if (D.debug) Log.i(activityName, "Activity destroyed"); 
	}

	
    //**********************************************************************//
 	// Another overridden method of an Activity.  The menu is defined in 	//
    // res/menu.menu.xml													//
    //																		//
    // Basically, we create a MenuInflater, and tell it what the menu looks //
    // like through this "R" thing.											//
 	//**********************************************************************//
     @Override
     public boolean onCreateOptionsMenu(Menu menu) 
     {
     	super.onCreateOptionsMenu(menu);	//Must invoke super class.
     	
     	return true;
     }


     
     
    //*********************************************************************//
    // Overriden from Activity, the callback we get when we take a photo	//
    //*********************************************************************//
    @Override
    protected void onActivityResult(int    requestCode, 
    		                        int    resultCode, 
    		                        Intent intent) 
    {
    	switch (requestCode)
    	{
    		case 1:			//Check it came from our intent wot we fired off.
    		   	 try		//It can't have come from anywhere else.
    		   	 {
    		   		 
    		   		 //*****************************************************//
    		   		 // The "data" extra contains an image, but not a 		//
    		   		 // high quality one.  And we don't get it at all if we //
    		   		 // specify a file name.								//
    		   		 //*****************************************************//
	    		   	 //Bundle extras = intent.getExtras();
	    	    	 //Bitmap imageHhumbnail = (Bitmap) extras.get("data");
	    	    	 
    		   		 
    		   		 //*****************************************************//
    		   		 // We fired off the Intent with a file name extra, so	//
    		   		 // we expect a file to be dumped instead of the "data" //
    		   		 // extra. 												//
    		   		 //*****************************************************//
     				 String fileName 
				        = secchiPackageVariables.buildFullPathForFileOnSDCard(highResFileName);
	    	    	 Bitmap bitmapHiRes   = BitmapFactory.decodeFile(fileName); 
	    	    	 int    highResWidth  = bitmapHiRes.getWidth();
	    	    	 int    highResHeight = bitmapHiRes.getHeight(); 
	    	    	 float  aspectRatio   = (float) highResWidth / (float) highResHeight;
	    	    	 
	    	    	 if (D.debug)
	    	    		 Log.i(activityName, "Photo taken.  Native width "
	    	    				             + highResWidth
	    	    				             + " native height "
	    	    				             + highResHeight
	    	    				             + " aspect ratio "
	    	    				             + aspectRatio);
	    	    	 
	    	    	 int sx, sy;
	    	    	 if (aspectRatio > 1) 
	    	    	 {
	    	    		 sx = imageResX;	sy = imageResY;
	    	    	 }
	    	    	 else
	    	    	 {
	    	    		 sx= imageResY;		sy = imageResX;
	    	    	 }
	    	    	 
	    	    	 bitmapPhoto        = Bitmap.createScaledBitmap(
	    	    			              bitmapHiRes, 
	    	    			              sx, sy, false);
	    	    	 
	    	    	 imageViewPhotoOnView.setImageBitmap(bitmapPhoto);
	    	   
	    	    	 
	    	    	 //*****************************************************//
	    	    	 // Clean up by deleting the file.						//
	    	    	 //*****************************************************//
	    	    	 File file = new File(fileName);
	    	    	 file.delete();
    		   	 }
    		   	 catch (Exception doh)
    		   	 {
    		   		 if (D.debug)
    		   			 Log.e(activityName, "Can't get camera bitmap " + doh.getMessage());
    		   	 }
    		   	 break;
    		   	 
    	}
 
    }
     
     
  	//***********************************************************************//
  	// Overridden from View.OnclickListener.								 //
  	//***********************************************************************//
 	@Override
 	public void onClick(View view) 
 	{
 		Intent intent = null;
 		
 		switch (view.getId())
 		{
 					 
 			//**************************************************************//
 			// Save dumps an image "currentImage.jpeg" to the folder on the	//
 			// SD card.														//
 			//**************************************************************//
 			case R.id.buttonSave:
 				 try
 				 {
	 				 String fileName = secchiPackageVariables.buildFullPathForFileOnSDCard(
	 						           SecchiPackageVariables.FileNameCurrentPhoto);
	 				 File   file     = new File(fileName);
	 				 if (file.exists()) file.delete();
	 				 FileOutputStream out = new FileOutputStream(file);
	 				 
	 				 bitmapPhoto.compress(CompressFormat.JPEG, 
	 						              80,					//80% quality 
	 						 			  out);
	 				 out.close();
	 				 secchiPackageVariables.setPhotoTaken(true);
 				 }
 				 catch (Exception doh)
 				 {
 					 if (D.debug)
 						 Log.e(activityName, "Can't save image "
 								 + doh.getMessage());
 					 Toast.makeText(this, R.string.imageNotSaved, 
 							        Toast.LENGTH_SHORT).show();
 					 secchiPackageVariables.setPhotoTaken(false);
 				 }
 				 finish();
 				 break;		// Unreachable.
 				 
 			case R.id.buttonCancel:
 				 finish();
 				 break;		// Unreachable.
 				 
 				 
 			//**************************************************************//
 			// Fire off an intent to take a photo.  If we want a high-res	//
 			// image, we also need to specify a file name.					//
 			//**************************************************************//
 			case R.id.imageButtonTakePhoto:
 				 String fileName 
 				        = secchiPackageVariables.buildFullPathForFileOnSDCard(highResFileName);
 				 File file = new File(fileName);
 				 if (file.exists()) file.delete();
 				 
 				 intent    = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 				 
 				 intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
 				 startActivityForResult(intent, 1);	//Action code 1 returned

 		}
 	}



 

}	// End of classy class.
